-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2019 at 08:52 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gss`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_lamaran`
--

CREATE TABLE `data_lamaran` (
  `id_lamaran` int(11) NOT NULL,
  `data_pelamar_id` int(11) DEFAULT NULL,
  `data_lowongan_id` int(11) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `upload`
--

CREATE TABLE `upload` (
  `id` int(11) NOT NULL,
  `data_pelamar_id` int(11) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `sertifikat_satpam` varchar(255) DEFAULT NULL,
  `skck` varchar(255) DEFAULT NULL,
  `surat_dokter` varchar(255) DEFAULT NULL,
  `surat_lamaran_kerja` varchar(255) DEFAULT NULL,
  `daftar_riwayat_hidup` varchar(255) DEFAULT NULL,
  `scan_ktp` varchar(255) DEFAULT NULL,
  `kartu_keluarga` varchar(255) DEFAULT NULL,
  `surat_keterangan_suami` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `upload`
--

INSERT INTO `upload` (`id`, `data_pelamar_id`, `foto`, `sertifikat_satpam`, `skck`, `surat_dokter`, `surat_lamaran_kerja`, `daftar_riwayat_hidup`, `scan_ktp`, `kartu_keluarga`, `surat_keterangan_suami`) VALUES
(28, 0, '', '', '', '', '', '', '', '', ''),
(29, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 16, '', '', '', '', '', '', '', '', ''),
(31, 17, NULL, 'e3bcc3a06bb752e1bac6a5838413fd2b.jpg', '9e3089f5c5329792f640d74207da556c.png', '3e7a5a67fec025189c8ad607275b31cb.jpg', 'dc5a1708839642b963c32d75b2347985.png', '3d31b508ab0437c86d5a0e67eaec0e19.png', '700bcf351dc07fef68f46dce5b79e817.png', 'fbb4e6d58d55392efada03870c52af6d.png', '224904c5c5d049f4209cfe157308963f.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_lamaran`
--
ALTER TABLE `data_lamaran`
  ADD PRIMARY KEY (`id_lamaran`);

--
-- Indexes for table `upload`
--
ALTER TABLE `upload`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `upload`
--
ALTER TABLE `upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
