-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 05, 2019 at 04:26 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gss`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_atribut_calon`
--

CREATE TABLE `data_atribut_calon` (
  `id` int(10) NOT NULL,
  `data_pelamar_id` int(11) NOT NULL,
  `id_atribut` int(10) NOT NULL,
  `status` enum('YA','TIDAK','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_barang`
--

CREATE TABLE `data_barang` (
  `id` int(10) NOT NULL,
  `kode_barang` varchar(12) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `nama_category` varchar(50) NOT NULL,
  `ukuran` varchar(10) NOT NULL,
  `harga_satuan` varchar(10) NOT NULL,
  `stok_qty` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_barang`
--

INSERT INTO `data_barang` (`id`, `kode_barang`, `nama_barang`, `nama_category`, `ukuran`, `harga_satuan`, `stok_qty`) VALUES
(1, 'k123', 'wkwkw', 'kwkw', '12', '123', 123);

-- --------------------------------------------------------

--
-- Table structure for table `data_fisik_pelamars`
--

CREATE TABLE `data_fisik_pelamars` (
  `id` int(11) NOT NULL,
  `data_pelamar_id` int(11) DEFAULT NULL,
  `tinggi_bdn` varchar(10) DEFAULT NULL,
  `berat_bdn` varchar(10) DEFAULT NULL,
  `gol_darah` varchar(5) DEFAULT NULL,
  `warna_kulit` varchar(20) DEFAULT NULL,
  `bentuk_muka` varchar(20) DEFAULT NULL,
  `warna_mata` varchar(20) DEFAULT NULL,
  `jenis_rambut` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_fisik_pelamars`
--

INSERT INTO `data_fisik_pelamars` (`id`, `data_pelamar_id`, `tinggi_bdn`, `berat_bdn`, `gol_darah`, `warna_kulit`, `bentuk_muka`, `warna_mata`, `jenis_rambut`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(5, 5, 'tatat', '12', 'B', 'Hitam', 'Lonjong', 'lain kali', 'lainnya lagi', '2018-09-25 11:28:21', '', '2018-09-25 11:28:21', '', NULL),
(6, 1, '167', '68', '0', 'Item', 'Kotak', 'Ijo', 'Merah', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(13, 1, '173', '50', 'B', 'Hitam', 'Lonjong', 'Coklat', 'Keriting', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(14, 14, '160', '66', 'A', 'Sawo Matang', 'Oval', 'Hitam', 'Lurus', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(15, 15, '', '', '', '', '', '', '', '2018-12-21 21:24:01', '', '0000-00-00 00:00:00', '', NULL),
(16, 16, '180', '50', 'AB', 'Putih', 'Bulat', 'Coklat', 'Ikal', '2018-12-24 23:08:13', '', '0000-00-00 00:00:00', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_kategori_barang`
--

CREATE TABLE `data_kategori_barang` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_kategori_barang`
--

INSERT INTO `data_kategori_barang` (`id`, `nama_kategori`) VALUES
(1, 'Atribut'),
(2, 'Seragam'),
(3, 'Lain-lain');

-- --------------------------------------------------------

--
-- Table structure for table `data_keluarga_pelamars`
--

CREATE TABLE `data_keluarga_pelamars` (
  `id` int(11) NOT NULL,
  `data_pelamar_id` int(11) DEFAULT NULL,
  `nama_ayah` varchar(50) DEFAULT NULL,
  `nama_ibu` varchar(50) DEFAULT NULL,
  `alamat_ortu` varchar(50) DEFAULT NULL,
  `no_tlp_ortu` varchar(50) DEFAULT NULL,
  `saudara_terdekat` varchar(50) DEFAULT NULL,
  `no_tlp_saudara` varchar(50) DEFAULT NULL,
  `anak_ke` varchar(50) DEFAULT NULL,
  `jml_bersaudara` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_keluarga_pelamars`
--

INSERT INTO `data_keluarga_pelamars` (`id`, `data_pelamar_id`, `nama_ayah`, `nama_ibu`, `alamat_ortu`, `no_tlp_ortu`, `saudara_terdekat`, `no_tlp_saudara`, `anak_ke`, `jml_bersaudara`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 4, 'lele', 'lele', 'llele', 'klajdks', 'asdjklasd', 'laskjd', '3', '3', '2018-11-10 09:50:47', '', '2018-11-10 09:50:47', '', '2018-11-10 09:50:47'),
(2, 5, 'maruan', 'idhsihiodsoih', 'oihoihiohio', 'hiohiohiohi', 'ohiohiohio', 'hihoih', '12', '12', '2018-09-25 11:28:21', '', '2018-09-25 11:28:21', '', NULL),
(9, 1, 'sdvdsvsdvsd', 'sdvdsvdsvs', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(10, 14, 'asfasfas', 'fasfas', 'gwew', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(11, 15, 'ayah', 'dgsraes', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(12, 16, 'ba', 'sarni', '     jalan jalan', '0987654', 'firman', '234567', '2', '10', '2018-12-25 03:36:06', '', '0000-00-00 00:00:00', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_keluar_barang`
--

CREATE TABLE `data_keluar_barang` (
  `id` int(10) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `nama_category` varchar(50) NOT NULL,
  `qty_out` int(10) NOT NULL,
  `tujuan_tempat` varchar(200) NOT NULL,
  `tanggal` date NOT NULL,
  `create_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_lain_pelamars`
--

CREATE TABLE `data_lain_pelamars` (
  `id` int(11) NOT NULL,
  `data_pelamar_id` int(11) DEFAULT NULL,
  `referensi` varchar(50) DEFAULT NULL,
  `teman_global` int(2) DEFAULT NULL,
  `nama_teman_global` varchar(50) DEFAULT NULL,
  `alamat_tinggal_sekarang` varchar(100) DEFAULT NULL,
  `tlp_keluarga1` varchar(20) DEFAULT NULL,
  `tlp_keluarga2` varchar(20) DEFAULT NULL,
  `tlp_keluarga3` varchar(20) DEFAULT NULL,
  `nama_tetangga_kiri` varchar(50) DEFAULT NULL,
  `alamat_tetangga_kiri` varchar(100) DEFAULT NULL,
  `tlp_tetangga_kiri` varchar(50) DEFAULT NULL,
  `nama_tetangga_kanan` varchar(50) DEFAULT NULL,
  `alamat_tetangga_kanan` varchar(100) DEFAULT NULL,
  `tlp_tetangga_kanan` varchar(50) DEFAULT NULL,
  `nama_tetangga_belakang` varchar(50) DEFAULT NULL,
  `alamat_tetangga_belakang` varchar(100) DEFAULT NULL,
  `tlp_tetangga_belakang` varchar(50) DEFAULT NULL,
  `nama_tetangga_depan` varchar(50) DEFAULT NULL,
  `alamat_tetangga_depan` varchar(100) DEFAULT NULL,
  `tlp_tetangga_depan` varchar(50) DEFAULT NULL,
  `nama_rt` varchar(50) DEFAULT NULL,
  `alamat_rt` varchar(100) DEFAULT NULL,
  `tlp_rt` varchar(50) DEFAULT NULL,
  `kerja_dari1` date DEFAULT NULL,
  `kerja_sampai1` date DEFAULT NULL,
  `kerja_jabatan1` varchar(50) DEFAULT NULL,
  `kerja_pt1` varchar(50) DEFAULT NULL,
  `kerja_dari2` date DEFAULT NULL,
  `kerja_sampai2` date DEFAULT NULL,
  `kerja_jabatan2` varchar(50) DEFAULT NULL,
  `kerja_pt2` varchar(50) DEFAULT NULL,
  `kerja_dari3` date DEFAULT NULL,
  `kerja_sampai3` date DEFAULT NULL,
  `kerja_pt3` varchar(50) DEFAULT NULL,
  `kerja_jabatan3` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_lain_pelamars`
--

INSERT INTO `data_lain_pelamars` (`id`, `data_pelamar_id`, `referensi`, `teman_global`, `nama_teman_global`, `alamat_tinggal_sekarang`, `tlp_keluarga1`, `tlp_keluarga2`, `tlp_keluarga3`, `nama_tetangga_kiri`, `alamat_tetangga_kiri`, `tlp_tetangga_kiri`, `nama_tetangga_kanan`, `alamat_tetangga_kanan`, `tlp_tetangga_kanan`, `nama_tetangga_belakang`, `alamat_tetangga_belakang`, `tlp_tetangga_belakang`, `nama_tetangga_depan`, `alamat_tetangga_depan`, `tlp_tetangga_depan`, `nama_rt`, `alamat_rt`, `tlp_rt`, `kerja_dari1`, `kerja_sampai1`, `kerja_jabatan1`, `kerja_pt1`, `kerja_dari2`, `kerja_sampai2`, `kerja_jabatan2`, `kerja_pt2`, `kerja_dari3`, `kerja_sampai3`, `kerja_pt3`, `kerja_jabatan3`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 5, 'aziz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-01', '2018-09-26', 'anuan', 'anu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-25 11:28:21', '', '2018-09-25 11:28:21', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_logistik_calon`
--

CREATE TABLE `data_logistik_calon` (
  `id` int(10) NOT NULL,
  `data_pelamar_id` int(11) NOT NULL,
  `nama_calon` varchar(200) NOT NULL,
  `ukuran_seragam` varchar(5) NOT NULL,
  `ukuran_sepatu` varchar(5) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_masuk_barang`
--

CREATE TABLE `data_masuk_barang` (
  `id` int(10) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `qty_in` int(10) NOT NULL,
  `uniform` varchar(200) NOT NULL,
  `keterangan_PO` varchar(200) NOT NULL,
  `tanggal` date NOT NULL,
  `create_by` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_pelamars`
--

CREATE TABLE `data_pelamars` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tanggal_lahir` datetime DEFAULT NULL,
  `umur` int(5) DEFAULT NULL,
  `jns_kelamin` varchar(50) DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `alamat_tinggal` varchar(100) DEFAULT NULL,
  `alamat_tinggal_kecamatan` varchar(50) DEFAULT NULL,
  `alamat_tinggal_kota` varchar(50) DEFAULT NULL,
  `status_tempat_tinggal` varchar(50) DEFAULT NULL,
  `no_tlp` varchar(50) DEFAULT NULL,
  `type_id` varchar(50) DEFAULT NULL,
  `no_id` varchar(50) DEFAULT NULL,
  `no_id_berkalu` datetime DEFAULT NULL,
  `status_nikah` varchar(50) DEFAULT NULL,
  `nama_pasangan` varchar(50) DEFAULT NULL,
  `tgl_lahir_pasangan` datetime DEFAULT NULL,
  `nama_anak1` varchar(50) DEFAULT NULL,
  `tgl_lahir_anak1` datetime DEFAULT NULL,
  `nama_anak2` varchar(50) DEFAULT NULL,
  `tgl_lahir_anak2` datetime DEFAULT NULL,
  `nama_anak3` varchar(50) DEFAULT NULL,
  `tgl_lahir_anak3` datetime DEFAULT NULL,
  `nama_anak4` varchar(50) DEFAULT NULL,
  `tgl_lahir_anak4` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_pelamars`
--

INSERT INTO `data_pelamars` (`id`, `nama_lengkap`, `tempat_lahir`, `tanggal_lahir`, `umur`, `jns_kelamin`, `agama`, `alamat_tinggal`, `alamat_tinggal_kecamatan`, `alamat_tinggal_kota`, `status_tempat_tinggal`, `no_tlp`, `type_id`, `no_id`, `no_id_berkalu`, `status_nikah`, `nama_pasangan`, `tgl_lahir_pasangan`, `nama_anak1`, `tgl_lahir_anak1`, `nama_anak2`, `tgl_lahir_anak2`, `nama_anak3`, `tgl_lahir_anak3`, `nama_anak4`, `tgl_lahir_anak4`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(5, 'aziz', 'tangsel', '2018-09-01 00:00:00', 15, 'Pria', 'Kristen', 'jdsugdsg', 'ciputat', 'tangesr', 'Milik Sendiri', 'hdios', '', '1234567', NULL, 'Belum Menikah', 'sumijem', '2018-12-23 00:00:00', 'augusdgsdiu', '2018-09-26 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', NULL, NULL, '2018-09-25 04:28:21', '', '0000-00-00 00:00:00', '', NULL),
(13, 'gunawan', 'bandung', '2018-11-21 00:00:00', 1213, 'Pria', 'Islam', '31rsrgsrgs', 'sdvsd', 'sdvds', '', '', '', '', NULL, '', 'asfdg', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', NULL, NULL, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(14, 'jono', 'jkt', '0000-00-00 00:00:00', 0, '', '', 'asfas', '', '', '', '12345', '', '2341', NULL, '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', NULL, NULL, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(15, 'joni', '', '2018-12-04 00:00:00', NULL, '', '', '', '', '', '', '', '', '123445', NULL, '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', NULL, NULL, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(16, 'maman abdu', 'jakarta', '2018-12-16 00:00:00', 30, 'Wanita', 'Islam', 'jalan jalan jalan', 'disana', 'disitu', 'Keluarga', '021468', '', '12345699', NULL, 'Menikah', 'jini', '2018-12-10 00:00:00', 'dana', '2018-12-13 00:00:00', 'dini', '2018-12-11 00:00:00', 'dunu', '2018-12-20 00:00:00', NULL, NULL, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_pendidikan_pelamars`
--

CREATE TABLE `data_pendidikan_pelamars` (
  `id` int(11) NOT NULL,
  `data_pelamar_id` int(11) DEFAULT NULL,
  `pendidikan_terakhir` varchar(20) DEFAULT NULL,
  `asal_sekolah` varchar(100) DEFAULT NULL,
  `kota` varchar(50) DEFAULT NULL,
  `pendidikan_satpam` varchar(100) DEFAULT NULL,
  `tempat_pendidikan` varchar(100) DEFAULT NULL,
  `sertifikat` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_pendidikan_pelamars`
--

INSERT INTO `data_pendidikan_pelamars` (`id`, `data_pelamar_id`, `pendidikan_terakhir`, `asal_sekolah`, `kota`, `pendidikan_satpam`, `tempat_pendidikan`, `sertifikat`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(4, 5, 'SMA', 'jakarta', 'tangsel', 'Pra - Dasar', 'sonoh', '1', '2018-09-25 04:28:21', '', '2018-09-25 04:28:21', '', NULL),
(11, 1, 'SMA', 'dfsvs', '', '', '', NULL, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(12, 14, 'D3', 'awfsasfasf', 'safsaa', 'Dasar', '', NULL, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(13, 15, 'SMA', 'sfdsgs', 'sdgsdg', '', '', NULL, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(14, 16, 'SMA', 'SMK', 'disana', 'Lanjutan', 'jakarta', NULL, '2018-12-25 02:09:35', '', '0000-00-00 00:00:00', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `entity_id` int(10) UNSIGNED DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assets` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `history_types`
--

CREATE TABLE `history_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_types`
--

INSERT INTO `history_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'User', '2018-09-25 19:42:38', '2018-09-25 19:42:38'),
(2, 'Role', '2018-09-25 19:42:38', '2018-09-25 19:42:38');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_12_28_171741_create_social_logins_table', 1),
(4, '2015_12_29_015055_setup_access_tables', 1),
(5, '2016_07_03_062439_create_history_tables', 1),
(6, '2017_04_04_131153_create_sessions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'view-backend', 'View Backend', '2018-09-25 19:42:37', '2018-09-25 19:42:37');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`) VALUES
(1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `all` tinyint(1) NOT NULL DEFAULT '0',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `all`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 1, 1, '2018-09-25 19:42:36', '2018-09-25 19:42:36'),
(2, 'Executive', 0, 2, '2018-09-25 19:42:36', '2018-09-25 19:42:36'),
(3, 'User', 0, 3, '2018-09-25 19:42:36', '2018-09-25 19:42:36');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('FvOPD5pdJpuXXxLcv0WN1Nkb1x1pqFdUgWzFCnzZ', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'ZXlKcGRpSTZJamwwVUU5RmIyMVBiV0ZMUjNCV2REZzBTa1JJYkVFOVBTSXNJblpoYkhWbElqb2lOekpOWjFZcmNETndSMVpCWTJWUloxd3ZaV0psSzNsaFVHSm1WemRsYzNjMU9ERnplSEUyTTJscVQzazFXamxhUlc5SGQweG5UVFYzVjBNd2IxRmtaMXd2UTNOYVdWaGNMMGcyUzFGTU5GazFaRmMxYmpoVmJHbGxkbkZ2UTNGR2JtSjBaMGRQZFhsNFUzVkpkek16UmxnelNWVlFZV2hxZFVOcmFHNUxZVWRFU0VabVdHWnlhV04wWnpoSVVrUm1SMlpjTDJsMVNDdG9Za0pJTmxoNVdteDNSbkUwUldwdFJ6SnBXR2Q1WTJoUmJXOTVPVWw2TmtwalZWWkJZbkJLV0ZOSVFuUk9kR3ROY1U5aWFVZHRRazlJT0Z3dlFVc3pTRTEyUkc1UlFYQmtPVzFKWm0wNFdFUXlWVlZCT0VkRVVGQTFjR280UXpRNE4yRlFjRUpSWjJsbGQxUmxXVzB4UlZrcmJFUTVOM3BqTXpSSGFXcE5VMVV5ZVRGcFRTdFBSMUk1WjB4TU4zcFBTV3cwWlVKbVdHcGpXVmxVUVhwaldXVlliMnBVZEUwd1RtTXpObXBIVFRaTmEwSXhla2hEWjB4S01HTmxaRkpLWjBOUllqVjRUVmRZVm04d01tRjBRalZITjNCcGEwcHpXVlJXWldaRFpUaFRUWE5WWEM5Q1NYSnVVRmhJVlVvNWVTdExWSEoyWm01UU1FbG1ORmRwTlRKUlhDOXRNa1pLUTJzNE9HeG1lU3RuYml0WlQyeExWWE4zWmxadk4yOTVaMGh2ZVV0b05rZ3lOVnd2ZUhkdFhDOWhRM1UzTWxCUmJXZFRTVkkyUjA5a04wbEZXVnBEVG5WeFpHdDZWek41WjFKM1kybDRRM2cwUkVVOUlpd2liV0ZqSWpvaU5EVTNNVEJrWWpCaU1tTXdNelF5TnpJMk4ySmxPRFJtTURjM05qUXhNak5pTm1Fd00yWXhOak5oWkRGaU4yTTFaREE1Tm1NeVlqSmpOVEF6TkRJNE5pSjk=', 1540032571),
('JbNz5UPSd6eJ6jn8cr9CugglP8cPEFetaCLXJrDX', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'ZXlKcGRpSTZJakZoUkZWRVoyeDFUelZaZFdsdlUycFdiMDFGVTFFOVBTSXNJblpoYkhWbElqb2lhRGg0YlRCcVUxaEZYQzlPTTFSMmVsWjVNM0ZpUTNaSVVuRm1UbHd2UkhCaVpGWnlUbUpJUlV4Sk0wMW5aMDVvTmt4VFQwNUtZM014TUhkMFRUUjJXVTgwT1RCSmNFaDVUMllyYldsR01XZ3dOSEJ2T0hWaVYyRk9UMWhDTmpOUlVrMWNMM1o1TVRKcWRrNHdjblpTZG1GNGNrdFdPVFpWYTNCVFJsVnhTVVZxUWpOU0sxSklXV2RWVFUxYVVtZzNZV1UwWlhoT1V6Qk5NMVEzZVdsbU1ISkZhbnB5YTNaUE9HZDZhbUpVTm01eFVXOVdPVW96TTJWMk5sSmxNR3g1TmxSeVVtSldjVWwwYkhWcGFuRlRjVTlHWVc4MlNWVlBSVzFCY3pnM01tTmtNRVZWVkhJMlNUbE5SSFZCVkZONU5ITTFTSGRtVjBSWVowa3JSVTFPU3pZd01FaFhTSGMzWkVGWk5qbFFRMGRQWVU5aFVGaG1XVlZZVGt0U1hDOUZWamROYTBkUFYwNW9NVkpzUmxaU1RISndlazhyT1dOd1JsZGlOVFlyWW14Q1VYVkJRMUpYY1ZGNWJXRktOVE5JYURWYUswb3lPRVIxZWpFelkwVndTR3d5Ums1TWFFRnZjMVJHWVhKcU5EMGlMQ0p0WVdNaU9pSXlPV0U1T0RVM01tSTFaalkyWWpRMU1EQm1ObVk0TURNM1pqaGtOalpsT0dSbU1EWXhNMkZoWVdVeVpXWmhNall5WVdVMVlqazBNVGM1TmpJd01XWmhJbjA9', 1538018119);

-- --------------------------------------------------------

--
-- Table structure for table `social_logins`
--

CREATE TABLE `social_logins` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `upload`
--

CREATE TABLE `upload` (
  `id` int(11) NOT NULL,
  `data_pelamar_id` int(11) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `sertifikat_satpam` varchar(255) DEFAULT NULL,
  `skck` varchar(255) DEFAULT NULL,
  `surat_dokter` varchar(255) DEFAULT NULL,
  `surat_lamaran_kerja` varchar(255) DEFAULT NULL,
  `daftar_riwayat_hidup` varchar(255) DEFAULT NULL,
  `scan_ktp` varchar(255) DEFAULT NULL,
  `kartu_keluarga` varchar(255) DEFAULT NULL,
  `surat_keterangan_suami` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `confirmation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `status`, `confirmation_code`, `confirmed`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'Istrator', 'admin@admin.com', '$2y$10$B4R60xGlhyy2o.JaLZft7exeJVzdTN0eoxw8ZfvhHAsFG/2Cu0ocS', 1, '98051ba8ee356e2d3672537f4543a85a', 1, NULL, '2018-09-25 19:42:35', '2018-09-25 19:42:35', NULL),
(2, 'Backend', 'User', 'executive@executive.com', '$2y$10$17DsLvy1n5KzjVbbPsTdd..VKst0g6a5MYq7GDlfSp0LAtnJSfYjO', 1, 'aa1923fc1da62d335fe24a57ce201e54', 1, NULL, '2018-09-25 19:42:35', '2018-09-25 19:42:35', NULL),
(3, 'Default', 'User', 'user@user.com', '$2y$10$KQKU9BCxxbaJaHZ0elIouuc.HOKGwYWrV80jyS50l5ILeakdHUAmO', 1, 'df2df9c424d509c18a48927ca0f13eea', 1, NULL, '2018-09-25 19:42:35', '2018-09-25 19:42:35', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_atribut_calon`
--
ALTER TABLE `data_atribut_calon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_barang`
--
ALTER TABLE `data_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_fisik_pelamars`
--
ALTER TABLE `data_fisik_pelamars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_kategori_barang`
--
ALTER TABLE `data_kategori_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_keluarga_pelamars`
--
ALTER TABLE `data_keluarga_pelamars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_keluar_barang`
--
ALTER TABLE `data_keluar_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_lain_pelamars`
--
ALTER TABLE `data_lain_pelamars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_logistik_calon`
--
ALTER TABLE `data_logistik_calon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_masuk_barang`
--
ALTER TABLE `data_masuk_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_pelamars`
--
ALTER TABLE `data_pelamars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_pendidikan_pelamars`
--
ALTER TABLE `data_pendidikan_pelamars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_type_id_foreign` (`type_id`),
  ADD KEY `history_user_id_foreign` (`user_id`);

--
-- Indexes for table `history_types`
--
ALTER TABLE `history_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_logins_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_barang`
--
ALTER TABLE `data_barang`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `data_fisik_pelamars`
--
ALTER TABLE `data_fisik_pelamars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `data_kategori_barang`
--
ALTER TABLE `data_kategori_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `data_keluarga_pelamars`
--
ALTER TABLE `data_keluarga_pelamars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `data_keluar_barang`
--
ALTER TABLE `data_keluar_barang`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `data_lain_pelamars`
--
ALTER TABLE `data_lain_pelamars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `data_logistik_calon`
--
ALTER TABLE `data_logistik_calon`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `data_masuk_barang`
--
ALTER TABLE `data_masuk_barang`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `data_pelamars`
--
ALTER TABLE `data_pelamars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `data_pendidikan_pelamars`
--
ALTER TABLE `data_pendidikan_pelamars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `history_types`
--
ALTER TABLE `history_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `social_logins`
--
ALTER TABLE `social_logins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `history_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `history_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD CONSTRAINT `social_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
