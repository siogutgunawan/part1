<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<style>
		.p_header{
			font-size: 11px;
			padding-top: -10px;
		}
		.line_header{
			border: 0;
			border-style: inset;
			border-top: 1px solid #000;
			margin-top: -1px;
			margin-right: 50%;
			width: 75%;
			text-align: left;
		}
		p{
			font-size: 10px;
		}
	</style>
</head>
<body>
	<div class="container">
		<img src="assets/img/Burung2.png" alt="gambar" style="position: absolute; width: 130px; height: auto; margin-top: 10px;">
		<table style="width: 100%;margin-left: 80px; margin-top: 5px;">
		<tr>
			<td align="right">
				<span style="line-height: 1.6; font-weight: bold;"><h3 style="margin-top: 17px;"><b>PT. GELOBAL SECONT</b></h3></span>
				<p class="p_header"><b>Human Resource Departement</b></p>
				<p class="p_header">JL.CIPUTAT RAYA NO.16 PONDOK PINANG JAKARTA SELATAN 12310</p>
				<p class="p_header">Phone : (6221) 2904 4909 - Fax : (6221) 2904 4902</p>
				<p class="p_header">www.globalsecont.com / e-mail : contact@globalsecont.com</p>
			</td>
		</tr>
		</table>
		<hr class="line_header">
		<p style="font-size: 9px; padding-top: -20px;"><i>Tuesday, April 04, 2017</i></p>

		<div style="width: 100%; background-color: #eaeaea;">
			<p style="text-align: center;"><i><b>Employee Profile</b></i></p>
		</div>	
		<table style="margin-top: 5px;">
			<tr>
				<td width="150"><p>Nama / Name</p></td>
				<td width="250"><p style="padding-left: 3px;border: 1px solid #000;"><b>Nama</b></p></td>
				<td><span><img src="assets/img/foto.png" alt="foto" style="width: 100px;height: 150px; position: absolute;right: 15px; margin-top: 3px;"></span></td>
			</tr>
			<tr>
				<td width="150"><p>Tempat Lahir / Place of Birth</p></td>
				<td width="250"><p style="padding-left: 3px;border: 1px solid #000;"><b>Jakarta</b></p></td>
			</tr>
			<tr>
				<td width="150"><p>Tanggal Lahir / Date of Birth</p></td>
				<td><p style="padding-left: 3px;border: 1px solid #000; width: 100px;"><b>26-oct-73</b></p></td>
			</tr>
			<tr>
				<td width="150"><p>Jenis Kelamin / Sex</p></td>
				<td><p style="padding-left: 3px;border: 1px solid #000; width: 160px;"><b>Laki-laki</b></p></td>
				<td><p style="padding-left: 3px;border: 1px solid #000; width: 155px;margin-left: -160px;"><b>Male</b></p></td>
			</tr>
			<tr>
				<td width="150"><p>Alamat / Address</p></td>
				<td><p style="padding-left: 3px;border: 1px solid #000; height: 40px;"><b>KOMP. CAKRAWALA III BLOK G NO. 10 RT 004 RW 017 KEL. LAGOA KEC. KOJA</b></p></td>
			</tr>
			<tr>
				<td width="150"><p>Telepon / Phone</p></td>
				<td width="250"><p style="padding-left: 3px;border: 1px solid #000; width: 130px;"><b>081251570168</b></p></td>
			</tr>
			<tr>
				<td width="150"><p>Pendidikan / Education</p></td>
				<td width="250"><p style="padding-left: 3px;border: 1px solid #000; width: 130px;"><b>SMA</b></p></td>
				<td><p style="padding-left: 3px;border: 1px solid #000; width: 185px;margin-left: -190px;"><b>Senior Hight School</b></p></td>
			</tr>
			<tr>
				<td width="150"><p>Agama / Religion</p></td>
				<td width="250"><p style="padding-left: 3px;border: 1px solid #000; width: 130px;"><b>Islam</b></p></td>
				<td><p style="padding-left: 3px;border: 1px solid #000; width: 185px;margin-left: -190px;"><b>Moeslim</b></p></td>
			</tr>
		</table>

	</div>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>