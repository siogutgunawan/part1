<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<?php $this->load->view("backend/_partials/head.php") ?>
	<style>
		.line1{
			border: 0;
			border-style: inset;
			border-top: 1px thin #000;
			margin-top: -5px;
			width: 63%;
			text-align: center;
			background-color: lightblue;
		}
		.line2{
			border: 0;
			border-style: inset;
			border-top: 1px thin #000;
			margin-top: -20px;
			width: 64%;
			text-align: center;
			background-color: salmon;
		}
		.line3{
			border: 0;
			border-style: inset;
			border-top: 1px thin #000;
			margin-top: -10px;
			margin-right: -85px;
			width: 48%;
			text-align: center;
		}
		.paragraf{
			font-size: 12px;
			text-align-last: justify;
		}
		p{
			font-size: 11px;
		}
		li{
			margin-left: 11px;
			list-style-type: 1;
		}
		

	</style>
</head>
<body>
	
	<img src="assets/img/Burung2.png" style="position: absolute; width: 130px; height: auto; margin-top: 10px; left: 60px;">
	<table style="width: 100%;margin-left: 80px; margin-top: 5px;">
		<tr>
			<td align="center">
				<span style="line-height: 1.6; font-weight: bold;"><h1 style="margin-top: 17px;">PT GLOBAL SECONT</h1></span>
				<div style="margin-top: -8px;">
				<hr class="line1">
				<hr class="line2">
				</div>
			</td>
		</tr>
	</table>
	
	<br><br><br>
	<div class="container" style="width: 82%;">
		<h4 align="center"><b><u>SURAT KETERANGAN KERJA</u></b></h4>
		<p align="center" style="margin-top: -8px;">No. 993/SKK/GSS/17042017</p>
	<br>
		<p class="paragraf">Yang bertanda tangan dibawah ini, menerangkan bahwa :</p>
		<div style="margin-left: 35px;">
			<table>
			<tr>
				<td width="100"><p class="paragraf">Nama</p></td>
				<td width="20"><p class="paragraf">:</p></td>
				<td><p class="paragraf"><b>Nama</b></p></td>
			</tr>
			<tr>
				<td width="100"><p class="paragraf">No ID</p></td>
				<td width="20"><p class="paragraf">:</p></td>
				<td><p class="paragraf"><b>12014040239</b></p></td>
			</tr>
			<tr>
				<td width="100"><p class="paragraf">Jabatan</p></td>
				<td width="20"><p class="paragraf">:</p></td>
				<td><p class="paragraf"><b>SECURITY OFFICER</b></p></td>
			</tr>
			<tr>
				<td width="100"><p class="paragraf">Departement</p></td>
				<td width="20"><p class="paragraf">:</p></td>
				<td><p class="paragraf"><b>OPERATION</b></p></td>
			</tr>
			<tr>
				<td width="100"><p class="paragraf">Lokasi</p></td>
				<td width="20"><p class="paragraf">:</p></td>
				<td><p class="paragraf"><b>PT. MARGASARI MAKMUR</b></p></td>
			</tr>
		</table>
		</div>
		<p class="paragraf">Dengan ini menyatakan bahwa nama tersebut diatas adalah benar karyawan PT. GLOBAL SECONT mulai bekerja tanggal <b>9 April 2014</b> sampai dengan tanggal <b>16 Januari 2017</b>.</p>
		
		<p class="paragraf">Demikian surat keterangan kerja ini dibuat sebenar-benarnya untuk dapat dipergunakan sebagaimana mestinya.</p>
		<br>
		<div style="margin-top: 100px;">
			<table>
				<tr>
					<td><p class="paragraf">Dikeluarkan di </p></td>
					<td><p class="paragraf"> : </p></td>
					<td><p class="paragraf"> Jakarta</p></td>
				</tr>
				<tr>
					<td><p class="paragraf" style="padding-top: -13px;">Tanggal </p></td>
					<td><p class="paragraf" style="padding-top: -13px;"> : </p></td>
					<td><p class="paragraf" style="padding-top: -13px;"> 17 April 2017</p></td>
					<span>
						<hr style="border: 0;
							border-style: inset;
							border-top: 1px thin #000;
							margin-top: -10px;
							width: 27%;
							margin-right: 80%;">
					</span>
				</tr>
			</table>
			<p class="paragraf"><b>PT. GELOBAL SECONT</b></p>
			<br><br><br>
			<p class="paragraf"><b><u>SUHERI</u></b><br>SENIOR ASST.MANAGER HR</p>
		</div>
		<br>
		<footer style="margin-top: 150px;">
			<p>Jl. Ciputat Raya No 16, Pondok Pinang - Jakarta Selatan Telp : (021) 29044909 (Hunting) Fax : (021) 29044902</p>
			<p style="letter-spacing: 2.9px; padding-top: -10px;"><b style="color: salmon;">e-mail : contact@globalsecont.com</b><b style="color: lightblue">Website : www.globalsecont.com</b></p>			
		</footer>
	</div>


	
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
	<?php $this->load->view("backend/_partials/js.php") ?>
</body>
</html>