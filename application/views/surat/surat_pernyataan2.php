<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>coba</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
	
<header>
		<div class="container">
			<b><u><h2 align="center">SURAT PERNYATAAN</h2></u></b>
		</div>
	</header>
<br><br>
	<div class="container" style="text-align: justify;">
		<p>Saya yang bertanda tangan dibawah ini :</p>
		<div style="margin-left: 35px;">
			<table>
			<tr>
				<td width="200">Nama</td>
				<td width="20">:</td>
				<td><?php echo $nama; ?></td>
			</tr>
			<tr>
				<td width="200">Jabatan</td>
				<td width="20">:</td>
				<td><?php echo $jabatan; ?></td>
			</tr>
			<tr>
				<td width="200">Alamat Tinggal</td>
				<td width="20">:</td>
				<td><?php echo $alamat; ?></td>
			</tr>
		</table>
		</div>
		<p>Dengan ini menyatakan dengan sebenarnya bahwa :</p>
		<div style="margin-left: 10px;">
			<ol>
				<li>Apabila saya mengundurkan diri dari PT. Global Secont tanpa prosedur dan ketentuan yang berlaku, maka saya brsedia dikenakan PINALTI sesuai dengan isi perjanjian kerja (PKWT) yang telah saya tanda tangani.</li>
				<li>Apabila saya mengundurkan diri dari PT. Global Secont tanpa prosedur dan ketentuan yang berlaku, maka saya besedia mengganti biaya seragam yang telah diberikan kepada saya yang dipotong dari sisa gaji saya.</li>
				<li>Apabila saya mengundurkan diri dari PT. Global Secont tanpa prosedur dan ketentuan yang berlaku, maka saya bersedia tidak diberikan Surat Pengalaman Kerja.</li>				
			</ol>
		</div>
		
		<p>Demikian Surat Pernyataan ini saya buat dengan sebenarnya tanpa ada paksaan dari pihak manapun dan tidak akan ada tuntutan apapun dikemudian hari.</p>
		<p>Jakarta, 12 Desember, 2018</p>
		<br><br><br>
		<p>(.............................)</p>
		
	</div>


	<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
</body>
</html>