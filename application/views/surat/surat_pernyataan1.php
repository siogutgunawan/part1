<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>coba</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
	
<header>
		<div class="container">
			<b><u><h2 align="center">SURAT PERNYATAAN</h2></u></b>
		</div>
	</header>
<br><br>
	<div class="container" style="text-align: justify;">
		<p>Yang bertanda tangan dibawah ini </p>
		<div style="margin-left: 35px;">
			<table>
			<tr>
				<td width="200">Nama</td>
				<td width="20">:</td>
				<td><?php echo $nama; ?></td>
			</tr>
			<tr>
				<td width="200">Umur/Tempat Tanggal Lahir</td>
				<td width="20">:</td>
				<td><?php echo $umur; ?> Thn <?php echo $tempat_lahir; ?>, <?php echo $tanggal_lahir; ?></td>
			</tr>
			<tr>
				<td width="200">Alamat Tinggal</td>
				<td width="20">:</td>
				<td><?php echo $alamat; ?></td>
			</tr>
			<tr>
				<td width="200">No.Telepon/HP</td>
				<td width="20">:</td>
				<td><?php echo $no_telepon; ?></td>
			</tr>
		</table>
		</div>
		<p>Dengan ini menyatakan bahwa saya :</p>
		<div style="margin-left: 10px;">
			<ol>
				<li>Bersedia untuk melakukan pembayaran Pendidikan dan Pelatihan sebagai berikut :</li>
					<ol type="a">				
						<li>Uang sebesar Rp 2 Miliyar (sebagai uang Pendidikan dan Pelatihan Dasar Satpam).</li>
						<li>Pembayaran uang muka Rp 1 Miliyar.</li>
						<li>Kekurangan Rp 1 Miliyar akan dipotong dari gaji selama 10 (sepuluh) bulan terhitung bulan Januari sampai bulan Januari. </li>
					</ol>
				<li>Besedia ditempatkan di lokasi tugas yang ditunjuk dan atau ditetapkan oleh PT. Global Secont.</li>
				<li>Bilamana saya melakuan pelanggaran berat sebagaimana telah diatur dalam PKWT, maka PT GLOBAL SECONT berhak melakuakn pemutusan hubungan kerja secara sepihak kepada saya baik pada masa evaluasi maupun sebelum masa kontrak kerja saya berakhir tanpa adanya konpensasi apapun.</li>
				<li>Bersedia menerima upah yang besarnya disesuaikan dengan UMP setempat.</li>
				<li>Bersedia untuk tunduk dan patuh pada perusahaan PT Global Secont.</li>
				<li>Saya Menyatakan tidak sebagai anggota ORMASmaupun golongan / kelompok masyarakat tertentu.</li>				
			</ol>
		</div>
		<p>Bilamana dikemudian hari saya terbukti mengingkari ini maka say bersedia untuk menerima sanksi apapun dari PT. Global Secont.</p>
		<p>Demikianlah pernyataan ini saya dengan sebenar-benarnya tanpa adanya paksaan dari pihak manapun.</p>
		<p>Jakarta /12/12/2018</p>
		<br><br><br>
		<p>(.............................)</p>

	</div>


	<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
</body>
</html>