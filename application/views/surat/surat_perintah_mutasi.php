<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<?php $this->load->view("backend/_partials/head.php") ?>
	<style>
		.line1{
			border: 0;
			border-style: inset;
			border-top: 1px thin #000;
			margin-top: -5px;
			width: 63%;
			text-align: center;
			background-color: lightblue;
		}
		.line2{
			border: 0;
			border-style: inset;
			border-top: 1px thin #000;
			margin-top: -20px;
			width: 64%;
			text-align: center;
			background-color: salmon;
		}
		.line3{
			border: 0;
			border-style: inset;
			border-top: 1px thin #000;
			margin-top: -10px;
			margin-right: -85px;
			width: 48%;
			text-align: center;
		}
		p{
			font-size: 11px;
		}
		li{
			margin-left: 11px;
			list-style-type: 1;
		}
		

	</style>
</head>
<body>
	
	<img src="assets/img/Burung2.png" style="position: absolute; width: 130px; height: auto; margin-top: 10px; left: 60px;">
	<table style="width: 100%;margin-left: 80px; margin-top: 5px;">
		<tr>
			<td align="center">
				<span style="line-height: 1.6; font-weight: bold;"><h1 style="margin-top: 17px;">PT GLOBAL SECONT</h1></span>
				<div style="margin-top: -8px;">
				<hr class="line1">
				<hr class="line2">
				</div>
			</td>
		</tr>
	</table>
	
	<br><br>
	<div class="container" style="width: 82%;">
		<h6 align="center"><b><u>LETTER OF RE-ASSIGNMENT / SURAT PERINTAH MUTASI TUGAS</u></b></h6>
		<p align="center" style="margin-top: -8px;">No. 0200/SPMT/GSC/MATTEL/20170504</p>

		<p>it is ordered to / Diperintahkan kepada :</p>
		<div style="margin-left: 28px;">
			<table>
				<tr>
					<td width="150"><p>Name / Nama</p></td>
					<td width="15"><p>:</p></td>
					<td><p>Nama</p></td>
				</tr>
				<tr>
					<td width="150"><p>Position / Jabatan</p></td>
					<td width="15"><p>:</p></td>
					<td><p>Security Guard / Anggota</p></td>
				</tr>
				<tr>
					<td width="150"><p style="padding-top: -20px;">Previous Location of Duty / Lokasi Tugas</p></td>
					<td width="15"><p style="padding-top: -20px;">:</p></td>
					<td><p>PT. MATAHARI DEPARTEMENT STORE<br>-</p></td>
				</tr>		
				<tr>
					<td width="160"><p style="padding-top: -20px;">New Location of Duty / Lokasi Tugas Baru</p></td>
					<td width="15"><p style="padding-top: -20px;">:</p></td>
					<td><p>PT. MATTEL INDONESIA<br>-</p></td>
				</tr>
				<tr>
					<td width="160"><p >Address of Duty / Alamat Tugas</p></td>
					<td width="15"><p >:</p></td>
					<td width="160"><p>Jl. Industri Utama Blok SS Kav. 1-3 Cikarang-Bekasi</p></td>
				</tr>
				<tr>
					<td width="150"><p>Starting Date / Mulai Tugas</p></td>
					<td width="15"><p>:</p></td>
					<td><p>May 05,2017</p></td>
				</tr>
				<tr>
					<td width="150"><p>Time of Duty / Jam Tugas</p></td>
					<td width="15"><p>:</p></td>
					<td><p>As instructed / Sesuai perintah</p></td>
				</tr>
				<tr>
					<td width="150"><p>Equipment / Perlengkapan</p></td>
					<td width="15"><p>:</p></td>
					<td><p>PDH & PDL Uniform / Seragam PDH & PDL</p></td>
				</tr>
				<tr>
					<td width="150"><p>Periode of Duty / Masa Tugas</p></td>
					<td width="15"><p>:</p></td>
					<td><p>As long as required / Sampai ada perubahan</p></td>
				</tr>
			</table>
		</div>
		
		<p>to conduct the guarding task at / untuk melaksanakan tugas pengamanan :</p>
		<p>- Report to the Local Leader to receive further instructions.<br>/ Lapor kepada pimpinan setempat untuk mendapat petunjuk-petunjuk lebih lanjut.</p>
		<p>- Perform the duty as well as possible and with responsibility.<br>/ Laksanakan tugas dengan sebaik-baiknya serta penuh rasa tanggung jawab.</p>

		<table style="width: 100%; margin-right: -280px;">
			<tr>
				<td align="right">
					<p>issued in / Dikeluarkan di </p>
				</td>
				<td width="15">
					<p>:</p>
				</td>
				<td>
					<p>Jakarta</p>
				</td>
			</tr>
			<tr>
				<td align="right">
					<p style="margin-right: 22px;">Date / Pada Tanggal </p>
				</td>
				<td width="15">
					<p>:</p>
				</td>
				<td>
					<p>May 04, 2017</p>
				</td>
			</tr>
			<tr>
				<td align="right">
					<hr class="line3">
					<span><p style="margin-top: -20px; margin-right: 10px;"><b>PT. GLOBAL SECONT</b></p></span>
				</td>
			</tr>
		</table>
		<table>
			<tr>
				<td><p><b>Penerima</b></p></td>
				<td><span><img src="assets/img/foto.png" style="width: 150px; height: 170px; position: absolute; margin-left: 90px; border: 1px solid #000;"></span></td>
			</tr>
		</table>
		<br><br><br>
		<table style="width: 100%; margin-top: 50px;">
			<tr>
				<td><p style="padding-top: -15px;"><b>IWAN SUWANDI</b></p></td>
				<td width="200"><p><b><u>SUHERI</u><br>SENIOR ASST. MANAGER HR</b></p></td>
			</tr>
		</table>
		<p style="font-size: 9px;">Cc/Tembusan :</p>
			<li style="font-size: 9px;"><p style="font-size: 9px;">Payroll of PT.GLOBAL SECONT</p></li>
			<li style="font-size: 9px;"><p style="font-size: 9px;">Logistic of PT.GLOBAL SECONT</p></li>
			<li style="font-size: 9px;"><p style="font-size: 9px;">Corporate security of PT DHL EXEL (MUF Cikarang)</p></li>
<br><br>
		<footer>
			<p>Jl. Ciputat Raya No 16, Pondok Pinang - Jakarta Selatan Telp : (021) 29044909 (Hunting) Fax : (021) 29044902</p>
			<p style="letter-spacing: 2.9px; padding-top: -10px;"><b style="color: salmon;">e-mail : contact@globalsecont.com</b><b style="color: lightblue">Website : www.globalsecont.com</b></p>			
		</footer>
	</div>


	
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
	<?php $this->load->view("backend/_partials/js.php") ?>
</body>
</html>