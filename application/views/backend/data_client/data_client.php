<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view("backend/_partials/head.php") ?>
</head>

<body class="hold-transition skin-blue sidebar-mini ">

  <div class="wrapper">
    <?php $this->load->view("backend/_partials/navbar.php") ?>

    <?php $this->load->view("backend/_partials/sidebar.php") ?>

    <div class="content-wrapper">
      <!-- tag link -->
      <?php $this->load->view("backend/_partials/breadcrumb.php") ?>

      <!-- Main content -->
      <section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title"><?php echo $title; ?></h3>
          <div class="box-tools pull-right">
            <div class="pull-right mb-10 hidden-sm hidden-xs">
              <a href="#" class="btn btn-success btn-xs">Export Excel</a>
            </div><!--pull right-->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Nama Client</th>
                  <th>Alamat</th>
                  <th>No Telepon</th>
                  <th>Opsi</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>DHL</td>
                  <td>JAKARTA</td>
                  <td>021123123</td>
                  <td><a href="<?php echo base_url() ?>backend/hrd/detail_client" class="btn btn-xs btn-info"><i class="fa fa-search" data-toggle="tooltip" data-placement="top"
                    title="Cek Data"></i> Cek Data</a></td>
                  </tr>
                </tbody>
              </table>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
        <!-- /.content-wrapper -->
<!-- Footer -->
<?php $this->load->view("backend/_partials/footer.php") ?>
</div>
<!-- ./wrapper -->  

<?php $this->load->view("backend/_partials/modal.php") ?>
<?php $this->load->view("backend/_partials/js.php") ?>

<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable()
  });
</script>

</body>
</html>