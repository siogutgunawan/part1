<!DOCTYPE html>
<html>
<head>
<?php $this->load->view("backend/_partials/head.php") ?>
</head>

<body class="hold-transition skin-blue sidebar-mini ">

<div class="wrapper">
<?php $this->load->view("backend/_partials/navbar.php") ?>

<?php $this->load->view("backend/_partials/sidebar.php") ?>

	<div class="content-wrapper">
        <!-- tag link -->
		<?php $this->load->view("backend/_partials/breadcrumb.php") ?>

    <!-- Main content -->
    <section class="content">
<?php foreach($data_pelamars as $pelamar){ ?>
        <?= form_open(base_url('backend/pelamar/edit/'.$pelamar['data_pelamar_id'])); ?>

      <div class="row">
        <div class="col-md-12">
        <div class="box">
        <div class="box-header with-border">
        <div class="text-center">
            <h3 class="box-title">Check Administrasi & Fisik</h3>
        </div>
        </div>
        <div class="box-body">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">DATA PELAMAR</a></li>
                <li><a href="#tab_2" data-toggle="tab">DATA FISIK PELAMAR</a></li>
                <li><a href="#tab_3" data-toggle="tab">DATA PENDIDIKAN</a></li>
                <li><a href="#tab_4" data-toggle="tab">DATA KELUARGA</a></li>
                <li><a href="#tab_5" data-toggle="tab">PENGALAMAN KERJA</a></li>
                <li><a href="#tab_6" data-toggle="tab">LAIN - LAIN</a></li>
            </ul>
            <div class="tab-content">
            <hr>

<!-- DATA PELAMAR -->

            <div class="tab-pane active" id="tab_1">
                <div class="step1">
                <div class="col-md-12">                                 
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="nama">Nama Lengkap (Sesuai KTP)</label>
                                        <input type="text" name="data_pelamars[nama]" class="form-control" id="nama" placeholder="Nama Lengkap" value="<?= $pelamar['nama_lengkap'] ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="no_id">No. Identitas (KTP / SIM)</label>
                                        <input type="text" class="form-control" id="no_id" name="data_pelamars[no_id]" placeholder="No. Identitas (KTP / SIM)" value="<?= $pelamar['no_id'] ?>">
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-8">
                                        <label for="tempat_lahir">Tempat / Tanggal Lahir</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="tempat_lahir" name="data_pelamars[tempat_lahir]" placeholder="Tempat Lahir" value="<?= $pelamar['tempat_lahir'] ?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="date" class="form-control" id="tanggal_lahir" name="data_pelamars[tanggal_lahir]" placeholder="Tanggal Lahir" value="<?= $pelamar['tanggal_lahir'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="umur">Umur</label>
                                        <input type="text" class="form-control" name="data_pelamars[umur]" id="umur" placeholder="Umur" value="<?= $pelamar['umur'] ?>">
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="jns_kelamin">Jenis Kelamin</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pelamars[jns_kelamin]" value="Pria" <?= ($pelamar['jns_kelamin']=='Pria')?'checked':'' ?>> Pria</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pelamars[jns_kelamin]" value="Wanita" <?= ($pelamar['jns_kelamin']=='Wanita')?'checked':'' ?>> Wanita</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="radio-agama">Agama</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pelamars[agama]" value="Islam"<?= ($pelamar['agama']=='Islam')?'checked':'' ?>> Islam</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pelamars[agama]" value="Kristen"<?= ($pelamar['agama']=='Kristen')?'checked':'' ?>> Kristen</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pelamars[agama]" value="Katolik"<?= ($pelamar['agama']=='Katolik')?'checked':'' ?>> Katolik</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pelamars[agama]" value="Hindu" <?= ($pelamar['agama']=='Hindu')?'checked':'' ?>> Hindu</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pelamars[agama]" value="Budha"<?= ($pelamar['agama']=='Budha')?'checked':'' ?>> Budha</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="alamat_tinggal">Alamat Tinggal</label>
                                        <textarea class="form-control" rows="3" name="data_pelamars[alamat_tinggal]" id="alamat_tinggal"><?= $pelamar['alamat_tinggal'] ?></textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="no_tlp">Telpon Rumah / Ponsel</label>
                                        <input type="text" class="form-control" id="no_tlp" name="data_pelamars[no_tlp]" placeholder="Telpon Rumah / Ponsel" value="<?= $pelamar['no_tlp'] ?>">
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="alamat_tinggal_kecamatan">Kecamatan</label>
                                                <input type="text" class="form-control" id="alamat_tinggal_kecamatan" name="data_pelamars[alamat_tinggal_kecamatan]" placeholder="Kecamatan" value="<?= $pelamar['alamat_tinggal_kecamatan'] ?>">
                                            </div>
                                            <div class="col-md-4">
                                                <label for="kota">Kota</label>
                                                <input type="text" class="form-control" id="alamat_tinggal_kota" name="data_pelamars[alamat_tinggal_kota]" placeholder="Kota" value="<?= $pelamar['alamat_tinggal_kota'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="status_tempat_tinggal">Status Tempat Tinggal</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pelamars[status_tempat_tinggal]" value="Milik Sendiri" <?= ($pelamar['status_tempat_tinggal']=='Milik Sendiri')?'checked':'' ?>> Milik Sendiri</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pelamars[status_tempat_tinggal]" value="Keluarga" <?= ($pelamar['status_tempat_tinggal']=='Keluarga')?'checked':'' ?>> Keluarga</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pelamars[status_tempat_tinggal]" value="Kontrak / Kos"  <?= ($pelamar['status_tempat_tinggal']=='Kontrak / Kos')?'checked':'' ?>> Kontrak / Kos</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pelamars[status_tempat_tinggal]" value="Lainnya"  <?= ($pelamar['status_tempat_tinggal']=='Lainnya')?'checked':'' ?>> Lainnya</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="status_nikah">Status Nikah</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pelamars[status_nikah]" value="Belum Menikah" <?= ($pelamar['status_nikah']=='Belum Menikah')?'checked':'' ?>> Belum Menikah</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pelamars[status_nikah]" value="Menikah" <?= ($pelamar['status_nikah']=='Menikah')?'checked':'' ?>> Menikah</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pelamars[status_nikah]" value="Duda" <?= ($pelamar['status_nikah']=='Duda')?'checked':'' ?>> Duda</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pelamars[status_nikah]" value="Janda" <?= ($pelamar['status_nikah']=='Janda')?'checked':'' ?>> Janda</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="nama_pasangan">Nama Suami / Istri</label>
                                        <input type="text" class="form-control" id="nama_pasangan" name="data_pelamars[nama_pasangan]" placeholder="Nama Suami / Istri" value="<?= $pelamar['nama_pasangan'] ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="tgl_lahir_pasangan">Tanggal Lahir</label>
                                        <input type="date" class="form-control" id="tgl_lahir_pasangan" name="data_pelamars[tgl_lahir_pasangan]" placeholder="Tanggal Lahir" value="<?= $pelamar['tgl_lahir_pasangan'] ?>">
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="nama_anak1">Nama Anak 1</label>
                                        <input type="text" class="form-control" id="nama_anak1" name="data_pelamars[nama_anak1]" placeholder="Nama Anak" value="<?= $pelamar['nama_anak1'] ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="tgl_lahir_anak1">Tanggal Lahir Anak 1</label>
                                        <input type="date" class="form-control" id="tgl_lahir_anak1" name="data_pelamars[tgl_lahir_anak1]" placeholder="Tanggal Lahir" value="<?= $pelamar['tgl_lahir_anak1'] ?>">
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="nama_anak2">Nama Anak 2</label>
                                        <input type="text" class="form-control" id="nama_anak2" name="data_pelamars[nama_anak2]" placeholder="Nama Anak" value="<?= $pelamar['nama_anak2'] ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="tgl_lahir_anak2">Tanggal Lahir Anak 2</label>
                                        <input type="date" class="form-control" id="tgl_lahir_anak2" name="data_pelamars[tgl_lahir_anak2]" placeholder="Tanggal Lahir" value="<?= $pelamar['tgl_lahir_anak2'] ?>">
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="nama_anak3">Nama Anak 3</label>
                                        <input type="text" class="form-control" id="nama_anak3" name="data_pelamars[nama_anak3]" placeholder="Nama Anak" value="<?= $pelamar['nama_anak3'] ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="tgl_lahir_anak3">Tanggal Lahir Anak 3</label>
                                        <input type="date" class="form-control" id="tgl_lahir_anak3" name="data_pelamars[tgl_lahir_anak3]" placeholder="Tanggal Lahir" value="<?= $pelamar['tgl_lahir_anak3'] ?>">
                                    </div>
                                </div>
                                <hr>
                            </div>
                        </div>                                                  
                </div>
<!-- DATA FISIK -->
                <div class="tab-pane" id="tab_2">
                <div class="step1">
                <div class="col-md-12"> 
                        <div class="row">
                                    <div class="col-md-8">
                                        <label for="tinggi_bdn">Tinggi / Berat Badan</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <span>
                                                    <input type="text" class="form-control" id="tinggi_bdn" name="data_fisik_pelamars[tinggi_bdn]" placeholder="Tinggi Badan" value="<?= $pelamar['tinggi_bdn'] ?>">
                                                </span>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="berat_bdn" name="data_fisik_pelamars[berat_bdn]" placeholder="Berat Badan" value="<?= $pelamar['berat_bdn'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="gol_darah">Golongan Darah</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_fisik_pelamars[gol_darah]" value="A" <?= ($pelamar['gol_darah']=='A')?'checked':'' ?>>A</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_fisik_pelamars[gol_darah]" value="B" <?= ($pelamar['gol_darah']=='B')?'checked':'' ?>>B</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_fisik_pelamars[gol_darah]" value="AB" <?= ($pelamar['gol_darah']=='AB')?'checked':'' ?>>AB</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_fisik_pelamars[gol_darah]" value="O" <?= ($pelamar['gol_darah']=='O')?'checked':'' ?>>O</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="warna_kulit">Warna Kulit</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_fisik_pelamars[warna_kulit]" value="Sawo Matang" <?= ($pelamar['warna_kulit']=='Sawo Matang')?'checked':'' ?>> Sawo Matang</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_fisik_pelamars[warna_kulit]" value="Putih" <?= ($pelamar['warna_kulit']=='Putih')?'checked':'' ?>> Putih</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_fisik_pelamars[warna_kulit]" value="Hitam" <?= ($pelamar['warna_kulit']=='Hitam')?'checked':'' ?>> Hitam</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_fisik_pelamars[warna_kulit]" value="Kuning Langsat" <?= ($pelamar['warna_kulit']=='Kuning Langsat')?'checked':'' ?>> Kuning Langsat</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="bentuk_muka">Bentuk Muka</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_fisik_pelamars[bentuk_muka]" value="Oval" <?= ($pelamar['bentuk_muka']=='Oval')?'checked':'' ?>> Oval</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_fisik_pelamars[bentuk_muka]" value="Bulat" <?= ($pelamar['bentuk_muka']=='Bulat')?'checked':'' ?>> Bulat</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_fisik_pelamars[bentuk_muka]" value="Lonjong" <?= ($pelamar['bentuk_muka']=='Lonjong')?'checked':'' ?>> Lonjong</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="warna_mata">Warna Mata</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_fisik_pelamars[warna_mata]" value="Hitam" <?= ($pelamar['warna_mata']=='Hitam')?'checked':'' ?>> Hitam</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_fisik_pelamars[warna_mata]" value="Coklat" <?= ($pelamar['warna_mata']=='Coklat')?'checked':'' ?>> Coklat</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_fisik_pelamars[warna_mata]" value="Lainnya" <?= ($pelamar['warna_mata']=='Lainnya')?'checked':'' ?>> Lainnya</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="jenis_rambut">Jenis Rambut</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_fisik_pelamars[jenis_rambut]" value="Lurus" <?= ($pelamar['jenis_rambut']=='Lurus')?'checked':'' ?>> Lurus</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_fisik_pelamars[jenis_rambut]" value="Ikal" <?= ($pelamar['jenis_rambut']=='Ikal')?'checked':'' ?>> Ikal</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_fisik_pelamars[jenis_rambut]" value="Keriting" <?= ($pelamar['jenis_rambut']=='Keriting')?'checked':'' ?>> Keriting</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_fisik_pelamars[jenis_rambut]" value="Lainnya" <?= ($pelamar['jenis_rambut']=='Menikah')?'Lainnya':'' ?>> Lainnya</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                        </div>
                    </div>
                </div>


<!-- DATA PENDIDIKAN -->
                <div class="tab-pane" id="tab_3">
                    <div class="step1">
                        <div class="col-md-12">
                            <div class="row">
                                    <div class="col-md-12">
                                        <label for="pendidikan_terakhir">Pendidikan Terakhir</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pendidikan_pelamars[pendidikan_terakhir]" value="SMA" <?= ($pelamar['pendidikan_terakhir']=='SMA')?'checked':'' ?>> SMA</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pendidikan_pelamars[pendidikan_terakhir]" value="D3" <?= ($pelamar['pendidikan_terakhir']=='D3')?'checked':'' ?>> D3</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pendidikan_pelamars[pendidikan_terakhir]" value="S1 / Universitas" <?= ($pelamar['pendidikan_terakhir']=='S1 / Universitas')?'checked':'' ?>> S1 / Universitas</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="asal_sekolah">Asal Sekolah</label>
                                                <input type="text" class="form-control" id="asal_sekolah" placeholder="Asal Sekolah" name="data_pendidikan_pelamars[asal_sekolah]" value="<?= $pelamar['asal_sekolah'] ?>">
                                            </div>
                                            <div class="col-md-4">
                                                <label for="kota">Kota</label>
                                                <input type="text" class="form-control" id="kota" placeholder="Kota" name="data_pendidikan_pelamars[kota]" value="<?= $pelamar['kota'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="pendidikan_satpam">Pendidikan Satpam</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pendidikan_pelamars[pendidikan_satpam]" value="Pra - Dasar" <?= ($pelamar['pendidikan_satpam']=='Pra - Dasar')?'checked':'' ?>> Pra - Dasar</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pendidikan_pelamars[pendidikan_satpam]" value="Dasar" <?= ($pelamar['pendidikan_satpam']=='Dasar')?'checked':'' ?>> Dasar</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pendidikan_pelamars[pendidikan_satpam]" value="Lanjutan" <?= ($pelamar['pendidikan_satpam']=='Lanjutan')?'checked':'' ?>> Lanjutan</label>
                                            </div> 
                                            <div class="col-md-2">
                                                <label><input type="radio" class="square-blue" name="data_pendidikan_pelamars[pendidikan_satpam]" value="Tidak Ada" <?= ($pelamar['pendidikan_satpam']=='Tidak Ada')?'checked':'' ?>> Tidak Ada</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <label for="tempat_pendidikan">Tempat Pendidikan</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" id="tempat_pendidikan" placeholder="Tempat Pendidikan" name="data_pendidikan_pelamars[tempat_pendidikan]" value="<?= $pelamar['tempat_pendidikan'] ?>">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label><input type="radio" class="square-blue" name="data_pendidikan_pelamars[sertifikat]" value="1"  <?= ($pelamar['sertifikat']=='1')?'checked':'' ?> >Sertifikat</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div> 
                
<!-- DATA KELUARGA -->
                <div class="tab-pane" id="tab_4">
                    <div class="step1">
                        <div class="col-md-12">
                            <div class="row">
                                    <div class="col-md-4">
                                        <label for="nama_ayah">Nama Ayah</label>
                                        <input type="text" class="form-control" id="nama_ayah" name="data_keluarga_pelamars[nama_ayah]" placeholder="Nama Ayah" value="<?= $pelamar['nama_ayah'] ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="nama_ibu">Nama Ibu</label>
                                        <input type="text" class="form-control" id="nama_ibu" name="data_keluarga_pelamars[nama_ibu]" placeholder="Nama Ibu" value="<?= $pelamar['nama_ibu'] ?>">
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="no_tlp_ortu">No. Telepon (Orang Tua)</label>
                                        <input type="text" class="form-control" id="no_tlp_ortu" name="data_keluarga_pelamars[no_tlp_ortu]" placeholder="No. Telepon" value="<?= $pelamar['no_tlp_ortu'] ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="alamat_ortu">Alamat Orang Tua</label>
                                        <textarea name="data_keluarga_pelamars[alamat_ortu]" id="alamat_ortu" rows="2" class="form-control"placeholder="Alamat Orang Tua"> <?= $pelamar['alamat_ortu'] ?></textarea>
                                    </div>
                                </div>
                                <hr>
                                
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="saudara_terdekat">Saudara Terdekat / Emergency</label>
                                        <input type="text" class="form-control" id="saudara_terdekat" name="data_keluarga_pelamars[saudara_terdekat]" placeholder="Saudara Terdekat / Emergency" value="<?= $pelamar['saudara_terdekat'] ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="no_tlp_saudara">No. Telepon (Saudara Terdekat)</label>
                                        <input type="text" class="form-control" id="no_tlp_saudara" name="data_keluarga_pelamars[no_tlp_saudara]" placeholder="No. Telepon" value="<?= $pelamar['no_tlp_saudara'] ?>">
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="status_anak">Status Anak</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label for="anak_ke">Anak Ke</label>
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control" id="anak_ke" name="data_keluarga_pelamars[anak_ke]" placeholder="Anak Ke" value="<?= $pelamar['anak_ke'] ?>">
                                            </div>
                                            <div class="col-md-1">
                                                <label for="jml_bersaudara">Dari</label>
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control" id="dari" name="data_keluarga_pelamars[jml_bersaudara]" placeholder="Dari" value="<?= $pelamar['jml_bersaudara'] ?>">
                                            </div>
                                            <div class="col-md-3">
                                                <label for="exampleInputEmail1">Bersaudara</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                        </div>
                    </div>
                </div>

<!--  PENGALAMAN KERJA -->
                <div class="tab-pane" id="tab_5">
                    <div class="step1">
                        <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="pengalamankerja1">Tgl/Bln/Thn</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <input type="date" class="form-control" id="kerja_dari1" name="data_lain_pelamars[kerja_dari1]" placeholder="Dari" value="<?= $pelamar['kerja_dari1'] ?>">
                                            </div>
                                            <div class="col-md-1">
                                                <label for="kerja_sampai1">s/d</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="date" class="form-control" id="kerja_sampai1" name="data_lain_pelamars[kerja_sampai1]" placeholder="Sampai" value="<?= $pelamar['kerja_sampai1'] ?>">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" id="kerja_pt1" name="data_lain_pelamars[kerja_pt1]" placeholder="Pekerjaan" value="<?= $pelamar['kerja_pt1'] ?>">
                                            </div>
                                            <div class="col-md-1">
                                                <label for="kerja_jabatan1">Jabatan</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" id="kerja_jabatan1" name="data_lain_pelamars[kerja_jabatan1]" placeholder="Jabatan" value="<?= $pelamar['kerja_jabatan1'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="pengalamankerja2">Tgl/Bln/Thn</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <input type="date" class="form-control" id="kerja_dari2" name="data_lain_pelamars[kerja_dari2]" placeholder="Dari" value="<?= $pelamar['kerja_dari2'] ?>">
                                            </div>
                                            <div class="col-md-1">
                                                <label for="kerja_sampai2">s/d</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="date" class="form-control" id="kerja_sampai2" name="data_lain_pelamars[kerja_sampai2]" placeholder="Sampai" value="<?= $pelamar['kerja_sampai2'] ?>">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" id="kerja_pt2" name="data_lain_pelamars[kerja_pt2]" placeholder="Pekerjaan" value="<?= $pelamar['kerja_pt2'] ?>">
                                            </div>
                                            <div class="col-md-1">
                                                <label for="kerja_jabatan2">Jabatan</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" id="kerja_jabatan2" name="data_lain_pelamars[kerja_jabatan2]" placeholder="Jabatan" value="<?= $pelamar['kerja_jabatan2'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="pengalamankerja3">Tgl/Bln/Thn</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <input type="date" class="form-control" id="kerja_dari3" name="data_lain_pelamars[kerja_dari3]" placeholder="Dari" value="<?= $pelamar['kerja_dari3'] ?>">
                                            </div>
                                            <div class="col-md-1">
                                                <label for="kerja_sampai3">s/d</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="date" class="form-control" id="kerja_sampai3" name="data_lain_pelamars[kerja_sampai3]" placeholder="Sampai" value="<?= $pelamar['kerja_sampai3'] ?>">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" id="kerja_pt3" name="data_lain_pelamars[kerja_pt3]" placeholder="Pekerjaan" value="<?= $pelamar['kerja_pt3'] ?>">
                                            </div>
                                            <div class="col-md-1">
                                                <label for="kerja_jabatan3">Jabatan</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" id="kerja_jabatan3" name="data_lain_pelamars[kerja_jabatan3]" placeholder="Jabatan" value="<?= $pelamar['kerja_jabatan3'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                        </div>
                    </div>
                </div>  

<!-- LAIN LAIN -->
                <div class="tab-pane" id="tab_6">
                    <div class="step1">
                        <div class="col-md-12">
                            <div class="row">
                                    <div class="col-md-12">
                                        <label for="referensi">Referensi</label>
                                        <input type="text" class="form-control" id="referensi" name="data_lain_pelamars[referensi]" placeholder="Referensi" value="<?= $pelamar['referensi'] ?>">
                                    </div>
                                    <div class="col-md-12">
                                        <label for="referensi">Teman Global</label>
                                        <input type="text" class="form-control" id="teman_global" name="data_lain_pelamars[teman_global]" placeholder="teman global" value="<?= $pelamar['teman_global'] ?>">
                                    </div>
                                </div>
                                <hr>
                                <h3><strong>Informasi Tempat Tinggal</strong></h3>
                                <hr>
                                
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="alamat_rumah_tinggal">Alamat Rumah Tinggal Saat Ini</label>
                                            <textarea name="data_lain_pelamars[alamat_tinggal_sekarang]" id="alamat_tinggal_sekarang" rows="2" class="form-control"placeholder="Alamat Rumah Tinggal Saat Ini"><?= $pelamar['alamat_tinggal_sekarang']?></textarea>
                                        </div>
                                        <div class="col-md-8">
                                            <label for="no_tlp_keluarga">No Telepon Keluarga Penting yang dapat dihubungi</label>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" name="data_lain_pelamars[tlp_keluarga1]" class="form-control" placeholder="No. Telepon 1" value="<?= $pelamar['tlp_keluarga1'] ?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" name="data_lain_pelamars[tlp_keluarga2]" class="form-control" placeholder="No. Telepon 2" value="<?= $pelamar['tlp_keluarga2'] ?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" name="data_lain_pelamars[tlp_keluarga3]" class="form-control" placeholder="No. Telepon 3" value="<?= $pelamar['tlp_keluarga3'] ?>">
                                        </div>
                                    </div>
                                

                                <hr>
                                <h3><strong>Informasi Nama, Alamat dan Nomor Telepon Tetangga yang berdomisili disekitar Rumah Anda</strong></h3>
                                <hr>
                                
                                    <h4>Tetangga Sebelah Kiri</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="alamat_tetangga_kiri">Alamat</label>
                                            <textarea name="data_lain_pelamars[alamat_tetangga_kiri]" id="alamat_tetangga_kiri" rows="2" class="form-control"placeholder="Alamat"> <?= $pelamar['alamat_tetangga_kiri'] ?> </textarea>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="nama_tetangga_sebelah_kiri">Nama</label>
                                            <input type="text" name="data_lain_pelamars[nama_tetangga_kiri]" class="form-control" placeholder="Nama" value="<?= $pelamar['nama_tetangga_kiri'] ?>">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="tlp_tetangga_kiri">No Telepon</label>
                                            <input type="text" name="data_lain_pelamars[tlp_tetangga_kiri]" class="form-control" placeholder="No. Telepon" value="<?= $pelamar['tlp_tetangga_kiri'] ?>">
                                        </div>
                                        
                                    </div>
                                
<hr>
                                   
                                    <h4>Tetangga Sebelah Kanan</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="alamat_tetangga_kanan">Alamat</label>
                                            <textarea name="data_lain_pelamars[alamat_tetangga_kanan]" id="alamat_tetangga_kanan" rows="2" class="form-control"placeholder="Alamat"><?= $pelamar['alamat_tetangga_kanan']?></textarea>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="nama_tetangga_kanan">Nama</label>
                                            <input type="text" name="data_lain_pelamars[nama_tetangga_kanan]" class="form-control" placeholder="Nama" value="<?= $pelamar['nama_tetangga_kanan'] ?>">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="tlp_tetangga_kanan">No Telepon</label>
                                            <input type="text" name="data_lain_pelamars[tlp_tetangga_kanan]" class="form-control" placeholder="No. Telepon" value="<?= $pelamar['tlp_tetangga_kanan'] ?>">
                                        </div>
                                        
                                    </div>
                                
<hr>
                                
                                <h4>Tetangga Sebelah Belakang</h4>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="alamat_tetangga_belakang">Alamat</label>
                                        <textarea name="data_lain_pelamars[alamat_tetangga_belakang]" id="alamat_tetangga_belakang" rows="2" class="form-control"placeholder="Alamat"><?= $pelamar['alamat_tetangga_belakang']?></textarea>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="nama_tetangga_belakang">Nama</label>
                                        <input type="text" name="data_lain_pelamars[nama_tetangga_belakang]" class="form-control" placeholder="Nama" value="<?= $pelamar['nama_tetangga_belakang'] ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="tlp_tetangga_belakang">No Telepon</label>
                                        <input type="text" name="data_lain_pelamars[tlp_tetangga_belakang]" class="form-control" placeholder="No. Telepon" value="<?= $pelamar['tlp_tetangga_belakang'] ?>">
                                    </div>
                                     
                                </div>
                                
<hr>
                               
                                    <h4>Tetangga Sebelah Depan</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="alamat_tetangga_depan">Alamat</label>
                                            <textarea name="alamat_tetangga_depan" id="data_lain_pelamars[alamat_tetangga_depan]" rows="2" class="form-control"placeholder="Alamat"><?= $pelamar['alamat_tetangga_depan']?></textarea>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="nama_tetangga_depan">Nama</label>
                                            <input type="text" name="data_lain_pelamars[nama_tetangga_depan]" class="form-control" placeholder="Nama" value="<?= $pelamar['nama_tetangga_depan'] ?>">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="tlp_tetangga_depan">No Telepon</label>
                                            <input type="text" name="data_lain_pelamars[tlp_tetangga_depan]" class="form-control" placeholder="No. Telepon" value="<?= $pelamar['tlp_tetangga_depan'] ?>">
                                        </div>
                                        
                                    </div>
                                
<hr>
                                
                                    <h4>Pejabat Rukun Tetangga di Wilayah Rumah Anda</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="alamat_rt">Alamat</label>
                                            <textarea name="data_lain_pelamars[alamat_rt]" id="alamat_rt" rows="2" class="form-control"placeholder="Alamat"><?= $pelamar['alamat_rt']?></textarea>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="nama_rt">Nama</label>
                                            <input type="text" name="data_lain_pelamars[nama_rt]" class="form-control" placeholder="Nama" value="<?= $pelamar['nama_rt'] ?>">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="tlp_rt">No Telepon</label>
                                            <input type="text" name="data_lain_pelamars[tlp_rt]" class="form-control" placeholder="No. Telepon" value="<?= $pelamar['tlp_rt'] ?>">
                                        </div>
                                        
                                    </div>
<hr>
                        </div>
                    </div>
                </div> 
            

            <div>
                    <td><input type="hidden" name="hidden_id" value="<?= $pelamar['data_pelamar_id']?>" /></td>
                    <td><input type="submit" name="update" value="update" class="btn btn-info"/>  </td>
        
            </div>
                
        </div>
        </div>
        </div>
        </div>

        </div>
<?php  } ?>
<?= form_close(); ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

		 

		<!-- Footer -->
		<?php $this->load->view("backend/_partials/footer.php") ?>
</div>
<!-- ./wrapper -->	

<?php $this->load->view("backend/_partials/modal.php") ?>
<?php $this->load->view("backend/_partials/js.php") ?>

<script>
    $(document).ready(function(){
  $('input[type="checkbox"].square-blue, input[type="radio"].square-blue').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square-blue',
    increaseArea: '10%' // optional
  });
});
</script>

    
</body>
</html>