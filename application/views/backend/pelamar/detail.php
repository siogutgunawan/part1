<!DOCTYPE html>
<html>
<head>
<?php $this->load->view("backend/_partials/head.php") ?>
<style type="text/css">
    .tab-gray { 
      background: #f9f9f9;
      padding: 8px;
      font-size: 16px;
    }
    .tab{
        padding: 8px;
        font-size: 16px;
    }
  </style>
</head>

<body class="hold-transition skin-blue sidebar-mini ">

<div class="wrapper">
<?php $this->load->view("backend/_partials/navbar.php") ?>

<?php $this->load->view("backend/_partials/sidebar.php") ?>

	<div class="content-wrapper">
        <!-- tag link -->
		<?php $this->load->view("backend/_partials/breadcrumb.php") ?>

    <!-- Main content -->
    <section class="content">
<?php foreach($data_pelamars as $pelamar){?>

      <div class="row">
        <div class="col-md-8">
        <div class="box">
        <div class="box-header with-border">
        <div class="text-center">
            <h3 class="box-title">Profil Pelamar</h3>
        </div>
        </div>
        <div class="box-body no-padding">
        <div class="table-responsive">
            <table class="table no-border table-striped">
                    <tr>
                        <td style="width: 180px;">Nama Lengkap (Sesuai KTP)</td>
                        <td width="20">:</td>
                        <td><?= $pelamar['nama_lengkap']; ?></td>
                    </tr>
                    <tr>
                        <td>Tempat Tanggal Lahir</td>
                        <td>:</td>
                        <td><?= $pelamar['tempat_lahir'].' - '.$pelamar['tanggal_lahir']; ?></td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td><?= $pelamar['jns_kelamin']; ?></td>
                    </tr>
                     <tr>
                        <td>No. Identitas</td>
                        <td>:</td>
                        <td><?= $pelamar['no_id']; ?></td>
                    </tr>
                    <tr>
                        <td>Catatan </td>
                        <td>:</td>
                        <td> Tidak Ada Catatan</td>
                    </tr>
                    
            </table>
        </div>
        </div>
        </div>
        </div>

        <div class="col-md-4">
        <div class="box">
        <div class="box-header with-border">
        <div class="text-center">
            <h3 class="box-title">Profil Pelamar</h3>
        </div>
        </div>
        <div class="box-body no-padding">
            <div class="center">
                <img src="<?php echo base_url('assets/img')?>" alt="">
            </div>
            <div>
                <input type="hidden" name="id" value="<?= $pelamar['data_pelamar_id']?>" />
                <a href="<?php echo base_url('backend/pelamar/edit/'.$pelamar['data_pelamar_id']);?>" class="btn btn- btn-warning">
                <i class="fa fa-edit" data-toggle="tooltip" data-placement="top" title="Edit">Edit</i>
            </a> 
            </div>
            
            
            
            
        </div>
        </div>
        </div>

        <div class="col-md-12">
        <div class="box">
        <div class="box-header with-border">
        <div class="text-center">
            <h3 class="box-title">Check Administrasi & Fisik</h3>
        </div>
        </div>
        <div class="box-body">
        <div class="">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">Persyaratan Administrasi</a></li>
                <li><a href="#tab_2" data-toggle="tab">Persyaratan Fisik</a></li>
                <li><a href="#tab_3" data-toggle="tab">Upload File</a></li>
            </ul>
            <div class="tab-content">

            <div class="tab-pane active" id="tab_1">
                                                   
                <div class="table-responsive no-padding" style="margin-top: 10px;">
                    <table class="table no-border table-striped">
                        <tr>
                            <td width="200">Lulus SMA Sederajat</td>
                            <td width="20">:</td>
                            <td width="130"> 
                                <label><input type="radio" class="square-blue" name="data_pelamars[pendidikan_terakhir]" value="YA" <?= ($pelamar['pendidikan_terakhir']=='SMA')?'checked':'' ?>> YA</label>
                                <label><input type="radio" class="square-blue" name="data_pelamars[pendidikan_terakhir]" value="TIDAK" <?= ($pelamar['pendidikan_terakhir']!='SMA')?'checked':'' ?>> TIDAK</label> 
                            </td>
                            <td ><?= $pelamar['pendidikan_terakhir'] ?></td>

                        </tr>
                        <tr>
                            <td>Usia 20 - 35 Tahun</td>
                            <td>:</td>
                            <td><?php if (($pelamar['umur'] >= "20") && ($pelamar['umur'] <= "35")) { ?>
                            <label><input type="radio" class="square-blue" name="radio-umur" value="YA" checked> YA </label>
                            <label><input type="radio" class="square-blue" name="radio-umur" value="TIDAK"> TIDAK</label>
                        <?php }else{ ?>
                            <label><input type="radio" class="square-blue" name="radio-umur" value="YA" > YA </label>
                            <label><input type="radio" class="square-blue" name="radio-umur" value="TIDAK" checked> TIDAK</label> 
                        <?php } ?>
                                </td>

                            <td><?= $pelamar['umur']; ?> Tahun</td>
                        </tr>
                        <tr>
                            <td>Status Belum Menikah</td>
                            <td>:</td>
                            <td>
                            <label><input type="radio" class="square-blue" name="data_pelamars[status_nikah]" value="YA" <?= ($pelamar['pendidikan_terakhir']=='Belum Menikah')?'checked':'' ?>> YA </label>
                            <label><input type="radio" class="square-blue" name="data_pelamars[status_nikah]" value="TIDAK" <?= ($pelamar['pendidikan_terakhir']!='Belum Menikah')?'checked':'' ?>> TIDAK</label>
                        </td>
                            <td ><?= $pelamar['status_nikah'] ?></td>
                        </tr>
                        <tr>
                            <td>Pengalaman Satpam</td>
                            <td>:</td>
                            <td><label><input type="radio" class="square-blue" name="" value="YA" > YA </label>
                            <label><input type="radio" class="square-blue" name="" value="TIDAK" > TIDAK</label></td>
                            <td></td>
                        </tr>

                         <tr>
                            <td>Foto</td>
                            <td>:</td>
                            <td><label><input type="radio" class="square-blue" name="" value="YA" > YA </label>
                            <label><input type="radio" class="square-blue" name="" value="TIDAK" > TIDAK</label> </td>
                            <td></td>

                        </tr>
                        <tr>
                            <td>Sertifikat Satpam</td>
                            <td>:</td>
                            <td><label><input type="radio" class="square-blue" name="" value="YA" > YA </label>
                            <label><input type="radio" class="square-blue" name="" value="TIDAK" > TIDAK</label> </td>
                            <td></td>
                        </tr>
                        
                        <tr>
                            <td>S.K.C.K</td>
                            <td>:</td>
                            <td><label><input type="radio" class="square-blue" name="" value="YA" > YA </label>
                            <label><input type="radio" class="square-blue" name="" value="TIDAK" > TIDAK</label></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Surat Dokter</td>
                            <td>:</td>
                            <td><label><input type="radio" class="square-blue" name="" value="YA" > YA </label>
                            <label><input type="radio" class="square-blue" name="" value="TIDAK" > TIDAK</label></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Surat lamaran Kerja</td>
                            <td>:</td>
                            <td><label><input type="radio" class="square-blue" name="" value="YA" > YA </label>
                            <label><input type="radio" class="square-blue" name="" value="TIDAK" > TIDAK</label></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Daftar Riwayat Hidup</td>
                            <td>:</td>
                            <td><label><input type="radio" class="square-blue" name="" value="YA" > YA </label>
                            <label><input type="radio" class="square-blue" name="" value="TIDAK" > TIDAK</label></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Foto copy KTP</td>
                            <td>:</td>
                            <td><label><input type="radio" class="square-blue" name="" value="YA" > YA </label>
                            <label><input type="radio" class="square-blue" name="" value="TIDAK" > TIDAK</label></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Kartu Keluarga</td>
                            <td>:</td>
                            <td><label><input type="radio" class="square-blue" name="" value="YA" > YA </label>
                            <label><input type="radio" class="square-blue" name="" value="TIDAK" > TIDAK</label></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Surat Keterangan dari Suami(Satpam Wanita)</td>
                            <td>:</td>
                            <td><label><input type="radio" class="square-blue" name="" value="YA" > YA </label>
                            <label><input type="radio" class="square-blue" name="" value="TIDAK" > TIDAK</label></td>
                            <td></td>
                        </tr>
                    
                    </table>
                </div>                                                           
                </div>

                <div class="tab-pane" id="tab_2">
                    <?= form_open(base_url('backend/pelamar/detail/')); ?>
                    <div class="container-fluid" style="margin-top: 10px;">                               
                    <div class="row tab-gray">
                        <div class="col-md-3">
                            Tinggi Badan Minimal 168 CM
                        </div>
                        <div class="col-md-2">
                            <label><input type="radio" class="square-blue" name="" value="YA" <?= ($pelamar['tinggi_bdn'] >'167')?'checked':'' ?>> YA </label>
                            <label><input type="radio" class="square-blue" name="" value="TIDAK" <?= ($pelamar['tinggi_bdn']<'168')?'checked':'' ?>> TIDAK </label>
                        </div>
                        <div class="col-md-2">
                            <?= $pelamar['tinggi_bdn'] ?> CM
                        </div>
                    </div>
                    <div class="row tab">
                        <div class="col-md-3">
                            Berat Seimbang
                        </div>
                        <div class="col-md-2">
                            <label><input type="radio" class="square-blue" name="" value="YA" > YA </label>
                            <label><input type="radio" class="square-blue" name="" value="TIDAK" > TIDAK </label>
                        </div>
                        <div class="col-md-2">
                            <!-- <?= $erat_seimbang ?> -->
                        </div>
                    </div>
                    <div class="row tab-gray" >
                        <div class="col-md-3">
                            Tidak Memiliki Penyakit Kulit 
                        </div>
                        <div class="col-md-2">
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[penyakit_kulit]" 
                                value=1 <?= ($pelamar['penyakit_kulit']=='1')?'checked':'' ?>> YA </label> 
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[penyakit_kulit]" 
                                value=0 <?= ($pelamar['penyakit_kulit']=='0')?'checked':'' ?>> TIDAK </label>
                        </div>
                    </div>    
                    <div class="row tab">
                        <div class="col-md-3">
                            Tidak varises
                        </div>
                        <div class="col-md-2">
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[varises]" value="1" 
                            <?= ($pelamar['varises']=='1')?'checked':'' ?>> YA </label>
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[varises]" value="0" 
                            <?= ($pelamar['varises']=='0')?'checked':'' ?>> TIDAK </label>
                        </div>
                    </div>  
                    <div class="row tab-gray">
                        <div class="col-md-3">
                            Tidak Ambien 
                        </div>
                        <div class="col-md-2">
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[ambien]" value="1" 
                            <?= ($pelamar['ambien']=='1')?'checked':'' ?>> YA </label>
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[ambien]" value="0" 
                            <?= ($pelamar['ambien']=='0')?'checked':'' ?>> TIDAK </label>
                        </div>
                    </div>  
                    <div class="row tab">
                        <div class="col-md-3">
                            Tidak Hernia                   
                        </div>
                        <div class="col-md-2">
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[hernia]" value="1" 
                            <?= ($pelamar['hernia']=='1')?'checked':'' ?>> YA </label>
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[hernia]" value="0" 
                            <?= ($pelamar['hernia']=='0')?'checked':'' ?>> TIDAK </label>
                        </div>
                    </div>    
                    <div class="row tab-gray">
                        <div class="col-md-3">
                            Tidak Tuli 
                        </div>
                        <div class="col-md-2">
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[tuli]" value="1" 
                            <?= ($pelamar['tuli']=='1')?'checked':'' ?>> YA </label>
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[tuli]" value="0" 
                            <?= ($pelamar['tuli']=='0')?'checked':'' ?>> TIDAK </label>
                        </div>
                    </div>
                    <div class="row tab">
                        <div class="col-md-3">
                            Tidak Berkacamata
                        </div>
                        <div class="col-md-2">
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[kacamata]" value="1" 
                            <?= ($pelamar['kacamata']=='1')?'checked':'' ?>> YA </label>
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[kacamata]" value="0" 
                            <?= ($pelamar['kacamata']=='0')?'checked':'' ?>> TIDAK </label>
                        </div>
                    </div>
                    <div class="row tab-gray">
                        <div class="col-md-3">
                            Tidak Bertato
                        </div>
                        <div class="col-md-2">
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[tato]" value="1" 
                            <?= ($pelamar['tato']=='1')?'checked':'' ?>> YA </label>
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[tato]" value="0" 
                            <?= ($pelamar['tato']=='0')?'checked':'' ?>> TIDAK </label>
                        </div>
                    </div>
                    <div class="row tab">
                        <div class="col-md-3">
                            Tidak Bertindik
                        </div>
                        <div class="col-md-2">
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[tindik]" value="1" 
                            <?= ($pelamar['tindik']=='1')?'checked':'' ?>> YA </label>
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[tindik]" value="0" 
                            <?= ($pelamar['tindik']=='0')?'checked':'' ?>> TIDAK </label>
                        </div>
                    </div>
                    <div class="row tab-gray">
                        <div class="col-md-3">
                            Kaki Tidak O atau X
                        </div>
                        <div class="col-md-2">
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[kaki]" value="1" 
                            <?= ($pelamar['kaki']=='1')?'checked':'' ?>> YA </label>
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[kaki]" value="0" 
                            <?= ($pelamar['kaki']=='0')?'checked':'' ?>> TIDAK </label>
                        </div>
                    </div>
                    <div class="row tab">
                        <div class="col-md-3">
                            Tangan Tidak Bengkok
                        </div>
                        <div class="col-md-2">
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[tangan_bengkok]" value="1" 
                                <?= ($pelamar['tangan_bengkok']=='1')?'checked':'' ?>> YA </label>
                            <label><input type="radio" class="square-blue" name="data_cek_fisik[tangan_bengkok]" value="0" 
                                <?= ($pelamar['tangan_bengkok']=='0')?'checked':'' ?>> TIDAK </label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <input type="hidden" name="hidden_id" value="<?= $pelamar['data_pelamar_id']?>" /> 
                            <input type="submit" name="submit_fisik" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="cek fisik" /> 
                            
                        </div>
                    </div>                                
                </div>
                <?= form_close(); ?> 
            </div>
        

                <div class="tab-pane" id="tab_3">
                    <?= form_open_multipart(base_url('backend/pelamar/upload_file/')); ?>
                    <div class="container-fluid" style="margin-top: 10px;">                               
                    <div class="row tab-gray">
                        <div class="col-md-3">
                            Foto
                        </div>
                        <div class="col-md-3">
                            <?php if(!empty($pelamar['foto'])) {?>
                            <input type="submit" name="Lihat" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Lihat Data" value="Lihat" />
                            <a href="<?php echo base_url('backend/pelamar/upload_file/'.$pelamar['data_pelamar_id']) ?>" name ="foto" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Delete Data"> 
                        <?php }else{ ?>
                            <input type="file" name="foto">
                            <input type="hidden" name="old_foto" value="<?= $pelamar['foto'] ?>">
                            <small>Maksimum 2 MB</small>
                        <?php } ?>
                        </div>
                    </div>
                    <div class="row tab">
                        <div class="col-md-3">
                            Sertifikat Satpam
                        </div>
                        <div class="col-md-3">
                            <input type="file" name="sertifikat_satpam"> 
                            <input type="hidden" name="old_sertifikat_satpam" value="<?= $pelamar['sertifikat_satpam'] ?>">
                            <small>Maksimum 2 MB</small>
                        </div>
                    </div>
                    <div class="row tab-gray">
                        <div class="col-md-3">
                            Scan S.K.C.K
                        </div>
                        <div class="col-md-3">
                            <input type="file" name="skck">
                            <input type="hidden" name="old_skck" value="<?= $pelamar['skck'] ?>">    
                            <small>Maksimum 2 MB</small>
                        </div>
                    </div>
                    <div class="row tab">
                        <div class="col-md-3">
                            Surat Dokter
                        </div>
                        <div class="col-md-3">
                            <input type="file" name="surat_dokter"> 
                            <input type="hidden" name="old_surat_dokter" value="<?= $pelamar['surat_dokter'] ?>">
                            <small>Maksimum 2 MB</small>
                        </div>
                    </div>
                    <div class="row tab-gray">
                        <div class="col-md-3">
                            Surat lamaran Kerja
                        </div>
                        <div class="col-md-3">
                            <input type="file" name="surat_lamaran_kerja">
                            <input type="hidden" name="old_surat_lamaran_kerja" value="<?= $pelamar['surat_lamaran_kerja'] ?>"> 
                            <small>Maksimum 2 MB</small>
                        </div>
                    </div>
                    <div class="row tab">
                        <div class="col-md-3">
                            Scan Daftar Riwayat Hidup
                        </div>
                        <div class="col-md-3">
                            <input type="file" name="daftar_riwayat_hidup">
                            <input type="hidden" name="old_daftar_riwayat_hidup" value="<?= $pelamar['daftar_riwayat_hidup'] ?>"> 
                            <small>Maksimum 2 MB</small>
                        </div>
                    </div>
                    <div class="row tab-gray">
                        <div class="col-md-3">
                            Scan KTP
                        </div>
                        <div class="col-md-3">
                            <input type="file" name="scan_ktp">
                            <input type="hidden" name="old_scan_ktp" value="<?= $pelamar['scan_ktp'] ?>"> 
                            <small>Maksimum 2 MB</small>
                        </div>
                    </div>
                    <div class="row tab">
                        <div class="col-md-3">
                            Scan Kartu Keluarga
                        </div>
                        <div class="col-md-3">
                            <input type="file" name="kartu_keluarga">
                            <input type="hidden" name="old_kartu_keluarga" value="<?= $pelamar['kartu_keluarga'] ?>"> 
                            <small>Maksimum 2 MB</small>
                        </div>
                    </div>
                    <div class="row tab-gray">
                        <div class="col-md-3">
                            Surat Keterangan dari Suami(Satpam Wanita)
                        </div>
                        <div class="col-md-3">
                            <input type="file" name="surat_keterangan_suami">
                            <input type="hidden" name="old_surat_keterangan_suami" value="<?= $pelamar['surat_keterangan_suami'] ?>"> 
                            <small>Maksimum 2 MB</small>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <input type="hidden" name="hidden_id" value="<?= $pelamar['data_pelamar_id']?>" /> 
                            <input type="submit" name="upload" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Upload Data" /> 
                            
                        </div>
                    </div>                                
                </div>
                <?= form_close(); ?>
            </div>

            </div>
        </div>
        </div>
        </div>
        </div>

        </div>
<?php  } ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
		 

		<!-- Footer -->
		<?php $this->load->view("backend/_partials/footer.php") ?>
</div>
<!-- ./wrapper -->	

<?php $this->load->view("backend/_partials/modal.php") ?>
<?php $this->load->view("backend/_partials/js.php") ?>

<script>
    $(document).ready(function(){
  $('input[type="checkbox"].square-blue, input[type="radio"].square-blue').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square-blue',
    increaseArea: '20%' // optional
  });
});
</script>

    
</body>
</html>