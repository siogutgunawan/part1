<!DOCTYPE html>
<html>
<head>
<?php $this->load->view("backend/_partials/head.php") ?>
</head>

<body class="hold-transition skin-blue sidebar-mini ">

<div class="wrapper">
<?php $this->load->view("backend/_partials/navbar.php") ?>

<?php $this->load->view("backend/_partials/sidebar.php") ?>

	<div class="content-wrapper">
    
        <!-- tag link -->
		<?php $this->load->view("backend/_partials/breadcrumb.php") ?>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Features</h3>
              
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th width="5">No</th>
            <th>Name</th>
            <th>Position</th>
            <th>Office</th>
            <th>Age</th>
            <th width="10">Action</th>

        </tr>
    </thead>
    <tbody>
    	<?php $i=1; foreach($data_pelamars as $pelamar){ ?>
    <tr>
        <td><?php echo $i ?></td>
        <td><?php echo $pelamar['nama_lengkap']; ?></td>
        <td><?php echo $pelamar['tempat_lahir']; ?></td>
        <td><?php echo $pelamar['tanggal_lahir']; ?></td>
        <td><?php echo $pelamar['no_id']; ?></td>
        <td><a href="<?php echo base_url('backend/pelamar/detail/'.$pelamar['id']);?>" class="btn btn-xs btn-info">
            <i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="Cek Data"></i> Cek Data</a></td>
	</tr>
		<?php $i++; }?>
    </tbody>
</table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box-header-->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
		 

		<!-- Footer -->
		<?php $this->load->view("backend/_partials/footer.php") ?>
</div>
<!-- ./wrapper -->	

<?php $this->load->view("backend/_partials/modal.php") ?>
<?php $this->load->view("backend/_partials/js.php") ?>

<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable()
  });
</script>
    
</body>
</html>