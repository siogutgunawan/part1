<!DOCTYPE html>
<html>
<head>
<?php $this->load->view("backend/_partials/head.php") ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">
<?php $this->load->view("backend/_partials/navbar.php") ?>

<?php $this->load->view("backend/_partials/sidebar.php") ?>

  <div class="content-wrapper">
    
        <!-- tag link -->
    <?php $this->load->view("backend/_partials/breadcrumb.php") ?>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
		<div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="5">No</th>
                  <th>Tanggal</th>
                  <th>No. ID</th>
                  <th>Nama Pegawai</th>
                  <th>Jabatan</th>
                  <th>aksi</th>
                </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>12/12/12</td>
                    <td>00001</td>
                    <td>Nama</td>
                    <td>Anggota</td>
                    <td>
                      <a href="<?php echo base_url('backend/absensi/info_absensi') ?>" class="btn btn-sm btn-success">Info Absensi</a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
	</div>
      <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

          <!-- Footer -->
    <?php $this->load->view("backend/_partials/footer.php") ?>
</div>
<!-- ./wrapper -->  


<?php $this->load->view("backend/_partials/scrolltop.php") ?>
<?php $this->load->view("backend/_partials/modal.php") ?>
<?php $this->load->view("backend/_partials/js.php") ?>
<script src="<?php echo base_url() ?>assets/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
   
  });
</script>
    
</body>
</html>
