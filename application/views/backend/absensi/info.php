<!DOCTYPE html>
<html>
<head>
<?php $this->load->view("backend/_partials/head.php") ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">
<?php $this->load->view("backend/_partials/navbar.php") ?>

<?php $this->load->view("backend/_partials/sidebar.php") ?>

  <div class="content-wrapper">
        <!-- tag link -->
    <?php $this->load->view("backend/_partials/breadcrumb.php") ?>
<br><br>
  <!-- Main content -->
    <section class="content">
       
          <div class="row">
            <div class="col-md-6">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Data Pegawai</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                     <table style="width: 100%;">
                        <tr>
                          <th width="150">No ID</th>
                          <td>123456</td>
                        </tr>
                        <tr>
                          <th width="150">Nama</th>
                          <td>Nama Pegawai</td>
                        </tr>
                        <tr>
                          <th width="150">Jabatan</th>
                          <td>Anggota</td>
                        </tr>
                        <tr>
                          <th width="150">Alamat</th>
                          <td>KOMP. CAKRAWALA III BLOK G NO. 10 RT 004 RW 017 KEL. LAGOA KEC. KOJA</td>
                        </tr>
                        <tr>
                          <th width="150">Lokasi Tugas</th>
                          <td>DHL</td>
                        </tr>
                        <tr>
                          <td><span><img src="<?php echo base_url() ?>assets/img/foto.png" alt="foto" style="width: 150px;height: auto;text-align: center;margin-top: 50px;"></span></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                  <!-- /.row -->
                </div>
                <!-- ./box-body -->
              </div>
              <!-- /.box -->
            </div>
            <!-- /.col1 -->

            <div class="col-md-6">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Absensi</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-8">
                     <a href="#" class="btn btn-success btn-xs"><i class="fa fa-file-excel-o"></i> export excel</a>
                      <a data-toggle="modal" data-target="#modal-input" class="btn btn-info btn-xs">Input Absensi</a>
                      <a data-toggle="modal" data-target="#modal-filter" class="btn btn-danger btn-xs">Filter</a>
                    </div>
                  </div><br>
                  <div class="row">
                    <div class="col-md-12">
                      <table class="table table-bordered table-striped" id="example1">
                      <thead>  
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>Tanggal</th>
                          <th>Nama Pegawai</th>
                          <th>Masuk</th>
                          <th>Keluar</th>
                          <th>aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1.</td>
                          <td>12/12/12</td>
                          <td>Nama</td>
                          <td>00:00</td>
                          <td>00:00</td>
                          <td><a data-toggle="modal" data-target="#modal-edit" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a></td>
                        </tr>
                        <tr>
                          <td>2.</td>
                          <td>12/18/18</td>
                          <td>Nama</td>
                          <td>00:00</td>
                          <td>00:00</td>
                          <td><a data-toggle="modal" data-target="#modal-edit" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a></td>
                        </tr>
                      </tbody>
                    </table>
                    </div>
                  </div>
                  <!-- /.row -->
                </div>
                <!-- ./box-body -->
              </div>
              <!-- /.box -->
            </div>
            <!-- /.col2 -->
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

     <!-- Modal input absensi -->
      <div class="modal fade" id="modal-input" role="dialog">
        <div class="modal-dialog">
          <form action="" method="post">
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <h4 class="modal-title">Input Absensi</h4>
              </div>
              <div class="modal-body">
                
                  <div class="form-horizontal">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Tanggal</label>
                        <div class="col-sm-6">
                          <input type="date" class="form-control" name="tanggalabsen">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Nama Pegawai</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" name="nama" placeholder="Nama Pegawai" value="" readonly="readonly">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Masuk</label>
                        <div class="col-sm-6">
                          <input type="time" class="form-control" name="masuk" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Keluar</label>
                        <div class="col-sm-6">
                          <input type="time" class="form-control" name="keluar" value="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="" data-dismiss="modal" class="btn btn-primary">Close</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      <!-- Close input absensi -->

      <!-- Modal edit absensi -->
      <div class="modal fade" id="modal-edit" role="dialog">
        <div class="modal-dialog">
          <form action="" method="post">
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <h4 class="modal-title">Edit Absensi</h4>
              </div>
              <div class="modal-body">
                
                  <div class="form-horizontal">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Tanggal</label>
                        <div class="col-sm-6">
                          <input type="date" class="form-control" name="tanggalabsen">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Nama Pegawai</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" name="nama" placeholder="Nama Pegawai" value="" readonly="readonly">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Masuk</label>
                        <div class="col-sm-6">
                          <input type="time" class="form-control" name="masuk" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Keluar</label>
                        <div class="col-sm-6">
                          <input type="time" class="form-control" name="keluar" value="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="" data-dismiss="modal" class="btn btn-primary">Close</button>
                </div>
              </div>
            </form>
          </div>      
        </div>
      <!-- Close edit absensi -->

      <!-- Modal edit absensi -->
      <div class="modal fade" id="modal-filter" role="dialog">
        <div class="modal-dialog">
          <form action="" method="post">
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <h4 class="modal-title">Filter Absensi</h4>
              </div>
              <div class="modal-body">
                
                  <div class="form-horizontal">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Periode</label>
                        <div class="col-sm-3">
                          <input type="date" class="form-control" name="periode1">
                        </div>
                        <div class="col-sm-3">
                          <input type="date" class="form-control" name="periode2">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="" data-dismiss="modal" class="btn btn-primary">Close</button>
                </div>
              </div>
            </form>
          </div>      
        </div>
      <!-- Close edit absensi -->

    <!-- Footer -->
    <?php $this->load->view("backend/_partials/footer.php") ?>
</div>
<!-- ./wrapper -->  

<?php $this->load->view("backend/_partials/scrolltop.php") ?>
<?php $this->load->view("backend/_partials/modal.php") ?>
<?php $this->load->view("backend/_partials/js.php") ?>
<script src="<?php echo base_url() ?>assets/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
   
  });
</script>
    
</body>
</html>