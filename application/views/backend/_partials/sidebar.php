 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url() ?>assets/backend/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li class="active"><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li>
<!-- Administrasi cek -->
         <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Cek Administrasi </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= base_url(); ?>backend/pelamar"><i class="fa fa-circle-o"></i> Data Pelamar 
              <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span></a></li>
          </ul>
        </li>

<!-- HRD -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>HRD </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= base_url(); ?>backend/data_pegawai"><i class="fa fa-circle-o"></i> Data Pelamar 
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span></a></li>


            <li><a href="<?= base_url(); ?>backend/data_pegawai"><i class="fa fa-circle-o"></i> Data Pegawai 

             <li><a href="<?= base_url(); ?>backend/absensi"><i class="fa fa-circle-o"></i> Absensi 
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span></a></li>

            <li><a href="<?= base_url(); ?>surat/surat"><i class="fa fa-circle-o"></i> Data Surat 
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span></a></li>

            <li><a href="<?php echo base_url(); ?>backend/hrd/data_client"><i class="fa fa-circle-o"></i> Data Client </a></li>
          </ul>
        </li>

<!-- Logistik -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Logistik </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>backend/logistik"><i class="fa fa-circle-o"></i> Data Calon Pelamar </a></li>
            <li><a href="<?php echo base_url(); ?>backend/logistik/data_barang"><i class="fa fa-circle-o"></i> Data Barang</a></li>
            <li><a href="<?php echo base_url(); ?>backend/logistik/daily_stock_product"><i class="fa fa-circle-o"></i> Stock Harian Barang</a></li>
          </ul>
        </li>

<!-- Finance -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Finance </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php base_url(); ?>backend/management_administrasi"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
            <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
          </ul>
        </li>

<!-- Management Users -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Management Users </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php base_url(); ?>backend/management_administrasi"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
            <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
          </ul>
        </li>
</ul>
</section>
<!-- /.sidebar -->
</aside>






  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
