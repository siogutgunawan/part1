<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view("backend/_partials/head.php") ?>
</head>

<body class="hold-transition skin-blue sidebar-mini ">

  <div class="wrapper">
    <?php $this->load->view("backend/_partials/navbar.php") ?>

    <?php $this->load->view("backend/_partials/sidebar.php") ?>

    <div class="content-wrapper">
      <!-- tag link -->
      <?php $this->load->view("backend/_partials/breadcrumb.php") ?>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-info">
              <div class="box-header">
                <h3 class="box-title"><?php echo $title; ?></h3>
                <div class="box-tools pull-right">
                  <div class="pull-right mb-10 hidden-sm hidden-xs">
                    <a type="button" data-toggle="modal" data-target="#modal-tambah" class="btn btn-primary btn-xs">Tambah Barang</a>
                    <a data-toggle="modal" data-target="#modal-keluar" class="btn btn-danger btn-xs">Keluar Barang</a>
                    <a data-toggle="modal" data-target="#modal-masuk" class="btn btn-success btn-xs">Masuk Barang</a>
                    <a href="#" class="btn btn-warning btn-xs">Export Excel</a>
                  </div><!--pull right-->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Kode Barang</th>
                        <th>Nama Barang</th>
                        <th>Category</th>
                        <th>Harga</th>
                        <th>Stock Qty</th>
                        <th>Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $i=1; 
                      foreach ($data_barang->result() as $key) :?>
                      <tr>
                        <td><?= $i++ ?></td>
                        <td><?= $key->kode_barang; ?></td>
                        <td><?= $key->nama_barang; ?></td>
                        <td><?= $key->nama_category; ?></td>
                        <td><?= $key->harga_satuan; ?></td>
                        <td><?= $key->stok_qty; ?></td>
                        <td><a href="<?php echo base_url('backend/logistik/data_barang') ?>/<?= $key->id;  ?>" class="btn btn-xs btn-info"><i class="fa fa-search" data-toggle="tooltip" data-placement="top"
                    title="Cek Data"></i> Cek Data</a></td>
                      </tr>
                    <?php endforeach; ?>
                    </tbody>
                  </table>

                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <!-- Modal tambah Barang -->
      <div class="modal fade" id="modal-tambah" role="dialog">
        <div class="modal-dialog">
          <form action="<?php echo site_url('backend/logistik/masuk_barang'); ?>" method="post">
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <h4 class="modal-title">Tambah Data</h4>
              </div>
              <div class="modal-body">
                
                  <div class="form-horizontal">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Kode Barang</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" name="kodebarang" placeholder="Kode Barang" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Nama Barang</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" name="namabarang" placeholder="Nama Barang" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Kategory Barang</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" name="kategorybarang" placeholder="Kategory Barang" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Ukuran</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" name="ukuran" placeholder="Ukuran" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Harga</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" name="harga" placeholder="Harga" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Qty</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" name="qty" placeholder="Qty" value="">
                        </div>
                      </div>
                    </div>
                  </div>
                </di>
              </div>
              <div class="modal-footer">
                <button type="submit" name="submit" value="Submit" class="btn btn-primary">Submit</button>
                <button type="" data-dismiss="modal" class="btn btn-primary">Close</button>
              </div>
            </div>
          </form>
        </div>
      </div>
       <!-- Close Modal Tambah -->

      <!-- Modal keluar Barang -->
      <div class="modal fade" id="modal-keluar" role="dialog">
        <div class="modal-dialog">
          <form action="" method="post">
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <h4 class="modal-title">Keluar Barang</h4>
              </div>
              <div class="modal-body">
                
                  <div class="form-horizontal">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Kode Barang</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" name="kodebarang" placeholder="Kode Barang" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Qty</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" name="qty" placeholder="Qty" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Tujuan</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" name="tujuan" placeholder="Tujuan" value="">
                        </div>
                      </div>
                    </div>
                  </div>
                </di>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="" data-dismiss="modal" class="btn btn-primary">Close</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- Close Modal Keluar -->

      <!-- Modal keluar Barang -->
      <div class="modal fade" id="modal-masuk" role="dialog">
        <div class="modal-dialog">
          <form action="" method="post">
            <div class="modal-content">
              <div class="modal-header bg-primary">
                <h4 class="modal-title">Masuk Barang</h4>
              </div>
              <div class="modal-body">
                
                  <div class="form-horizontal">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Kode Barang</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" name="kodebarang" placeholder="Kode Barang" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Qty</label>
                        <div class="col-sm-6">
                          <input type="text" class="form-control" name="qty" placeholder="Qty" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="" class="col-md-4 control-label">Keterangan</label>
                        <div class="col-sm-8">
                          <input type="textarea" class="form-control" name="Keterangan" placeholder="Keterangan" value="">
                        </div>
                      </div>
                    </div>
                  </div>
                </di>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="" data-dismiss="modal" class="btn btn-primary">Close</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- Close Modal Masuk -->
      <!-- Footer -->
      <?php $this->load->view("backend/_partials/footer.php") ?>
    </div>
    <!-- ./wrapper -->	

    <?php $this->load->view("backend/_partials/modal.php") ?>
    <?php $this->load->view("backend/_partials/js.php") ?>

    <!-- page script -->
    <script>
      $(function () {
        $("#example1").DataTable()
      });
    </script>

  </body>
  </html>