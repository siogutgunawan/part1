<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view("backend/_partials/head.php") ?>
</head>

<body class="hold-transition skin-blue sidebar-mini ">

  <div class="wrapper">
    <?php $this->load->view("backend/_partials/navbar.php") ?>

    <?php $this->load->view("backend/_partials/sidebar.php") ?>

    <div class="content-wrapper">
      <!-- tag link -->
      <?php $this->load->view("backend/_partials/breadcrumb.php") ?>
<br><br>
      <!-- Main content -->
      <section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-info">
        <div class="box-header">
          <h3 class="box-title"><?php echo $title; ?></h3>
          <div class="box-tools pull-right">
            <div class="pull-right mb-10 hidden-sm hidden-xs">
              <a href="#" class="btn btn-success btn-xs">Export Excel</a>
            </div><!--pull right-->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nama Calon</th>
                  <th>Ukuran Seragam</th>
                  <th>Ukuran Sepatu</th>
                  <th>Atribut</th>
                  <th>Tanggal</th>
                  <th>Opsi</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Trident</td>
                  <td>Internet
                    Explorer 4.0
                  </td>
                  <td>Win 95+</td>
                  <td> 4</td>
                  <td>X</td>
                  <td><a href="<?php echo base_url() ?>backend/logistik/data_logistik_calon" class="btn btn-xs btn-info"><i class="fa fa-search" data-toggle="tooltip" data-placement="top"
                    title="Cek Data"></i> Cek Data</a></td>
                  </tr>
                </tbody>
              </table>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
        <!-- /.content-wrapper -->
<!-- Footer -->
<?php $this->load->view("backend/_partials/footer.php") ?>
</div>
<!-- ./wrapper -->  

<?php $this->load->view("backend/_partials/modal.php") ?>
<?php $this->load->view("backend/_partials/js.php") ?>

<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable()
  });
</script>

</body>
</html>