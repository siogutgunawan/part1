<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view("backend/_partials/head.php") ?>
</head>

<body class="hold-transition skin-blue sidebar-mini ">

  <div class="wrapper">
    <?php $this->load->view("backend/_partials/navbar.php") ?>

    <?php $this->load->view("backend/_partials/sidebar.php") ?>

    <div class="content-wrapper">
      <!-- tag link -->
      <?php $this->load->view("backend/_partials/breadcrumb.php") ?>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title"><?php echo $title; ?></h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form">
                <div class="box-body">
                  <div class="form-group">
                    <label for="">Nama Calon Security</label>
                    <div class="row">
                    <div class="col-md-6">
                    <span>  
                    <input type="text" class="form-control" name="" id="">
                    </span>  
                    </div>  
                    </div>
                  </div>
                  <div class="form-group">  
                    <label for="">UKURAN SERAGAM</label>
                  </div>
                  
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" class="flat-red"> M
                    </label>
                    <label>
                      <input type="checkbox" class="flat-red"> L
                    </label>
                    <label>
                      <input type="checkbox" class="flat-red"> XL
                    </label>
                    <label>
                      <input type="checkbox" class="flat-red"> XXL
                    </label>
                  </div>
                  <br><br>
                  <div class="form-group">
                    <label for="">UKURAN SEPATU</label>
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" class="flat-red"> 39
                    </label>
                    <label>
                      <input type="checkbox" class="flat-red"> 40
                    </label>
                    <label>
                      <input type="checkbox" class="flat-red"> 41
                    </label>
                    <label>
                      <input type="checkbox" class="flat-red"> 42
                    </label>
                    <label>
                      <input type="checkbox" class="flat-red"> 43
                    </label>
                    <label>
                      <input type="checkbox" class="flat-red"> Lainnya
                    </label>
                  </div>
                  <br><br>
                  <div class="form-group">
                    <label for="exampleInputFile">ATRIBUT</label>
                  </div>
                  <div class="checkbox">
                    <div class="form-group">
                      <label class="col-md-3">Kopel Rim</label>
                    </div>
                    <label>
                      <input type="checkbox" class="flat-red"> YA
                    </label>
                    <label for="">
                      <input type="checkbox" class="flat-red"> TIDAK
                    </label>
                  </div>

                  <div class="checkbox">
                    
                    <div class="form-group">
                      <label class="col-md-3">Tali Kur</label>
                    </div>
                    <label>
                      <input type="checkbox" class="flat-red"> YA
                    </label>
                    <label for="">
                      <input type="checkbox" class="flat-red"> TIDAK
                    </label>
                  </div>
                  <div class="checkbox">
                    
                    <div class="form-group">
                      <label class="col-md-3">Peluit</label>
                    </div>
                    <label>
                      <input type="checkbox" class="flat-red"> YA
                    </label>
                    <label for="">
                      <input type="checkbox" class="flat-red"> TIDAK
                    </label>
                  </div>
                  <div class="checkbox">
                    
                    <div class="form-group">
                      <label class="col-md-3">Ikat Pinggang</label>
                    </div>
                    <label>
                      <input type="checkbox" class="flat-red"> YA
                    </label>
                    <label for="">
                      <input type="checkbox" class="flat-red"> TIDAK
                    </label>

                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button class="btn btn-danger">Back</button>
                </div>
              </form>
            </div>

          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- Footer -->
<?php $this->load->view("backend/_partials/footer.php") ?>
</div>
<!-- ./wrapper -->	

<?php $this->load->view("backend/_partials/modal.php") ?>
<?php $this->load->view("backend/_partials/js.php") ?>

<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable()
  });
</script>

</body>
</html>