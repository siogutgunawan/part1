<!DOCTYPE html>
<html>
<head>
<?php $this->load->view("backend/_partials/head.php") ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">
<?php $this->load->view("backend/_partials/navbar.php") ?>

<?php $this->load->view("backend/_partials/sidebar.php") ?>

	<div class="content-wrapper">
        <!-- tag link -->
		<?php $this->load->view("backend/_partials/breadcrumb.php") ?>

	<!-- Main content -->
    <section class="content">
			 
		      <div class="row">
		        <div class="col-md-5">
		          <div class="box">
		            <div class="box-header with-border">
		              <h3 class="box-title">Info Pegawai</h3>
		            </div>
		            <!-- /.box-header -->
		            <div class="box-body">
		              <div class="row">
                    <div class="col-md-12">
                      <table style="width: 100%;">
                        <tr>
                          <th width="150">No ID</th>
                          <td>123456</td>
                        </tr>
                        <tr>
                          <th width="150">Nama</th>
                          <td>Nama Pegawai</td>
                        </tr>
                        <tr>
                          <th width="150">Jabatan</th>
                          <td>Anggota</td>
                        </tr>
                        <tr>
                          <th width="150">Alamat</th>
                          <td>KOMP. CAKRAWALA III BLOK G NO. 10 RT 004 RW 017 KEL. LAGOA KEC. KOJA</td>
                        </tr>
                        <tr>
                          <th width="150">Lokasi Tugas</th>
                          <td>DHL</td>
                        </tr>
                        <tr>
                          <td><span><img src="<?php echo base_url() ?>assets/img/foto.png" alt="foto" style="width: 150px;height: auto;text-align: center;margin-top: 50px;"></span></td>
                        </tr>
                      </table>
                    </div>

		                </div>
		              <!-- /.row -->
		            </div>
		            <!-- ./box-body -->
		          </div>
		          <!-- /.box -->
		        </div>
		        <!-- /.col1 -->

            <div class="col-md-7">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Surat Keterangan Kerja</a></li>
              <li><a href="#tab_2" data-toggle="tab">Surat Perintah Tugas</a></li>
              <li><a href="#tab_3" data-toggle="tab">Surat Perintah Mutasi Tugas</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <form action="">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-6">
                      <label>No Surat</label>
                      <input type="number" name="no_skk" class="form-control" placeholder="No Surat">

                      <label>Jenis Surat</label>
                      <input type="text" name="jenis_surat" class="form-control" value="skk" readonly="readonly">

                      <label>No KTP</label>
                      <input type="number" name="no_ktp" class="form-control" placeholder="No KTP">

                      <label>Alamat</label>
                      <textarea name="alamat" class="form-control" cols="5" rows="5"></textarea>

                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                     <label>Nama</label>
                      <input type="text" name="nama" class="form-control" placeholder="Nama">

                      <label>Tempat Lahir</label>
                      <input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir">

                    <label>Tanggal Lahir</label>
                      <input type="date" name="tangga_lahir" class="form-control">

                    <label>Jabatan</label>
                    <input type="text" name="jabata" class="form-control" placeholder="Jabatan">

                    <label>Lokasi Tugas</label>
                      <input type="text" name="lokasi_tugas" class="form-control" placeholder="Lokasi Tugas">
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
                <!-- ./box-body -->

                <div class="box-footer">
                  <div class="row">
                    <div class="col-md-6">
                      <button class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                  <!-- /.row -->
                </div>
                <!-- /.box-footer -->
              </form>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <form action="">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-6">

                      <label>No Surat</label>
                      <input type="number" name="no_spt" class="form-control" placeholder="No Surat">

                      <label>Jenis Surat</label>
                      <input type="text" name="jenis_surat" class="form-control" value="spt" readonly="readonly">

                      <label>Nama</label>
                      <input type="text" name="nama" class="form-control" placeholder="Nama">
                 
                      <label>Position / Jabatan</label>
                      <input type="text" name="jabatan_lama" class="form-control" placeholder="Position / Jabatan">

                      <label>Alamat</label>
                      <textarea name="alamat" class="form-control" cols="5" rows="5"></textarea>

                      <label>Time of Duty / Jam Tugas</label>
                      <input type="text" name="jam_tugas" class="form-control" placeholder="Time of Duty / Jam Tugas">

                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                    
                      <label>Starting Date / Mulai Tugas</label>
                      <input type="date" name="mulai_tugas" class="form-control">

                      <label>Periode of Duty / Masa Tugas</label>
                      <input type="text" name="masa_tugas" class="form-control" placeholder="Periode of Duty / Masa Tugas">

                      <label>Previous Location of Duty / Lokasi Tugas</label>
                      <input type="text" name="lokasi_tugas" class="form-control" placeholder="Previous Location of Duty / Lokasi Tugas">

                      <label>Alamat Tugas</label>
                      <textarea name="alamat_tugas" class="form-control" cols="5" rows="4"></textarea>

                      <label>Equipment / Perlengkapan</label>
                      <input type="text" name="perlengkapan" class="form-control" placeholder="Equipment / Perlengkapan">
                      
                      <label>Upload foto</label>
                      <input type="file" name="foto" class="form-control">

                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
                <!-- ./box-body -->

                <div class="box-footer">
                  <div class="row">
                    <div class="col-md-6">
                      <button class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                  <!-- /.row -->
                </div>
                <!-- /.box-footer -->
              </form>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                <form action="">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-6">

                      <label>No Surat</label>
                      <input type="number" name="no_spmt" class="form-control" placeholder="No Surat">

                      <label>Jenis Surat</label>
                      <input type="text" name="jenis_surat" class="form-control" value="spmt" readonly="readonly">

                      <label>Nama</label>
                      <input type="text" name="nama" class="form-control" placeholder="Nama">
                  <br>
                  <p>Data Lama</p>    
                  <hr style="margin-top: -10px;">
                      <label>Position / Jabatan</label>
                      <input type="text" name="jabatan_lama" class="form-control" placeholder="Position / Jabatan">

                      <label>Previous Location of Duty / Lokasi Tugas</label>
                      <input type="text" name="lokasi_lama" class="form-control" placeholder="Previous Location of Duty / Lokasi Tugas">
                                     
                      <label>Starting Date / Mulai Tugas</label>
                      <input type="date" name="mulai_tugas" class="form-control">

                      <label>Time of Duty / Jam Tugas</label>
                      <input type="text" name="jam_tugas" class="form-control" placeholder="Time of Duty / Jam Tugas">

                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                     <p>Data Baru</p>    
                  <hr style="margin-top: -10px;">
                      <label>Position / Jabatan</label>
                      <input type="text" name="jabatan_baru" class="form-control" placeholder="Position / Jabatan">

                      <label>Periode of Duty / Masa Tugas</label>
                      <input type="text" name="masa_tugas" class="form-control" placeholder="Periode of Duty / Masa Tugas">

                      <label>Previous Location of Duty / Lokasi Tugas</label>
                      <input type="text" name="lokasi_baru" class="form-control" placeholder="Previous Location of Duty / Lokasi Tugas">

                      <label>Alamat Tugas</label>
                      <textarea name="alamat" class="form-control" cols="5" rows="5"></textarea>

                      <label>Equipment / Perlengkapan</label>
                      <input type="text" name="perlengkapan" class="form-control" placeholder="Equipment / Perlengkapan">

                      <label>Upload foto</label>
                      <input type="file" name="foto" class="form-control">

                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
                <!-- ./box-body -->

                <div class="box-footer">
                  <div class="row">
                    <div class="col-md-6">
                      <button class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                  <!-- /.row -->
                </div>
                <!-- /.box-footer -->
              </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col2 -->
		      </div>
		      <!-- /.row -->
		    </section>
		    <!-- /.content -->
		  </div>
		  <!-- /.content-wrapper -->
		 

		<!-- Footer -->
		<?php $this->load->view("backend/_partials/footer.php") ?>
</div>
<!-- ./wrapper -->	

<?php $this->load->view("backend/_partials/scrolltop.php") ?>
<?php $this->load->view("backend/_partials/modal.php") ?>
<?php $this->load->view("backend/_partials/js.php") ?>
    
</body>
</html>