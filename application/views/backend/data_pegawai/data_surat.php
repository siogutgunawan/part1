<!DOCTYPE html>
<html>
<head>
<?php $this->load->view("backend/_partials/head.php") ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">
<?php $this->load->view("backend/_partials/navbar.php") ?>

<?php $this->load->view("backend/_partials/sidebar.php") ?>

  <div class="content-wrapper">
        <!-- tag link -->
    <?php $this->load->view("backend/_partials/breadcrumb.php") ?>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Surat Pegawai</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="5">No</th>
                  <th>No. Surat</th>
                  <th>Nama</th>
                  <th>Jabatan</th>
                  <th>Lokasi Tugas</th>
                  <th>Jenis Surat</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>123456</td>
                    <td>Nama</td>
                    <td>Anggota</td>
                    <td>DHL</td>
                    <td>SKK</td>
                    <td>
                      <a href="<?php echo base_url('backend/data_pegawai/detail') ?>" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                      <a href="<?php echo base_url('backend/data_pegawai/detail') ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                      <a href="<?php echo base_url('surat/surat/surat_keterangan_kerja') ?>" class="btn btn-info btn-sm"><i class="fa fa-file-pdf-o"></i></a>
                    </td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>123456</td>
                    <td>Nama</td>
                    <td>Anggota</td>
                    <td>DHL</td>
                    <td>SPT</td>
                    <td>
                      <a href="<?php echo base_url('backend/data_pegawai/detail') ?>" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                      <a href="<?php echo base_url('backend/data_pegawai/detail') ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                      <a href="<?php echo base_url('surat/surat/surat_perintah_tugas') ?>" class="btn btn-info btn-sm"><i class="fa fa-file-pdf-o"></i></a>
                    </td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>123456</td>
                    <td>Nama</td>
                    <td>Anggota</td>
                    <td>DHL</td>
                    <td>SPMT</td>
                    <td>
                      <a href="<?php echo base_url('backend/data_pegawai/detail') ?>" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                      <a href="<?php echo base_url('backend/data_pegawai/detail') ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                      <a href="<?php echo base_url('surat/surat/surat_perintah_mutasi_tugas') ?>" class="btn btn-info btn-sm"><i class="fa fa-file-pdf-o"></i></a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box-header-->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

          <!-- Footer -->
    <?php $this->load->view("backend/_partials/footer.php") ?>
</div>
<!-- ./wrapper -->  


<?php $this->load->view("backend/_partials/modal.php") ?>
<?php $this->load->view("backend/_partials/js.php") ?>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
    
</body>
</html>
