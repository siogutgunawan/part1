<!-- nav -->
    <div class="container">
        <div class="row">
            <section>
            <div class="wizard">
                <div class="wizard-inner" style="margin:20px">
                    <div class="connecting-line"></div>
                    <ul class="nav nav-tabs" role="tablist">

                        <li role="presentation" class="active">
                            <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="DATA PELAMAR">
                                <span class="round-tab">
                                    <i class="fa fa-address-card"></i>
                                </span>
                            </a>
                        </li>

                        <li role="presentation" class="disabled">
                            <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="DATA FISIK PELAMAR">
                                <span class="round-tab">
                                    <i class="fa fa-plus-square"></i>
                                </span>
                            </a>
                        </li>
                        <li role="presentation" class="disabled">
                            <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Data Pendidikan">
                                <span class="round-tab">
                                    <i class="fa fa-university"></i>
                                </span>
                            </a>
                        </li>

                        <li role="presentation" class="disabled">
                            <a href="#step4" data-toggle="tab" aria-controls="complete" role="tab" title="Data Keluarga">
                                <span class="round-tab">
                                    <i class="fa fa-users"></i>
                                </span>
                            </a>
                        </li>
                        <li role="presentation" class="disabled">
                            <a href="#step5" data-toggle="tab" aria-controls="complete" role="tab" title="Document Upload">
                                <span class="round-tab">
                                    <i class="fa fa-upload"></i>
                                </span>
                            </a>
                        </li>
                        <li role="presentation" class="disabled">
                            <a href="#step6" data-toggle="tab" aria-controls="complete" role="tab" title="Pengalaman Kerja">
                                <span class="round-tab">
                                    <i class="fa fa-clipboard"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div style="margin:20px;">
                <!-- end nav -->

                <!-- isi -->
                <?php echo form_open(base_url('frontend/formpelamar/formpost')); ?>
<!-- data pelamar -->

                    <div class="tab-content">
                        <div class="tab-pane active" role="tabpanel" id="step1">
                            <h3><strong>DATA PELAMAR</strong></h3>
                            <hr>
                            <div class="step1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="nama">Nama Lengkap (Sesuai KTP)</label>
                                        <input type="text" name="data_pelamars[nama]" class="form-control" id="nama" placeholder="Nama Lengkap">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <label for="tempat_lahir">Tempat / Tanggal Lahir</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="tempat_lahir" name="data_pelamars[tempat_lahir]" placeholder="Tempat Lahir">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="date" class="form-control" id="tanggal_lahir" name="data_pelamars[tanggal_lahir]" placeholder="Tanggal Lahir">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="umur">Umur</label>
                                        <input type="text" class="form-control" name="data_pelamars[umur]" id="umur" placeholder="Umur">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="jns_kelamin">Jenis Kelamin</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" onclick="changeInputValue(this);" id="jns_kelamin_pria" data-select='jns_kelamin' name="radio-jns-kelamin" value="Pria">Pria</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" onclick="changeInputValue(this);" data-select="jns_kelamin" id="jns_kelamin_wanita" name="radio-jns-kelamin" value="Wanita">Wanita</label>
                                            </div>
                                            <input type="text" style="display: none" id="jns_kelamin" name="data_pelamars[jns_kelamin]"></input>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="radio-agama">Agama</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" data-select="agama" name="radio-agama" value="Islam" onclick="changeInputValue(this);">Islam</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" data-select="agama" name="radio-agama" value="Kristen" onclick="changeInputValue(this);">Kristen</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" data-select="agama" name="radio-agama" value="Katolik" onclick="changeInputValue(this);">Katolik</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-agama" value="Hindu" data-select="agama" onclick="changeInputValue(this);">Hindu</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" data-select="agama" name="radio-agama" value="Budha" onclick="changeInputValue(this);">Budha</label>
                                            </div>
                                            <input type="text" style="display: none" id="agama" name="data_pelamars[agama]"></input>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="alamat_tinggal">Alamat Tinggal</label>
                                        <textarea class="form-control" rows="5" name="data_pelamars[alamat_tinggal]" id="alamat_tinggal"></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="alamat_tinggal_kecamatan">Kecamatan</label>
                                                <input type="text" class="form-control" id="alamat_tinggal_kecamatan" name="data_pelamars[alamat_tinggal_kecamatan]" placeholder="Kecamatan">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail1">Kota</label>
                                                <input type="text" class="form-control" id="alamat_tinggal_kota" name="data_pelamars[alamat_tinggal_kota]" placeholder="Kota">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="status_tempat_tinggal">Status Tempat Tinggal</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="status_tempat_tinggal" value="Milik Sendiri" data-select="status_tempat_tinggal" onclick="changeInputValue(this);">Milik Sendiri</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="status_tempat_tinggal" value="Keluarga" data-select="status_tempat_tinggal" onclick="changeInputValue(this);">Keluarga</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="status_tempat_tinggal" value="Kontrak / Kos" data-select="status_tempat_tinggal" onclick="changeInputValue(this);">Kontrak / Kos</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="status_tempat_tinggal" value="Lainnya" data-select="status_tempat_tinggal" onclick="changeInputValue(this);">Lainnya</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" style="display: none" id="status_tempat_tinggal" name="data_pelamars[status_tempat_tinggal]"></input>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="no_tlp">Telpon Rumah / Ponsel</label>
                                        <input type="text" class="form-control" id="no_tlp" name="data_pelamars[no_tlp]" placeholder="Telpon Rumah / Ponsel">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="no_id">No. Identitas (KTP / SIM)</label>
                                        <input type="text" class="form-control" id="no_id" name="data_pelamars[no_id]" placeholder="No. Identitas (KTP / SIM)">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="status_nikah">Status Nikah</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-nikah" value="Belum Menikah" data-select="status_nikah" onclick="changeInputValue(this);">Belum Menikah</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-nikah" value="Menikah" data-select="status_nikah" onclick="changeInputValue(this);">Menikah</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-nikah" value="Duda" data-select="status_nikah" onclick="changeInputValue(this);">Duda</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-nikah" value="Janda" data-select="status_nikah" onclick="changeInputValue(this);">Janda</label>
                                            </div>
                                            <input type="text" class="form-control" style="display: none" id="status_nikah" name="data_pelamars[status_nikah]"></input>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="nama_pasangan">Nama Suami / Istri</label>
                                        <input type="text" class="form-control" id="nama_pasangan" name="data_pelamars[nama_pasangan]" placeholder="Nama Suami / Istri">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="tgl_lahir_pasangan">Tanggal Lahir</label>
                                        <input type="date" class="form-control" id="tgl_lahir_pasangan" name="data_pelamars[tgl_lahir_pasangan]" placeholder="Tanggal Lahir">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="nama_anak1">Nama Anak 1</label>
                                        <input type="text" class="form-control" id="nama_anak1" name="data_pelamars[nama_anak1]" placeholder="Nama Anak">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="tgl_lahir_anak1">Tanggal Lahir 1</label>
                                        <input type="date" class="form-control" id="tgl_lahir_anak1" name="data_pelamars[tgl_lahir_anak1]" placeholder="Tanggal Lahir">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="nama_anak2">Nama Anak 2</label>
                                        <input type="text" class="form-control" id="nama_anak2" name="data_pelamars[nama_anak2]" placeholder="Nama Anak">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="tgl_lahir_anak2">Tanggal Lahir 2</label>
                                        <input type="date" class="form-control" id="tgl_lahir_anak2" name="data_pelamars[tgl_lahir_anak2]" placeholder="Tanggal Lahir">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="nama_anak3">Nama Anak 3</label>
                                        <input type="text" class="form-control" id="nama_anak3" name="data_pelamars[nama_anak3]" placeholder="Nama Anak">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="tgl_lahir_anak3">Tanggal Lahir 3</label>
                                        <input type="date" class="form-control" id="tgl_lahir_anak3" name="data_pelamars[tgl_lahir_anak3]" placeholder="Tanggal Lahir">
                                    </div>
                                </div>
                            </div>
                            <ul class="list-inline pull-right">
                                <li><button type="button" class="btn btn-primary next-step">Save and continue</button></li>
                            </ul>
                        </div>
<!-- data fisik -->
                        <div class="tab-pane" role="tabpanel" id="step2">
                            <h3><strong>DATA FISIK PELAMAR</strong></h3>
                            <hr>
                            <div class="step1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="tinggi_bdn">Tinggi / Berat Badan</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <span>
                                                    <input type="text" class="form-control" id="tinggi_bdn" name="data_fisik_pelamars[tinggi_bdn]" placeholder="Tinggi Badan">
                                                </span>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="berat_bdn" name="data_fisik_pelamars[berat_bdn]" placeholder="Berat Badan">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="gol_darah">Golongan Darah</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-darah" value="A" data-select="gol_darah" onclick="changeInputValue(this);">A</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-darah" value="B" data-select="gol_darah" onclick="changeInputValue(this);">B</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-darah" value="AB" data-select="gol_darah" onclick="changeInputValue(this);">AB</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-darah" value="O" data-select="gol_darah" onclick="changeInputValue(this);">O</label>
                                            </div>
                                            <input type="text" class="form-control" style="display: none" id="gol_darah" name="data_fisik_pelamars[gol_darah]"></input>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="warna_kulit">Warna Kulit</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-warna-kulit" value="Sawo Matang" data-select="warna_kulit" onclick="changeInputValue(this);">Sawo Matang</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-warna-kulit" value="Putih" data-select="warna_kulit" onclick="changeInputValue(this);">Putih</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-warna-kulit" value="Hitam" data-select="warna_kulit" onclick="changeInputValue(this);">Hitam</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-warna-kulit" value="Kuning Langsat" data-select="warna_kulit" onclick="changeInputValue(this);">Kuning Langsat</label>
                                            </div>
                                            <input type="text" class="form-control" style="display: none" id="warna_kulit" name="data_fisik_pelamars[warna_kulit]"></input>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="bentuk_muka">Bentuk Muka</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-bentuk-wajah" value="Oval" data-select="bentuk_muka" onclick="changeInputValue(this);">Oval</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-bentuk-wajah" value="Bulat" data-select="bentuk_muka" onclick="changeInputValue(this);">Bulat</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-bentuk-wajah" value="Lonjong" data-select="bentuk_muka" onclick="changeInputValue(this);">Lonjong</label>
                                            </div>
                                            <input type="text" class="form-control" style="display: none" id="bentuk_muka" name="data_fisik_pelamars[bentuk_muka]"></input>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="warna_mata">Warna Mata</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-warna-mata" value="Hitam" data-select="warna_mata" onclick="changeInputValue(this);">Hitam</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-warna-mata" value="Coklat" data-select="warna_mata" onclick="changeInputValue(this);">Coklat</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-warna-mata" value="Lainnya" data-select="warna_mata" onclick="changeInputValue(this);">Lainnya</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" style="display: none" id="warna_mata" name="data_fisik_pelamars[warna_mata]"></input>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="jenis_rambut">Jenis Rambut</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-jenis-rambut" value="Lurus" data-select="jenis_rambut" onclick="changeInputValue(this);">Lurus</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-jenis-rambut" value="Ikal" data-select="jenis_rambut" onclick="changeInputValue(this);">Ikal</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-jenis-rambut" value="Keriting" data-select="jenis_rambut" onclick="changeInputValue(this);">Keriting</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-jenis-rambut" value="Lainnya" data-select="jenis_rambut" onclick="changeInputValue(this);">Lainnya</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" style="display: none" id="jenis_rambut" name="data_fisik_pelamars[jenis_rambut]"></input>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="list-inline pull-right">
                                <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                                <li><button type="button" class="btn btn-primary next-step">Save and continue</button></li>
                            </ul>
                        </div>
<!-- data pendidikan -->
                        <div class="tab-pane" role="tabpanel" id="step3">
                            <h3><strong>DATA PENDIDIKAN</strong></h3>
                            <hr>
                            <div class="step1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="pendidikan_terakhir">Pendidikan Terakhir</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-pendidikan" value="SMA" data-select="pendidikan_terakhir" onclick="changeInputValue(this);">SMA</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-pendidikan" value="D3" data-select="pendidikan_terakhir" onclick="changeInputValue(this);">D3</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-pendidikan" value="S1 / Universitas" data-select="pendidikan_terakhir" onclick="changeInputValue(this);">S1 / Universitas</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" style="display: none" id="pendidikan_terakhir" name="data_pendidikan_pelamars[pendidikan_terakhir]"></input>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="asal_sekolah">Asal Sekolah</label>
                                                <input type="text" class="form-control" id="asal_sekolah" placeholder="Asal Sekolah" name="data_pendidikan_pelamars[asal_sekolah]">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="kota">Kota</label>
                                                <input type="text" class="form-control" id="kota" placeholder="Kota" name="data_pendidikan_pelamars[kota]">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="pendidikan_satpam">Pendidikan Satpam</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-pendidikan-satpam" value="Pra - Dasar" data-select="pendidikan_satpam" onclick="changeInputValue(this);">Pra - Dasar</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-pendidikan-satpam" value="Dasar" data-select="pendidikan_satpam" onclick="changeInputValue(this);">Dasar</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-pendidikan-satpam" value="Lanjutan" data-select="pendidikan_satpam" onclick="changeInputValue(this);">Lanjutan</label>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="radio-inline"><input type="radio" name="radio-pendidikan-satpam" value="Tidak Ada" data-select="pendidikan_satpam" onclick="changeInputValue(this);">Tidak Ada</label>
                                            </div>
                                            <input type="text" class="form-control" style="display: none" id="pendidikan_satpam" name="data_pendidikan_pelamars[pendidikan_satpam]"></input>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <label for="tempat_pendidikan">Tempat Pendidikan</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" id="tempat_pendidikan" placeholder="Tempat Pendidikan" name="data_pendidikan_pelamars[tempat_pendidikan]">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="radio-inline"><input type="radio" name="data_pendidikan_pelamars[sertifikat]" value="1">Sertifikat</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="list-inline pull-right">
                                <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                                <!-- <li><button type="button" class="btn btn-default next-step">Skip</button></li> -->
                                <li><button type="button" class="btn btn-primary btn-info-full next-step">Save and continue</button></li>
                            </ul>
                        </div>
<!-- data keluarga -->
                        <div class="tab-pane" role="tabpanel" id="step4">
                            <h3><strong>DATA KELUARGA</strong></h3>
                            <hr>
                            <div class="step1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="nama_ayah">Nama Ayah</label>
                                        <input type="text" class="form-control" id="nama_ayah" name="data_keluarga_pelamars[nama_ayah]" placeholder="Nama Ayah">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="nama_ibu">Nama Ibu</label>
                                        <input type="text" class="form-control" id="nama_ibu" name="data_keluarga_pelamars[nama_ibu]" placeholder="Nama Ibu">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="alamat_ortu">Alamat Orang Tua</label>
                                        <input type="text" class="form-control" id="alamat_ortu" name="data_keluarga_pelamars[alamat_ortu]" placeholder="Alamat Orang Tua">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="no_tlp_ortu">No. Telepon (Orang Tua)</label>
                                        <input type="text" class="form-control" id="no_tlp_ortu" name="data_keluarga_pelamars[no_tlp_ortu]" placeholder="No. Telepon">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="saudara_terdekat">Saudara Terdekat / Emergency</label>
                                        <input type="text" class="form-control" id="saudara_terdekat" name="data_keluarga_pelamars[saudara_terdekat]" placeholder="Saudara Terdekat / Emergency">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="no_tlp_saudara">No. Telepon (Saudara Terdekat)</label>
                                        <input type="text" class="form-control" id="no_tlp_saudara" name="data_keluarga_pelamars[no_tlp_saudara]" placeholder="No. Telepon">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="status_anak">Status Anak</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label for="anak_ke">Anak Ke</label>
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control" id="anak_ke" name="data_keluarga_pelamars[anak_ke]" placeholder="Anak Ke">
                                            </div>
                                            <div class="col-md-1">
                                                <label for="jml_bersaudara">Dari</label>
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control" id="dari" name="data_keluarga_pelamars[jml_bersaudara]" placeholder="Dari">
                                            </div>
                                            <div class="col-md-3">
                                                <label for="exampleInputEmail1">Bersaudara</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="list-inline pull-right">
                                <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                                <!-- <li><button type="button" class="btn btn-default next-step">Skip</button></li> -->
                                <li><button type="button" class="btn btn-primary btn-info-full next-step">Save and continue</button></li>
                            </ul>
                        </div>
<!-- lain-lain -->
                         <div class="tab-pane" role="tabpanel" id="step5">
                            <div class="step1">
                                <hr>
                                <h3><strong>LAIN - LAIN</strong></h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="referensi">Referensi</label>
                                        <input type="text" class="form-control" id="referensi" name="data_lain_pelamars[referensi]" placeholder="Referensi">
                                    </div>
                                    <div class="col-md-12">
                                        <label for="referensi">Teman Global</label>
                                        <input type="text" class="form-control" id="teman_global" name="data_lain_pelamars[teman_global]" placeholder="teman global">
                                    </div>
                                </div>
                                <hr>
                                <h3><strong>Informasi Tempat Tinggal</strong></h3>
                                <hr>
                                <div style="background-color: #f5f5ef; padding: 5px; box-shadow: 5px 6px 18px #7777;">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="alamat_rumah_tinggal">Alamat Rumah Tinggal Saat Ini</label>
                                            <textarea name="data_lain_pelamars[alamat_tinggal_sekarang]" id="alamat_tinggal_sekarang" cols="30" rows="10" class="form-control"placeholder="Alamat Rumah Tinggal Saat Ini"></textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="no_tlp_keluarga">No Telepon Keluarga Penting yang dapat dihubungi</label>
                                            <input type="text" name="data_lain_pelamars[tlp_keluarga1]" class="form-control" placeholder="No. Telepon">

                                            <input type="text" name="data_lain_pelamars[tlp_keluarga2]" class="form-control" placeholder="No. Telepon" style="margin-top: 25px;">
                                            
                                            <input type="text" name="data_lain_pelamars[tlp_keluarga3]" class="form-control" placeholder="No. Telepon" style="margin-top: 25px;">
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <h3><strong>Informasi Nama, Alamat dan Nomor Telepon Tetangga yang berdomisili disekitar Rumah Anda</strong></h3>
                                <hr>
                                <div style="background-color: #f5f5ef; padding: 5px; box-shadow: 5px 6px 18px #7777;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4>Tetangga Sebelah Kiri</h4>
                                            <label for="nama_tetangga_sebelah_kiri">Nama</label>
                                            <input type="text" name="data_lain_pelamars[nama_tetangga_kiri]" class="form-control" placeholder="Nama">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="alamat_tetangga_kiri">Alamat</label>
                                            <textarea name="data_lain_pelamars[alamat_tetangga_kiri]" id="alamat_tetangga_kiri" cols="30" rows="10" class="form-control"placeholder="Alamat"></textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="tlp_tetangga_kiri">No Telepon</label>
                                            <input type="text" name="data_lain_pelamars[tlp_tetangga_kiri]" class="form-control" placeholder="No. Telepon">
                                        </div>
                                    </div>
                                </div>
<hr>
                                <div style="background-color: #f5f5ef; padding: 5px; box-shadow: 5px 6px 18px #7777;">   
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4>Tetangga Sebelah Kanan</h4>
                                            <label for="nama_tetangga_kanan">Nama</label>
                                            <input type="text" name="data_lain_pelamars[nama_tetangga_kanan]" class="form-control" placeholder="Nama">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="alamat_tetangga_kanan">Alamat</label>
                                            <textarea name="data_lain_pelamars[alamat_tetangga_kanan]" id="alamat_tetangga_kanan" cols="30" rows="10" class="form-control"placeholder="Alamat"></textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="tlp_tetangga_kanan">No Telepon</label>
                                            <input type="text" name="data_lain_pelamars[tlp_tetangga_kanan]" class="form-control" placeholder="No. Telepon">
                                        </div>
                                    </div>
                                </div>
<hr>
                                <div style="background-color: #f5f5ef; padding: 5px; box-shadow: 5px 6px 18px #7777;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Tetangga Sebelah Belakang</h4>
                                        <label for="nama_tetangga_belakang">Nama</label>
                                        <input type="text" name="data_lain_pelamars[nama_tetangga_belakang]" class="form-control" placeholder="Nama">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="alamat_tetangga_belakang">Alamat</label>
                                        <textarea name="data_lain_pelamars[alamat_tetangga_belakang]" id="alamat_tetangga_belakang" cols="30" rows="10" class="form-control"placeholder="Alamat"></textarea>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="tlp_tetangga_belakang">No Telepon</label>
                                        <input type="text" name="data_lain_pelamars[tlp_tetangga_belakang]" class="form-control" placeholder="No. Telepon">
                                    </div>
                                </div>
                                </div>
<hr>
                                <div style="background-color: #f5f5ef; padding: 5px; box-shadow: 5px 6px 18px #7777;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4>Tetangga Sebelah Depan</h4>
                                            <label for="nama_tetangga_depan">Nama</label>
                                            <input type="text" name="data_lain_pelamars[nama_tetangga_depan]" class="form-control" placeholder="Nama">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="alamat_tetangga_depan">Alamat</label>
                                            <textarea name="alamat_tetangga_depan" id="data_lain_pelamars[alamat_tetangga_depan]" cols="30" rows="10" class="form-control"placeholder="Alamat"></textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="tlp_tetangga_depan">No Telepon</label>
                                            <input type="text" name="data_lain_pelamars[tlp_tetangga_depan]" class="form-control" placeholder="No. Telepon">
                                        </div>
                                    </div>
                                </div>
<hr>
                                <div style="background-color: #f5f5ef; padding: 5px; box-shadow: 5px 6px 18px #7777;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4>Pejabat Rukun Tetangga di Wilayah Rumah Anda</h4>
                                            <label for="nama_rt">Nama</label>
                                            <input type="text" name="data_lain_pelamars[nama_rt]" class="form-control" placeholder="Nama">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="alamat_rt">Alamat</label>
                                            <textarea name="data_lain_pelamars[alamat_rt]" id="alamat_rt" cols="30" rows="10" class="form-control"placeholder="Alamat"></textarea>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="tlp_rt">No Telepon</label>
                                            <input type="text" name="data_lain_pelamars[tlp_rt]" class="form-control" placeholder="No. Telepon">
                                        </div>
                                    </div>
                                </div>
<hr>

                            </div>
                            <ul class="list-inline pull-right">
                                <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                                <!-- <li><button type="button" class="btn btn-default next-step">Skip</button></li>  -->
                                <li><button type="button" class="btn btn-primary btn-info-full next-step">Save and continue</button></li>
                            </ul>
                        </div>
                       
<!-- pengalaman kerja -->
                        <div class="tab-pane" role="tabpanel" id="step6">
                            <h3><strong>PENGALAMAN KERJA</strong></h3>
                            <hr>
                            <div class="step1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="pengalamankerja1">Tgl/Bln/Thn</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <input type="date" class="form-control" id="kerja_dari1" name="data_lain_pelamars[kerja_dari1]" placeholder="Dari">
                                            </div>
                                            <div class="col-md-1">
                                                <label for="kerja_sampai1">s/d</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="date" class="form-control" id="kerja_sampai1" name="data_lain_pelamars[kerja_sampai1]" placeholder="Sampai">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" id="kerja_pt1" name="data_lain_pelamars[kerja_pt1]" placeholder="Pekerjaan">
                                            </div>
                                            <div class="col-md-1">
                                                <label for="kerja_jabatan1">Jabatan</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" id="kerja_jabatan1" name="data_lain_pelamars[kerja_jabatan1]" placeholder="Jabatan">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="pengalamankerja2">Tgl/Bln/Thn</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <input type="date" class="form-control" id="kerja_dari2" name="data_lain_pelamars[kerja_dari2]" placeholder="Dari">
                                            </div>
                                            <div class="col-md-1">
                                                <label for="kerja_sampai2">s/d</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="date" class="form-control" id="kerja_sampai2" name="data_lain_pelamars[kerja_sampai2]" placeholder="Sampai">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" id="kerja_pt2" name="data_lain_pelamars[kerja_pt2]" placeholder="Pekerjaan">
                                            </div>
                                            <div class="col-md-1">
                                                <label for="kerja_jabatan2">Jabatan</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" id="kerja_jabatan2" name="data_lain_pelamars[kerja_jabatan2]" placeholder="Jabatan">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="pengalamankerja3">Tgl/Bln/Thn</label>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <input type="date" class="form-control" id="kerja_dari3" name="data_lain_pelamars[kerja_dari3]" placeholder="Dari">
                                            </div>
                                            <div class="col-md-1">
                                                <label for="kerja_sampai3">s/d</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="date" class="form-control" id="kerja_sampai3" name="data_lain_pelamars[kerja_sampai3]" placeholder="Sampai">
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" id="kerja_pt3" name="data_lain_pelamars[kerja_pt3]" placeholder="Pekerjaan">
                                            </div>
                                            <div class="col-md-1">
                                                <label for="kerja_jabatan3">Jabatan</label>
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" id="kerja_jabatan3" name="data_lain_pelamars[kerja_jabatan3]" placeholder="Jabatan">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            <ul class="list-inline pull-right">
                                <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                                <!-- <li><button type="button" class="btn btn-default next-step">Skip</button></li> -->
                                <li><button type="submit" class="btn btn-primary btn-info-full next-step">Save and continue</button></li>
                            </ul>
                        </div>
                         <div class="clearfix"></div>
                    </div>
                <?php echo form_close();?>
                </div>
            </div>
        </section>
       </div>
    </div>

<!-- end isi -->