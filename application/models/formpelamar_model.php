<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formpelamar_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
    }
    // private $_table = ["data_pelamars","data_fisik_pelamars","data_pendidikan_pelamars","data_keluarga_pelamars",
    //                     "data_lain_pelamars"];

    public function getPelamar()
    {
        $query = $this->db->get('data_pelamars');
            return $query->result_array();
    }

    public function getPelamarById($id)
    {
        
        $query = $this->db->select('*')
                 ->from('data_pelamars AS a')
                 ->join('data_fisik_pelamars AS b', 'a.id=b.data_pelamar_id', 'left')
                 ->join('data_pendidikan_pelamars AS c', 'a.id=c.data_pelamar_id', 'left')
                 ->join('data_keluarga_pelamars AS d', 'a.id=d.data_pelamar_id', 'left')
                 ->join('upload AS e', 'a.id=e.data_pelamar_id', 'left')
                 ->join('data_cek_fisik AS f', 'a.id=f.data_pelamar_id', 'left')
                 ->join('data_lain_pelamars AS g', 'a.id=g.data_pelamar_id', 'left')
                 ->where('a.id', $id)     
                 ->get();
        return $query->result_array();
    }

    public function getFileById($id){
         
                 $this->db->from('upload AS a ');
                 $this->db->where('a.id', $id);
        $result = $this->db->get('');
        if($result->num_rows() > 0)
        return $result->row();
    }

	public function create_form() {

        $i = $this->input;
		$data_pelamars = [
            'nama_lengkap'              => $i->post('data_pelamars[nama]', TRUE),
            'tempat_lahir'              => $i->post('data_pelamars[tempat_lahir]', TRUE),
            'tanggal_lahir'             => $i->post('data_pelamars[tanggal_lahir]', TRUE),
            'umur'                      => $i->post('data_pelamars[umur]', TRUE),
            'jns_kelamin'               => $i->post('data_pelamars[jns_kelamin]', TRUE),
            'agama'                     => $i->post('data_pelamars[agama]', TRUE),
            'alamat_tinggal'            => $i->post('data_pelamars[alamat_tinggal]', TRUE),
            'alamat_tinggal_kecamatan'  => $i->post('data_pelamars[alamat_tinggal_kecamatan]', TRUE),
            'alamat_tinggal_kota'       => $i->post('data_pelamars[alamat_tinggal_kota]', TRUE),
            'status_tempat_tinggal'     => $i->post('data_pelamars[status_tempat_tinggal]', TRUE),
            'no_tlp'                    => $i->post('data_pelamars[no_tlp]', TRUE),
            'type_id'                   => '',
            'no_id'                     => $i->post('data_pelamars[no_id]', TRUE),
            'no_id_berkalu'             => null,
            'status_nikah'              => $i->post('data_pelamars[status_nikah]', TRUE),
            'nama_pasangan'             => $i->post('data_pelamars[nama_pasangan]', TRUE),
            'tgl_lahir_pasangan'        => $i->post('data_pelamars[tgl_lahir_pasangan]', TRUE),
            'nama_anak1'                => $i->post('data_pelamars[nama_anak1]', TRUE),
            'tgl_lahir_anak1'           => $i->post('data_pelamars[tgl_lahir_anak1]', TRUE),
            'nama_anak2'                => $i->post('data_pelamars[nama_anak2]', TRUE),
            'tgl_lahir_anak2'           => $i->post('data_pelamars[tgl_lahir_anak2]', TRUE),
            'nama_anak3'                => $i->post('data_pelamars[nama_anak3]', TRUE),
            'tgl_lahir_anak3'           => $i->post('data_pelamars[tgl_lahir_anak3]', TRUE),
            'nama_anak4'                => null,
            'tgl_lahir_anak4'           => null,
            'created_at'                => '',
            'created_by'                => '',
            'updated_at'                => '',
            'updated_by'                => '',
            'deleted_at'                => null
        ];
        $this->db->insert('data_pelamars', $data_pelamars);
		$last_insert_id = $this->db->insert_id();

        $data_fisik_pelamars = [
            'data_pelamar_id'           => $last_insert_id,
            'tinggi_bdn'                => $i->post('data_fisik_pelamars[tinggi_bdn]', TRUE),
            'berat_bdn'                 => $i->post('data_fisik_pelamars[berat_bdn]', TRUE),
            'gol_darah'                 => $i->post('data_fisik_pelamars[gol_darah]', TRUE),
            'warna_kulit'               => $i->post('data_fisik_pelamars[warna_kulit]', TRUE),
            'bentuk_muka'               => $i->post('data_fisik_pelamars[bentuk_muka]', TRUE),
            'warna_mata'                => $i->post('data_fisik_pelamars[warna_mata]', TRUE),
            'jenis_rambut'              => $i->post('data_fisik_pelamars[jenis_rambut]', TRUE),
            'created_at'                => '',
            'created_by'                => '',
            'updated_at'                => '',
            'updated_by'                => '',
            'deleted_at'                => null
        ];
        $this->db->insert('data_fisik_pelamars', $data_fisik_pelamars);

        $data_pendidikan_pelamars = [
            'data_pelamar_id'           => $last_insert_id,
            'pendidikan_terakhir'       => $i->post('data_pendidikan_pelamars[pendidikan_terakhir]', TRUE),
            'asal_sekolah'              => $i->post('data_pendidikan_pelamars[asal_sekolah]', TRUE),
            'kota'                      => $i->post('data_pendidikan_pelamars[kota]', TRUE),
            'pendidikan_satpam'         => $i->post('data_pendidikan_pelamars[pendidikan_satpam]', TRUE),
            'tempat_pendidikan'         => $i->post('data_pendidikan_pelamars[tempat_pendidikan]', TRUE),
            'sertifikat'                => null,
            'created_at'                => '',
            'created_by'                => '',
            'updated_at'                => '',
            'updated_by'                => '',
            'deleted_at'                => null
        ];
        $this->db->insert('data_pendidikan_pelamars', $data_pendidikan_pelamars);

        $data_keluarga_pelamars = [
            'data_pelamar_id'           => $last_insert_id,
            'nama_ayah'                 => $i->post('data_keluarga_pelamars[nama_ayah]', TRUE),
            'nama_ibu'                  => $i->post('data_keluarga_pelamars[nama_ibu]', TRUE),
            'alamat_ortu'               => $i->post('data_keluarga_pelamars[alamat_ortu]', TRUE),
            'no_tlp_ortu'               => $i->post('data_keluarga_pelamars[no_tlp_ortu]', TRUE),
            'saudara_terdekat'          => $i->post('data_keluarga_pelamars[saudara_terdekat]', TRUE),
            'no_tlp_saudara'            => $i->post('data_keluarga_pelamars[no_tlp_saudara]', TRUE),
            'anak_ke'                   => $i->post('data_keluarga_pelamars[anak_ke]', TRUE),
            'jml_bersaudara'            => $i->post('data_keluarga_pelamars[jml_bersaudara]', TRUE),
            'created_at'                => '',
            'created_by'                => '',
            'updated_at'                => '',
            'updated_by'                => '',
            'deleted_at'                => null
        ];
        $this->db->insert('data_keluarga_pelamars', $data_keluarga_pelamars);

        $data_lain_pelamars = [
            'data_pelamar_id'           => $last_insert_id,
            'referensi'                 => $i->post('data_lain_pelamars[referensi]', TRUE),
            'teman_global'              => $i->post('data_lain_pelamars[teman_global]', TRUE),
            'nama_teman_global'         => $i->post('data_lain_pelamars[nama_teman_global]', TRUE),
            'alamat_tinggal_sekarang'   => $i->post('data_lain_pelamars[alamat_tinggal_sekarang]'),
            'tlp_keluarga1'             => $i->post('data_lain_pelamars[tlp_keluarga1]', TRUE),
            'tlp_keluarga2'             => $i->post('data_lain_pelamars[tlp_keluarga2]', TRUE),
            'tlp_keluarga3'             => $i->post('data_lain_pelamars[tlp_keluarga3]', TRUE),
            'nama_tetangga_kiri'        => $i->post('data_lain_pelamars[nama_tetangga_kiri]', TRUE),
            'alamat_tetangga_kiri'      => $i->post('data_lain_pelamars[alamat_tetangga_kiri]', TRUE),
            'tlp_tetangga_kiri'         => $i->post('data_lain_pelamars[tlp_tetangga_kiri]', TRUE),
            'nama_tetangga_kanan'       => $i->post('data_lain_pelamars[nama_tetangga_kanan]', TRUE),
            'alamat_tetangga_kanan'     => $i->post('data_lain_pelamars[alamat_tetangga_kanan]', TRUE),
            'tlp_tetangga_kanan'        => $i->post('data_lain_pelamars[tlp_tetangga_kanan]', TRUE),
            'nama_tetangga_belakang'    => $i->post('data_lain_pelamars[nama_tetangga_belakang]', TRUE),
            'alamat_tetangga_belakang'  => $i->post('data_lain_pelamars[alamat_tetangga_belakang]', TRUE),
            'tlp_tetangga_belakang'     => $i->post('data_lain_pelamars[tlp_tetangga_belakang]', TRUE),
            'nama_tetangga_depan'       => $i->post('data_lain_pelamars[nama_tetangga_depan]', TRUE),
            'alamat_tetangga_depan'     => $i->post('data_lain_pelamars[alamat_tetangga_depan]', TRUE),
            'tlp_tetangga_depan'        => $i->post('data_lain_pelamars[tlp_tetangga_depan]', TRUE),
            'nama_rt'                   => $i->post('data_lain_pelamars[nama_rt]', TRUE),
            'alamat_rt'                 => $i->post('data_lain_pelamars[alamat_rt]', TRUE),
            'tlp_rt'                    => $i->post('data_lain_pelamars[tlp_rt]', TRUE),
            'kerja_dari1'               => $i->post('data_lain_pelamars[kerja_dari1]', TRUE),
            'kerja_sampai1'             => $i->post('data_lain_pelamars[kerja_sampai1]', TRUE),
            'kerja_jabatan1'            => $i->post('data_lain_pelamars[kerja_jabatan1]', TRUE),
            'kerja_pt1'                 => $i->post('data_lain_pelamars[kerja_pt1]', TRUE),
            'kerja_dari2'               => $i->post('data_lain_pelamars[kerja_dari2]', TRUE),
            'kerja_sampai2'             => $i->post('data_lain_pelamars[kerja_sampai2]', TRUE),
            'kerja_jabatan2'            => $i->post('data_lain_pelamars[kerja_jabatan2]', TRUE),
            'kerja_pt2'                 => $i->post('data_lain_pelamars[kerja_pt2]', TRUE),
            'kerja_dari3'               => $i->post('data_lain_pelamars[kerja_dari3]', TRUE),
            'kerja_sampai3'             => $i->post('data_lain_pelamars[kerja_sampai3]', TRUE),
            'kerja_pt3'                 => $i->post('data_lain_pelamars[kerja_pt3]', TRUE),
            'kerja_jabatan3'            => $i->post('data_lain_pelamars[kerja_jabatan3]', TRUE),
            'created_at'                => '',
            'created_by'                => '',
            'updated_at'                => '',
            'updated_by'                => '',
            'deleted_at'                => null
        ];
        $this->db->insert('data_lain_pelamars', $data_lain_pelamars);

        $upload = [
            'data_pelamar_id'           => $last_insert_id,
        ];
        $this->db->insert('upload', $upload);

        $data_cek_fisik = [
            'data_pelamar_id'           => $last_insert_id,
        ];
        $this->db->insert('data_cek_fisik', $data_cek_fisik);
        

	}




    public function update_form($id)
    {
         $i = $this->input;
        $data_pelamars = [
            'nama_lengkap'              => $i->post('data_pelamars[nama]', TRUE),
            'tempat_lahir'              => $i->post('data_pelamars[tempat_lahir]', TRUE),
            'tanggal_lahir'             => $i->post('data_pelamars[tanggal_lahir]', TRUE),
            'umur'                      => $i->post('data_pelamars[umur]', TRUE),
            'jns_kelamin'               => $i->post('data_pelamars[jns_kelamin]', TRUE),
            'agama'                     => $i->post('data_pelamars[agama]', TRUE),
            'alamat_tinggal'            => $i->post('data_pelamars[alamat_tinggal]', TRUE),
            'alamat_tinggal_kecamatan'  => $i->post('data_pelamars[alamat_tinggal_kecamatan]', TRUE),
            'alamat_tinggal_kota'       => $i->post('data_pelamars[alamat_tinggal_kota]', TRUE),
            'status_tempat_tinggal'     => $i->post('data_pelamars[status_tempat_tinggal]', TRUE),
            'no_tlp'                    => $i->post('data_pelamars[no_tlp]', TRUE),
            'type_id'                   => '',
            'no_id'                     => $i->post('data_pelamars[no_id]', TRUE),
            'no_id_berkalu'             => null,
            'status_nikah'              => $i->post('data_pelamars[status_nikah]', TRUE),
            'nama_pasangan'             => $i->post('data_pelamars[nama_pasangan]', TRUE),
            'tgl_lahir_pasangan'        => $i->post('data_pelamars[tgl_lahir_pasangan]', TRUE),
            'nama_anak1'                => $i->post('data_pelamars[nama_anak1]', TRUE),
            'tgl_lahir_anak1'           => $i->post('data_pelamars[tgl_lahir_anak1]', TRUE),
            'nama_anak2'                => $i->post('data_pelamars[nama_anak2]', TRUE),
            'tgl_lahir_anak2'           => $i->post('data_pelamars[tgl_lahir_anak2]', TRUE),
            'nama_anak3'                => $i->post('data_pelamars[nama_anak3]', TRUE),
            'tgl_lahir_anak3'           => $i->post('data_pelamars[tgl_lahir_anak3]', TRUE),
            'nama_anak4'                => null,
            'tgl_lahir_anak4'           => null,
            'updated_at'                => '',
            'updated_by'                => '',
            'deleted_at'                => null
        ];
        $this->db->where("id", $id);
        $this->db->update('data_pelamars', $data_pelamars);

        $data_fisik_pelamars = [
            'tinggi_bdn'                => $i->post('data_fisik_pelamars[tinggi_bdn]', TRUE),
            'berat_bdn'                 => $i->post('data_fisik_pelamars[berat_bdn]', TRUE),
            'gol_darah'                 => $i->post('data_fisik_pelamars[gol_darah]', TRUE),
            'warna_kulit'               => $i->post('data_fisik_pelamars[warna_kulit]', TRUE),
            'bentuk_muka'               => $i->post('data_fisik_pelamars[bentuk_muka]', TRUE),
            'warna_mata'                => $i->post('data_fisik_pelamars[warna_mata]', TRUE),
            'jenis_rambut'              => $i->post('data_fisik_pelamars[jenis_rambut]', TRUE),
            'updated_at'                => '',
            'updated_by'                => '',
            'deleted_at'                => null
        ];
        $this->db->where("data_pelamar_id", $id);
        $this->db->update('data_fisik_pelamars', $data_fisik_pelamars);

        $data_pendidikan_pelamars = [
            'pendidikan_terakhir'       => $i->post('data_pendidikan_pelamars[pendidikan_terakhir]', TRUE),
            'asal_sekolah'              => $i->post('data_pendidikan_pelamars[asal_sekolah]', TRUE),
            'kota'                      => $i->post('data_pendidikan_pelamars[kota]', TRUE),
            'pendidikan_satpam'         => $i->post('data_pendidikan_pelamars[pendidikan_satpam]', TRUE),
            'tempat_pendidikan'         => $i->post('data_pendidikan_pelamars[tempat_pendidikan]', TRUE),
            'sertifikat'                => null,
            'updated_at'                => '',
            'updated_by'                => '',
            'deleted_at'                => null
        ];
        $this->db->where("data_pelamar_id", $id);
        $this->db->update('data_pendidikan_pelamars', $data_pendidikan_pelamars);

        $data_keluarga_pelamars = [
            'nama_ayah'                 => $i->post('data_keluarga_pelamars[nama_ayah]', TRUE),
            'nama_ibu'                  => $i->post('data_keluarga_pelamars[nama_ibu]', TRUE),
            'alamat_ortu'               => $i->post('data_keluarga_pelamars[alamat_ortu]', TRUE),
            'no_tlp_ortu'               => $i->post('data_keluarga_pelamars[no_tlp_ortu]', TRUE),
            'saudara_terdekat'          => $i->post('data_keluarga_pelamars[saudara_terdekat]', TRUE),
            'no_tlp_saudara'            => $i->post('data_keluarga_pelamars[no_tlp_saudara]', TRUE),
            'anak_ke'                   => $i->post('data_keluarga_pelamars[anak_ke]', TRUE),
            'jml_bersaudara'            => $i->post('data_keluarga_pelamars[jml_bersaudara]', TRUE),
            'updated_at'                => '',
            'updated_by'                => '',
            'deleted_at'                => null
        ];
        $this->db->where("data_pelamar_id", $id);
        $this->db->update('data_keluarga_pelamars', $data_keluarga_pelamars);

        $data_lain_pelamars = [
            'referensi'                 => $i->post('data_lain_pelamars[referensi]', TRUE),
            'teman_global'              => $i->post('data_lain_pelamars[teman_global]', TRUE),
            'nama_teman_global'         => $i->post('data_lain_pelamars[nama_teman_global]', TRUE),
            'alamat_tinggal_sekarang'   => $i->post('data_lain_pelamars[alamat_tinggal_sekarang]', TRUE),
            'tlp_keluarga1'             => $i->post('data_lain_pelamars[tlp_keluarga1]', TRUE),
            'tlp_keluarga2'             => $i->post('data_lain_pelamars[tlp_keluarga2]', TRUE),
            'tlp_keluarga3'             => $i->post('data_lain_pelamars[tlp_keluarga3]', TRUE),
            'nama_tetangga_kiri'        => $i->post('data_lain_pelamars[nama_tetangga_kiri]', TRUE),
            'alamat_tetangga_kiri'      => $i->post('data_lain_pelamars[alamat_tetangga_kiri]', TRUE),
            'tlp_tetangga_kiri'         => $i->post('data_lain_pelamars[tlp_tetangga_kiri]', TRUE),
            'nama_tetangga_kanan'       => $i->post('data_lain_pelamars[nama_tetangga_kanan]', TRUE),
            'alamat_tetangga_kanan'     => $i->post('data_lain_pelamars[alamat_tetangga_kanan]', TRUE),
            'tlp_tetangga_kanan'        => $i->post('data_lain_pelamars[tlp_tetangga_kanan]', TRUE),
            'nama_tetangga_belakang'    => $i->post('data_lain_pelamars[nama_tetangga_belakang]', TRUE),
            'alamat_tetangga_belakang'  => $i->post('data_lain_pelamars[alamat_tetangga_belakang]', TRUE),
            'tlp_tetangga_belakang'     => $i->post('data_lain_pelamars[tlp_tetangga_belakang]', TRUE),
            'nama_tetangga_depan'       => $i->post('data_lain_pelamars[nama_tetangga_depan]', TRUE),
            'alamat_tetangga_depan'     => $i->post('data_lain_pelamars[alamat_tetangga_depan]', TRUE),
            'tlp_tetangga_depan'        => $i->post('data_lain_pelamars[tlp_tetangga_depan]', TRUE),
            'nama_rt'                   => $i->post('data_lain_pelamars[nama_rt]', TRUE),
            'alamat_rt'                 => $i->post('data_lain_pelamars[alamat_rt]', TRUE),
            'tlp_rt'                    => $i->post('data_lain_pelamars[tlp_rt]', TRUE),
            'kerja_dari1'               => $i->post('data_lain_pelamars[kerja_dari1]', TRUE),
            'kerja_sampai1'             => $i->post('data_lain_pelamars[kerja_sampai1]', TRUE),
            'kerja_jabatan1'            => $i->post('data_lain_pelamars[kerja_jabatan1]', TRUE),
            'kerja_pt1'                 => $i->post('data_lain_pelamars[kerja_pt1]', TRUE),
            'kerja_dari2'               => $i->post('data_lain_pelamars[kerja_dari2]', TRUE),
            'kerja_sampai2'             => $i->post('data_lain_pelamars[kerja_sampai2]', TRUE),
            'kerja_jabatan2'            => $i->post('data_lain_pelamars[kerja_jabatan2]', TRUE),
            'kerja_pt2'                 => $i->post('data_lain_pelamars[kerja_pt2]', TRUE),
            'kerja_dari3'               => $i->post('data_lain_pelamars[kerja_dari3]', TRUE),
            'kerja_sampai3'             => $i->post('data_lain_pelamars[kerja_sampai3]', TRUE),
            'kerja_pt3'                 => $i->post('data_lain_pelamars[kerja_pt3]', TRUE),
            'kerja_jabatan3'            => $i->post('data_lain_pelamars[kerja_jabatan3]', TRUE),
            'updated_at'                => '',
            'updated_by'                => '',
            'deleted_at'                => null
        ];
        $this->db->where("data_pelamar_id", $id);
        $this->db->update('data_lain_pelamars', $data_lain_pelamars);
    }

        //     //file foto
        //  if(!empty($_FILES['foto']['name'])) {
        //       $upload['foto'            => $this->_upload()];
        //  }

        //  //file sertifikat satpam
        //  if(!empty($_FILES['sertifikat_satpam']['name'])) {
        //      $upload['sertifikat_satpam' => $this->_upload()];
        //  }

        //  //file skck
        //  if(!empty($_FILES['skck']['name'])) {
        //     $upload['skck' => $this->_upload()];
        //  }

        //  //file surat dokter
        //  if(!empty($_FILES['surat_dokter']['name'])) {
        //      $upload['surat_dokter' => $this->_upload()];
        //  }

        //  //file surat lamaran kerja
        //  if(!empty($_FILES['surat_lamaran_kerja']['name'])) {
        //      $upload['surat_lamaran_kerja' => $this->_upload()];
        //  }

        //  //file daftar riwayat hidup
        //  if(!empty($_FILES['daftar_riwayat_hidup']['name'])) {
        //      $upload['daftar_riwayat_hidup' => $this->_upload()];
        //  }

        //  //file scan ktp
        //  if(!empty($_FILES['scan_ktp']['name'])) {
        //      $upload['scan_ktp' => $this->_upload()];
        //  }

        //  //file kartu keluarga
        //  if(!empty($_FILES['kartu_keluarga']['name'])) {
        //      $upload['kartu_keluarga' => $this->_upload()];
        //  }

        //  //file surat keterangan suami
        //  if(!empty($_FILES['surat_keterangan_suami']['name'])) {
        //      $upload['surat_keterangan_suami' => $this->_upload()];
        //  }
        // $this->db->where("data_pelamar_id", $id);
        // $this->db->update('upload', $upload);
    

    // public function create_upload($id){

    //     $upload = [
    //         'data_pelamar_id'           => $id,
    //         'foto'                      => $this->_upload(),
    //         'sertifikat_satpam'         => $this->_upload(),
    //         'skck'                      => $this->_upload(),
    //         'surat_dokter'              => $this->_upload(),
    //         'surat_lamaran_kerja'       => $this->_upload(),
    //         'daftar_riwayat_hidup'      => $this->_upload(),
    //         'scan_ktp'                  => $this->_upload(),
    //         'kartu_keluarga'            => $this->_upload(),
    //         'surat_keterangan_suami'    => $this->_upload()
    //     ];
    //     $this->db->insert('upload', $upload);
    // }

    public function update_cek_fisik($id){

        $i = $this->input;
        $data_cek_fisik = [
            'penyakit_kulit'            => $i->post('data_cek_fisik[penyakit_kulit]'),
            'varises'                   => $i->post('data_cek_fisik[varises]'),
            'ambien'                    => $i->post('data_cek_fisik[ambien]'),
            'hernia'                    => $i->post('data_cek_fisik[hernia]'),
            'tuli'                      => $i->post('data_cek_fisik[tuli]'),
            'kacamata'                  => $i->post('data_cek_fisik[kacamata]'),
            'tato'                      => $i->post('data_cek_fisik[tato]'),
            'tindik'                    => $i->post('data_cek_fisik[tindik]'),
            'kaki'                      => $i->post('data_cek_fisik[kaki]'),
            'tangan_bengkok'            => $i->post('data_cek_fisik[tangan_bengkok]'),
            'updated_at'                => '',
            'updated_by'                => ''
        ];
        $this->db->where("data_pelamar_id", $id);
        $this->db->update('data_cek_fisik', $data_cek_fisik);
    }

    public function _upload(){



        

            // $config['upload_path'] = './uploads/file';
            // $config['allowed_types'] = 'jpeg|jpg|png|pdf';
            // $config['max_size']     = '2048'; //besar file maksimum 2 MB
            // $config['encrypt_name'] = 'TRUE';
            // $this->load->library('upload', $config);

            // //file foto
            // if($this->upload->do_upload('foto')) {
            //     return $this->upload->data('file_name');
            // }

            // //file sertifikat satpam
            // if($this->upload->do_upload('sertifikat_satpam')) {
            //     return $this->upload->data('file_name');
            // }

            // //file skck
            // if($this->upload->do_upload('skck')) {
            //     return $this->upload->data('file_name');
            // }

            // //file surat dokter
            // if($this->upload->do_upload('surat_dokter')) {
            //     return $this->upload->data('file_name');
            // }

            // //file surat lamaran kerja
            // if($this->upload->do_upload('surat_lamaran_kerja')) {
            //     return $this->upload->data('file_name');
            // }

            // //file daftar riwayat hidup
            // if($this->upload->do_upload('daftar_riwayat_hidup')) {
            //     return $this->upload->data('file_name');
            // }

            // //file scan ktp
            // if($this->upload->do_upload('scan_ktp')) {
            //     return $this->upload->data('file_name');
            // }

            // //file kartu keluarga
            // if($this->upload->do_upload('kartu_keluarga')) {
            //     return $this->upload->data('file_name');
            // }

            // //file surat keterangan suami
            // if($this->upload->do_upload('surat_keterangan_suami')) {
            //     return  $this->upload->data('file_name');
            // }

            
    }


}

/* End of file formpelamar_model.php */
/* Location: ./application/models/formpelamar_model.php */