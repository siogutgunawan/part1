<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelamar extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('formpelamar_model');
	}

	public function index()
	{
		
		$data = [	
					'title' 				=> 'Pelamar',
					'data_pelamars'			=> $this->formpelamar_model->getPelamar()
		];
		$this->load->view('backend/pelamar/list', $data);
	}


	public function detail(){

		


		if(isset($_POST['submit_fisik'])){
			$id=$this->input->post('hidden_id');
			$this->formpelamar_model->update_cek_fisik($id);
		redirect(base_url('backend/pelamar/detail/'.$id),'refresh');
		}
		if(isset($_POST['hapus_file'])){
			
			$file = $this->formpelamar_model->getFileById($id);
			$path = './uploads/file';
			@unlink($path.$file->foto);
			redirect(base_url('backend/pelamar/detail/'.$id),'refresh');
		}

		$id = $this->uri->segment(4);
		// $berat_seimbang = $data_pelamars['berat_bdn'] / ($data_pelamars['tinggi_bdn'] * $data_pelamars['tinggi_bdn']);
		$data = [	
					'title' 				=> 'Cek Pelamar',
					// 'berat_seimbang'		=> $berat_seimbang,
					'data_pelamars'			=> $this->formpelamar_model->getPelamarById($id)
					
		];
		$this->load->view('backend/pelamar/detail', $data);
		
	}



	public function edit(){
		$valid = $this->form_validation;
		$valid->set_rules('data_pelamars[nama]','Nama','required');

		if(!$valid->run()){
			$id = $this->uri->segment(4);
			$data = [	
					'title' 				=> 'Edit Pelamar',
					'data_pelamars'			=> $this->formpelamar_model->getPelamarById($id)
			];
			$this->load->view('backend/pelamar/edit', $data);

		}else{

		$id=$this->input->post('hidden_id');
		$this->formpelamar_model->update_form($id);
		redirect(base_url('backend/pelamar/detail/'.$id),'refresh');
		}
	}

	public function upload_file(){

 				$config['upload_path']          = './uploads/file';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 2048;
                // $config['max_width']            = 1024;
                // $config['max_height']           = 768;
                $config['encrypt_name']         = TRUE;
               
        $this->load->library('upload',$config);
       
        //file foto
         if(!empty($_FILES['foto']['name'])) {
         	$this->upload->initialize($config);
         	$this->upload->do_upload('foto');
         	$data1	= $this->upload->data();
         	$foto 	= $data1['file_name'];
         }else{
         	$foto 	= $this->input->post('old_foto');
         }

         //file sertifikat satpam
         if(!empty($_FILES['sertifikat_satpam']['name'])) {
            $this->upload->initialize($config);
         	$this->upload->do_upload('sertifikat_satpam');
         	$data2	= $this->upload->data();
         	$sertifikat_satpam 	= $data2['file_name'];
         }else{
         	$sertifikat_satpam 	= $this->input->post('old_sertifikat_satpam');
         }

         //file skck
         if(!empty($_FILES['skck']['name'])) {
            $this->upload->initialize($config);
         	$this->upload->do_upload('skck');
         	$data3	= $this->upload->data();
         	$skck 	= $data3['file_name'];
         }else{
         	$skck 	= $this->input->post('old_skck');
         }

         //file surat dokter
         if(!empty($_FILES['surat_dokter']['name'])) {
            $this->upload->initialize($config);
         	$this->upload->do_upload('surat_dokter');
         	$data4	= $this->upload->data();
         	$surat_dokter 	= $data4['file_name'];
         }else{
         	$surat_dokter 	= $this->input->post('old_surat_dokter');
         }

         //file surat lamaran kerja
         if(!empty($_FILES['surat_lamaran_kerja']['name'])) {
            $this->upload->initialize($config);
         	$this->upload->do_upload('surat_lamaran_kerja');
         	$data5	= $this->upload->data();
         	$surat_lamaran_kerja 	= $data5['file_name'];
         }else{
         	$surat_lamaran_kerja 	= $this->input->post('old_surat_lamaran_kerja');
         }

         //file daftar riwayat hidup
         if(!empty($_FILES['daftar_riwayat_hidup']['name'])) {
            $this->upload->initialize($config);
         	$this->upload->do_upload('daftar_riwayat_hidup');
         	$data6	= $this->upload->data();
         	$daftar_riwayat_hidup 	= $data6['file_name'];
         }else{
         	$daftar_riwayat_hidup 	= $this->input->post('old_daftar_riwayat_hidup');
         }

         //file scan ktp
         if(!empty($_FILES['scan_ktp']['name'])) {
            $this->upload->initialize($config);
         	$this->upload->do_upload('scan_ktp');
         	$data7	= $this->upload->data();
         	$scan_ktp 	= $data7['file_name'];
         }else{
         	$scan_ktp 	= $this->input->post('old_scan_ktp');
         }

         //file kartu keluarga
         if(!empty($_FILES['kartu_keluarga']['name'])) {
            $this->upload->initialize($config);
         	$this->upload->do_upload('kartu_keluarga');
         	$data8	= $this->upload->data();
         	$kartu_keluarga 	= $data8['file_name'];
         }else{
         	$kartu_keluarga 	= $this->input->post('old_kartu_keluarga');
         }

         //file surat keterangan suami
         if(!empty($_FILES['surat_keterangan_suami']['name'])) {
            $this->upload->initialize($config);
         	$this->upload->do_upload('surat_keterangan_suami');
         	$data9	= $this->upload->data();
         	$surat_keterangan_suami 	= $data9['file_name'];
         }else{
         	$surat_keterangan_suami 	= $this->input->post('old_surat_keterangan_suami');
         }

        $data = [	'foto'=>$foto, 'sertifikat_satpam'=>$sertifikat_satpam, 'skck'=>$skck, 
					'surat_dokter'=>$surat_dokter, 'surat_lamaran_kerja'=>$surat_lamaran_kerja,
					'daftar_riwayat_hidup'=>$daftar_riwayat_hidup, 'scan_ktp'=>$scan_ktp,
					'kartu_keluarga'=>$kartu_keluarga, 'surat_keterangan_suami'=>$surat_keterangan_suami];
        $id=$this->input->post('hidden_id');
        $this->db->where("data_pelamar_id", $id);
        $this->db->update('upload', $data);
        redirect(base_url('backend/pelamar/detail/'.$id),'refresh');
        }
}

			// $upload1 	= $_FILES['foto']['name'];
		 //    $upload2 	= $_FILES['sertifikat_satpam']['name'];
		 //    $upload3 	= $_FILES['skck']['name'];
		 //    $upload4 	= $_FILES['surat_dokter']['name'];
		 //    $upload5 	= $_FILES['surat_lamaran_kerja']['name'];
		 //    $upload6 	= $_FILES['daftar_riwayat_hidup']['name'];
		 //    $upload7 	= $_FILES['scan_ktp']['name'];
		 //    $upload8 	= $_FILES['kartu_keluarga']['name'];
		 //    $upload9	= $_FILES['surat_keterangan_suami']['name'];


	  //  	 	$config['upload_path']          = './uploads/file';
   //          $config['allowed_types']        = 'gif|jpg|png';
   //          $config['max_size']             = 2048; //Maksimum 2 MB
   //          // $config['max_width']            = 1024;
   //          // $config['max_height']           = 768;
   //          $config['encrypt_name']         = TRUE;

   //          $this->load->library('upload', $config);
		 //    for($i = 1; $i<=9; $i++){
		 //        if(!empty('$upload'.$i)){
		 //                        $this->upload->do_upload('upload'.$i);
		 //                        $upload_data = $this->upload->data();
		 //                        $file_name['$i'] = $upload_data['file_name'];
		 //                    }
		 //    }
		 //    $data = [	'foto'=>$file_name, 'sertifikat_satpam'=>$file_name, 'skck'=>$file_name, 
			// 			'surat_dokter'=>$file_name, 'surat_lamaran_kerja'=>$file_name,
			// 			'daftar_riwayat_hidup'=>$file_name, 'scan_ktp'=>$file_name,
			// 			'kartu_keluarga'=>$file_name, 'surat_keterangan_suami'=>$file_name];

			// $id=$this->input->post('hidden_id');
			// $this->db->where("data_pelamar_id", $id);			
			// $this->db->update('upload', $data);
			// redirect(base_url('backend/pelamar/detail/'.$id),'refresh');

		

				
	

/* End of file Pelamar.php */
/* Location: ./application/controllers/backend/Pelamar.php */