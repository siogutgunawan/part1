<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logistik extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_logistik');
	}

	public function index()
	{
		$data = array(	
						'header' => 'Logistik',
						'title' => 'Logistik',
						'isi'	=> ''
				);
		$this->load->view('backend/logistik/data_calon_logistik', $data);
	}

	public function tambah_barang()
	{
		$data['header'] = "Tambah Barang";

		$this->template->admin('admin/tambah_barang', $data);
	}

	public function keluar_barang()
	{
		$data['header'] = "Keluar Barang";
		$this->template->admin('admin/keluar_barang',$data);
	}

	public function masuk_barang()
	{
		$data['header'] = "Masuk Barang";
		
		if ($this->input->post('submit', TRUE) == 'Submit') {

			$barang = array(
				'kode_barang' => $this->input->post('kodebarang', TRUE),
				'nama_barang' => $this->input->post('namabarang', TRUE),
				'nama_category' => $this->input->post('kategorybarang', TRUE),
				'ukuran' => $this->input->post('ukuran', TRUE),
				'harga_satuan' => $this->input->post('harga', TRUE),
				'stok_qty' => $this->input->post('qty', TRUE),

			);
			$id = $this->m_logistik->insert_last('data_barang', $barang);
			redirect('backend/data_barang','refresh');
		}

	}

	public function data_barang()
	{
		$data['title'] = "Data Logistik";
		$data['data_barang'] = $this->m_logistik->get_all('data_barang');
		$this->load->view('backend/logistik/data_logistik', $data);
	}

	public function data_logistik_calon()
	{
		$data['title'] = "Data Logistik Calon";
		$this->load->view('backend/logistik/form_logistik', $data);	
	}
	public function report_barang()
	{	
		$data['header'] = "Report Barang";

	}
	public function daily_stock_product()
	{
		$data['title'] = "Daily Stock List Logistik";
		$this->load->view('backend/logistik/daily_stock', $data);
	} 

}

/* End of file Logistik.php */
/* Location: ./application/controllers/Logistik.php */