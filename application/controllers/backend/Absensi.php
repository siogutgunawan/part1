<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi extends CI_Controller {

	public function index()
	{
		$data = [	
					'title' 				=> 'Absensi'
			];
		$this->load->view('backend/absensi/list', $data);
	}

	public function list_cabang()
	{
		$data = [	
					'title' 				=> 'Daftar Cabang'
			];
		$this->load->view('backend/absensi/list_cabang', $data);
	}

	public function daftar_pegawai()
	{
		$data = [	
					'title' 				=> 'Daftar Pegawai'
			];
		$this->load->view('backend/absensi/detail', $data);
	}

	public function info_absensi()
	{
		$data = [	
					'title' 				=> 'Nama Pegawai'
			];
		$this->load->view('backend/absensi/info', $data);
	}

}

/* End of file Absensi.php */
/* Location: ./application/controllers/backend/Absensi.php */