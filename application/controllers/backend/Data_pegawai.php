<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_pegawai extends CI_Controller {

	public function index()
	{
		$data = [	
					'title' 				=> 'Data pegawai',
			];
		$this->load->view('backend/data_pegawai/list', $data);
	}

	public function detail()
	{
		$data = array('title'=>'Management Surat Pegawai');
		$this->load->view('backend/data_pegawai/detail',$data);
	}

}

/* End of file Data_pegawai.php */
/* Location: ./application/controllers/backend/Data_pegawai.php */