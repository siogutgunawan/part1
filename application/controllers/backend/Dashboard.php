<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function index()
	{
		$this->load->view('backend/overview');
	}

	public function verdua()
	{
		$data = array(	'title'		=>	'dashboard',
						'isi'		=>	'backend/dashboard/list');

		$this->load->view('backend/layout/wrapper', $data, FALSE);
	}

}

/* End of file dashboard.php */
/* Location: ./application/controllers/backend/dashboard.php */