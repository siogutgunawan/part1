<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formpelamar extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model("formpelamar_model");
		
	}

	public function index()
	{
		$data = array(	'title'		=>	'Form Pelamar',
						'isi'		=>	'frontend/formpelamar/list');

		$this->load->view('frontend/layout/wrapper', $data, FALSE);
	}

	public function formPost()
	{
		$this->form_validation->set_rules('nama_personil', 'Nama Personil', 'required');
		$this->formpelamar_model->create_form();

	}

}

/* End of file Formpelamar.php */
/* Location: ./application/controllers/frontend/Formpelamar.php */