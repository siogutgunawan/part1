<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surat extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('mypdf');
	}

	public function index()
	{
		$data = [	
					'title' 				=> '.....'
			];
		$this->load->view('backend/data_pegawai/data_surat', $data);
	}

	public function surat_keterangan_kerja()
	{
		$data = [	
					'title' 				=> '.....'
			];
		$this->mypdf->generate('surat/surat_keterangan_kerja', $data);
	}

	public function surat_perintah_tugas()
	{
		$data = [	
					'title' 				=> '.....'
			];
		$this->mypdf->generate('surat/surat_perintah', $data);
	}

	public function surat_perintah_mutasi_tugas()
	{
		$data = [	
					'title' 				=> '.....'
			];
		$this->mypdf->generate('surat/surat_perintah_mutasi', $data);
	}

}

/* End of file Surat.php */
/* Location: ./application/controllers/surat/Surat.php */