-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2018 at 01:17 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gss`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_fisik_pelamars`
--

CREATE TABLE `data_fisik_pelamars` (
  `id` int(11) NOT NULL,
  `data_pelamar_id` int(11) DEFAULT NULL,
  `tinggi_bdn` varchar(10) DEFAULT NULL,
  `berat_bdn` varchar(10) DEFAULT NULL,
  `gol_darah` varchar(5) DEFAULT NULL,
  `warna_kulit` varchar(20) DEFAULT NULL,
  `bentuk_muka` varchar(20) DEFAULT NULL,
  `warna_mata` varchar(20) DEFAULT NULL,
  `jenis_rambut` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_fisik_pelamars`
--

INSERT INTO `data_fisik_pelamars` (`id`, `data_pelamar_id`, `tinggi_bdn`, `berat_bdn`, `gol_darah`, `warna_kulit`, `bentuk_muka`, `warna_mata`, `jenis_rambut`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(5, 5, '165', '12', 'B', 'Sawo Matang', 'Oval', 'Hitam', 'Lurus', '2018-12-25 05:28:28', '', '0000-00-00 00:00:00', '', NULL),
(13, 1, '173', '50', 'B', 'Hitam', 'Lonjong', 'Coklat', 'Keriting', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(14, 14, '160', '66', 'A', 'Sawo Matang', 'Oval', 'Hitam', 'Lurus', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(15, 15, '', '', '', '', '', '', '', '2018-12-22 04:24:01', '', '0000-00-00 00:00:00', '', NULL),
(16, 16, '180', '50', 'AB', 'Putih', 'Bulat', 'Coklat', 'Ikal', '2018-12-25 06:08:13', '', '0000-00-00 00:00:00', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_keluarga_pelamars`
--

CREATE TABLE `data_keluarga_pelamars` (
  `id` int(11) NOT NULL,
  `data_pelamar_id` int(11) DEFAULT NULL,
  `nama_ayah` varchar(50) DEFAULT NULL,
  `nama_ibu` varchar(50) DEFAULT NULL,
  `alamat_ortu` varchar(50) DEFAULT NULL,
  `no_tlp_ortu` varchar(50) DEFAULT NULL,
  `saudara_terdekat` varchar(50) DEFAULT NULL,
  `no_tlp_saudara` varchar(50) DEFAULT NULL,
  `anak_ke` varchar(50) DEFAULT NULL,
  `jml_bersaudara` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_keluarga_pelamars`
--

INSERT INTO `data_keluarga_pelamars` (`id`, `data_pelamar_id`, `nama_ayah`, `nama_ibu`, `alamat_ortu`, `no_tlp_ortu`, `saudara_terdekat`, `no_tlp_saudara`, `anak_ke`, `jml_bersaudara`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-25 11:16:59', '', '2018-09-25 11:16:59', '', NULL),
(9, 1, 'sdvdsvsdvsd', 'sdvdsvdsvs', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(10, 14, 'asfasfas', 'fasfas', 'gwew', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(11, 15, 'ayah', 'dgsraes', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(12, 16, 'ba', 'sarni', '     jalan jalan', '0987654', 'firman', '234567', '2', '10', '2018-12-25 10:36:06', '', '0000-00-00 00:00:00', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_lain_pelamars`
--

CREATE TABLE `data_lain_pelamars` (
  `id` int(11) NOT NULL,
  `data_pelamar_id` int(11) DEFAULT NULL,
  `referensi` varchar(50) DEFAULT NULL,
  `teman_global` int(2) DEFAULT NULL,
  `nama_teman_global` varchar(50) DEFAULT NULL,
  `alamat_tinggal_sekarang` varchar(100) DEFAULT NULL,
  `tlp_keluarga1` varchar(20) DEFAULT NULL,
  `tlp_keluarga2` varchar(20) DEFAULT NULL,
  `tlp_keluarga3` varchar(20) DEFAULT NULL,
  `nama_tetangga_kiri` varchar(50) DEFAULT NULL,
  `alamat_tetangga_kiri` varchar(100) DEFAULT NULL,
  `tlp_tetangga_kiri` varchar(50) DEFAULT NULL,
  `nama_tetangga_kanan` varchar(50) DEFAULT NULL,
  `alamat_tetangga_kanan` varchar(100) DEFAULT NULL,
  `tlp_tetangga_kanan` varchar(50) DEFAULT NULL,
  `nama_tetangga_belakang` varchar(50) DEFAULT NULL,
  `alamat_tetangga_belakang` varchar(100) DEFAULT NULL,
  `tlp_tetangga_belakang` varchar(50) DEFAULT NULL,
  `nama_tetangga_depan` varchar(50) DEFAULT NULL,
  `alamat_tetangga_depan` varchar(100) DEFAULT NULL,
  `tlp_tetangga_depan` varchar(50) DEFAULT NULL,
  `nama_rt` varchar(50) DEFAULT NULL,
  `alamat_rt` varchar(100) DEFAULT NULL,
  `tlp_rt` varchar(50) DEFAULT NULL,
  `kerja_dari1` date DEFAULT NULL,
  `kerja_sampai1` date DEFAULT NULL,
  `kerja_jabatan1` varchar(50) DEFAULT NULL,
  `kerja_pt1` varchar(50) DEFAULT NULL,
  `kerja_dari2` date DEFAULT NULL,
  `kerja_sampai2` date DEFAULT NULL,
  `kerja_jabatan2` varchar(50) DEFAULT NULL,
  `kerja_pt2` varchar(50) DEFAULT NULL,
  `kerja_dari3` date DEFAULT NULL,
  `kerja_sampai3` date DEFAULT NULL,
  `kerja_pt3` varchar(50) DEFAULT NULL,
  `kerja_jabatan3` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_lain_pelamars`
--

INSERT INTO `data_lain_pelamars` (`id`, `data_pelamar_id`, `referensi`, `teman_global`, `nama_teman_global`, `alamat_tinggal_sekarang`, `tlp_keluarga1`, `tlp_keluarga2`, `tlp_keluarga3`, `nama_tetangga_kiri`, `alamat_tetangga_kiri`, `tlp_tetangga_kiri`, `nama_tetangga_kanan`, `alamat_tetangga_kanan`, `tlp_tetangga_kanan`, `nama_tetangga_belakang`, `alamat_tetangga_belakang`, `tlp_tetangga_belakang`, `nama_tetangga_depan`, `alamat_tetangga_depan`, `tlp_tetangga_depan`, `nama_rt`, `alamat_rt`, `tlp_rt`, `kerja_dari1`, `kerja_sampai1`, `kerja_jabatan1`, `kerja_pt1`, `kerja_dari2`, `kerja_sampai2`, `kerja_jabatan2`, `kerja_pt2`, `kerja_dari3`, `kerja_sampai3`, `kerja_pt3`, `kerja_jabatan3`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 5, 'aziz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-01', '2018-09-26', 'anuan', 'anu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-25 11:28:21', '', '2018-09-25 11:28:21', '', NULL),
(8, 1, '', 0, NULL, '', '', '', '', NULL, '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '0000-00-00', '0000-00-00', '', 'sdvdsv', '0000-00-00', '0000-00-00', '', 'sdvdsv', '0000-00-00', '0000-00-00', 'sdvds', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(9, 14, '', 0, NULL, '', '', '', '', NULL, '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '0000-00-00', '0000-00-00', '', 'ewfewf', '0000-00-00', '0000-00-00', '', 'wefwef', '0000-00-00', '0000-00-00', 'wefwe', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(10, 15, '', 0, NULL, '', '', '', '', NULL, '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '0000-00-00', '0000-00-00', '', 'wrgewgew', '0000-00-00', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(11, 16, 'brox', 0, NULL, 'jalan', '1245678', '123456', '123456', '', '          ', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '2018-12-18', '2018-12-23', 'tester', 'nge test', '0000-00-00', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '', '', '2018-12-25 10:36:06', '', '0000-00-00 00:00:00', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_pelamars`
--

CREATE TABLE `data_pelamars` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `umur` int(5) DEFAULT NULL,
  `jns_kelamin` varchar(50) DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `alamat_tinggal` varchar(100) DEFAULT NULL,
  `alamat_tinggal_kecamatan` varchar(50) DEFAULT NULL,
  `alamat_tinggal_kota` varchar(50) DEFAULT NULL,
  `status_tempat_tinggal` varchar(50) DEFAULT NULL,
  `no_tlp` varchar(50) DEFAULT NULL,
  `type_id` varchar(50) DEFAULT NULL,
  `no_id` varchar(50) DEFAULT NULL,
  `no_id_berkalu` datetime DEFAULT NULL,
  `status_nikah` varchar(50) DEFAULT NULL,
  `nama_pasangan` varchar(50) DEFAULT NULL,
  `tgl_lahir_pasangan` date DEFAULT NULL,
  `nama_anak1` varchar(50) DEFAULT NULL,
  `tgl_lahir_anak1` date DEFAULT NULL,
  `nama_anak2` varchar(50) DEFAULT NULL,
  `tgl_lahir_anak2` date DEFAULT NULL,
  `nama_anak3` varchar(50) DEFAULT NULL,
  `tgl_lahir_anak3` date DEFAULT NULL,
  `nama_anak4` varchar(50) DEFAULT NULL,
  `tgl_lahir_anak4` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_pelamars`
--

INSERT INTO `data_pelamars` (`id`, `nama_lengkap`, `tempat_lahir`, `tanggal_lahir`, `umur`, `jns_kelamin`, `agama`, `alamat_tinggal`, `alamat_tinggal_kecamatan`, `alamat_tinggal_kota`, `status_tempat_tinggal`, `no_tlp`, `type_id`, `no_id`, `no_id_berkalu`, `status_nikah`, `nama_pasangan`, `tgl_lahir_pasangan`, `nama_anak1`, `tgl_lahir_anak1`, `nama_anak2`, `tgl_lahir_anak2`, `nama_anak3`, `tgl_lahir_anak3`, `nama_anak4`, `tgl_lahir_anak4`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(5, 'aziz', 'tangsel', '2018-09-01', 15, 'Pria', 'Kristen', 'jdsugdsg', 'ciputat', 'tangesr', 'Milik Sendiri', 'hdios', '', '1234567', NULL, 'Belum Menikah', 'sumijem', '2018-12-23', 'augusdgsdiu', '2018-09-26', '', '0000-00-00', '', '0000-00-00', NULL, NULL, '2018-09-25 11:28:21', '', '0000-00-00 00:00:00', '', NULL),
(13, 'gunawan', 'bandung', '2018-11-21', 1213, 'Pria', 'Islam', '31rsrgsrgs', 'sdvsd', 'sdvds', '', '', '', '', NULL, '', 'asfdg', '0000-00-00', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00', NULL, NULL, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(14, 'jono', 'jkt', '0000-00-00', 0, '', '', 'asfas', '', '', '', '12345', '', '2341', NULL, '', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00', NULL, NULL, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(15, 'joni', '', '2018-12-04', NULL, '', '', '', '', '', '', '', '', '123445', NULL, '', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00', NULL, NULL, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(16, 'maman abdu', 'jakarta', '2018-12-16', 30, 'Wanita', 'Islam', 'jalan jalan jalan', 'disana', 'disitu', 'Keluarga', '021468', '', '12345699', NULL, 'Menikah', 'jini', '2018-12-10', 'dana', '2018-12-13', 'dini', '2018-12-11', 'dunu', '2018-12-20', NULL, NULL, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_pendidikan_pelamars`
--

CREATE TABLE `data_pendidikan_pelamars` (
  `id` int(11) NOT NULL,
  `data_pelamar_id` int(11) DEFAULT NULL,
  `pendidikan_terakhir` varchar(20) DEFAULT NULL,
  `asal_sekolah` varchar(100) DEFAULT NULL,
  `kota` varchar(50) DEFAULT NULL,
  `pendidikan_satpam` varchar(100) DEFAULT NULL,
  `tempat_pendidikan` varchar(100) DEFAULT NULL,
  `sertifikat` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_pendidikan_pelamars`
--

INSERT INTO `data_pendidikan_pelamars` (`id`, `data_pelamar_id`, `pendidikan_terakhir`, `asal_sekolah`, `kota`, `pendidikan_satpam`, `tempat_pendidikan`, `sertifikat`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(4, 5, 'SMA', 'jakarta', 'tangsel', 'Pra - Dasar', 'sonoh', '1', '2018-09-25 11:28:21', '', '2018-09-25 11:28:21', '', NULL),
(11, 1, 'SMA', 'dfsvs', '', '', '', NULL, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(12, 14, 'D3', 'awfsasfasf', 'safsaa', 'Dasar', '', NULL, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(13, 15, 'SMA', 'sfdsgs', 'sdgsdg', '', '', NULL, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', NULL),
(14, 16, 'SMA', 'SMK', 'disana', 'Lanjutan', 'jakarta', NULL, '2018-12-25 09:09:35', '', '0000-00-00 00:00:00', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `entity_id` int(10) UNSIGNED DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assets` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `history_types`
--

CREATE TABLE `history_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_types`
--

INSERT INTO `history_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'User', '2018-09-25 19:42:38', '2018-09-25 19:42:38'),
(2, 'Role', '2018-09-25 19:42:38', '2018-09-25 19:42:38');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_12_28_171741_create_social_logins_table', 1),
(4, '2015_12_29_015055_setup_access_tables', 1),
(5, '2016_07_03_062439_create_history_tables', 1),
(6, '2017_04_04_131153_create_sessions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'view-backend', 'View Backend', '2018-09-25 19:42:37', '2018-09-25 19:42:37');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`) VALUES
(1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `all` tinyint(1) NOT NULL DEFAULT '0',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `all`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 1, 1, '2018-09-25 19:42:36', '2018-09-25 19:42:36'),
(2, 'Executive', 0, 2, '2018-09-25 19:42:36', '2018-09-25 19:42:36'),
(3, 'User', 0, 3, '2018-09-25 19:42:36', '2018-09-25 19:42:36');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `user_id`, `role_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('CPQtziRBNLBSyT95ndXYAC9SrPjclTMjlFiazZ4b', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'ZXlKcGRpSTZJbWxTWkVWM09ISklVR1F6Umx3dllVSktWbXhMUzBkQlBUMGlMQ0oyWVd4MVpTSTZJbGh3VlRnclRGTlFZV3czVFUxMlEySjRNSHB0UkZKQlpsSlNWRVV4TmxOTmFqVTFZVTh3ZVhoaGEwSnVlVmhoUmxCUGRsQmllRzlhYVUxMVZrZGNMM0ZwUVZKMFowcFJXSFpZY0ZWaFpIZFZlVVYxT0U5SE1USmhUVmhEUTBoYVZtNW5lREpxVlhSVVdHUmpSM2hyZGpkNlVHOHdTMXd2TVVkdVRrSkpTRVZSSzBkU2RGcHdXQ3RWVjFWc2VYUndLemN3YURaNVNFRk1hRGhGZHpkWWRXSmtaM0ZoZFdaSmJXeHZOazlCTldaT1VGZEhRakJtV2l0d2MwWkVSbHBvVlVoTVZHb3Jja1JOZGtneGRWQjFiRGx6WlVsdE4wTjNiM3BPTUUxM1ZGaE5lRXd5U0dsM1kwRmtiR1YyVTJGNWJtOVdObE4yUm1GQlhDODFUMHByWVRKM1UwcHNNV2xKZUZKelkxd3ZkR2xWYlRjeFRVUjBLemRZWms1UFVsZHVlRnd2YmpkSU0xWXJYQzlNWkhSSWFsbFdXRzEyYkZBd1lXdzBlRTU0YTFZNWNWbFJORU5IY1ZkcGVHbHpkR2xQWVZOallWTnRVR1p6Um1vNVp6MDlJaXdpYldGaklqb2lOekF5T0RFeE5XTTFZVGt4WldJNU5XVmxNbVV5TURGa04yTmxNRFUyWkdFeU1EbGtaR1ZqTTJZM1pUQTFNRE5sTmpsbU4yUXdaVFl3TlRWa09UTXhOeUo5', 1540277476),
('faK1EigRcS1No3F5CifNGCeHTj8T6mFhX7cIsYoz', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'ZXlKcGRpSTZJbFpGWEM4MFNVOVdUMFpaYUd4T01IaE1hREJTZUU5blBUMGlMQ0oyWVd4MVpTSTZJa296YUhKT1JFVlpUMFp1YVRsSVYybDRkM0Y1YW01dFZFVkpkSGNyTm5rd1JtUldNVzh6YkhBNVVYWlhhR3RMVFZGMVJuSmtia1JGWEM5NVUwbG1NMXAyTVhJell6TkhhRUpaYXpkWE1ubFNNMjVpYjJ4Q1dUWkZRaXQ2YkZkR1MxaG9NV0Z3TjFFemJURndWVzU1UjJscGVXVTVNM1J4Y0hGblFtcFVSbVo2WjFkWWREWndTa053UkVOM1ZrVldOeko0YUZCbWNqaHZZWE5sWEM5NllVaE5kMGRhT0RaYWNFaEhiMkpYU1hKalRrOWtZVGRWU1RaeGRsTnBWWGhPUXpkWWEwcHlZV1VyZG1GSFMxWkhSbFJxY0hwaFRVVkpZME0wYlRVMVVFbHZSV0Z1WjBWaWFGTXpURk40YVZCVGVFaE1kVFZTVEhwT2JESmhUbVp0UVdzeVVqWmFkM2t4TkhsbVZqQldObEl5Y1hSMWVXSmFkemxwYUU1UVZIUXpjMGxZSzNwR04wVXlRV3hWUkZoUU9YcFFZVWRLY0VOcmQzSXpORTFSYkVGdFNFb2lMQ0p0WVdNaU9pSmlPR0UyT1Roa1lXSTVaVEJrTkRZd1pUZGlNV0kzTURjMlptRTNORFJoWW1WbVpEazJZMlppT1Rkak1UQXdNR1EyWlRRMVptVmhObUZrTm1FME16SXlJbjA9', 1542985313),
('GZ992CiT0ByFsFsZNvNuR8BYOwJtePOhnLUV0fvS', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'ZXlKcGRpSTZJbFI2YURjek9VbFZhWFZ4TTBsU016WTVlRnd2V0dOQlBUMGlMQ0oyWVd4MVpTSTZJazVrVEdaeU5FeHBjREExWjNBelMyNURNa2h0VDFaUWFGYzRhbUZpUzJWWU1tTTFaazlGUmt0MWRTczJSbnBtSzFaeVJFOXdaVGx3WXpCeVlVUTVRa1VyTjBOM1ZGRjZkRXB0Tkd3clMxRTBNVVEwUjNCUFUzWldkV1kxS3l0d1NXUkpNRGx3WjA1eFQzaFRWV1p4Vkc0M2QydHNUWGRwV1hkVlpraElkVVJZVWtOTmFYUldRamR5U1hwYWVFeEZVVmhDVEdaS2EwRmNMME5UV1hSUWVVdzVNbTVaVTJWdmJVOTRXR2N5VFVGb1dtVnNSRnd2TjB0bVozWmlkbk41UkRsWGQyaHRkMFY1V2xVMU56SkhlQ3NyVkVKVWVXRjRPVXQwTkN0SlIxZ3pVV1owYTNkYVRtMUlOMW93Ym5WM1pVSm5iM3BFUVZORmJUQmFVblJIWWpkQlJHaHZNWFJVYTFZeWNVUnBkV2wwVTBzd2JFbDBlVU5wVkRGVWNUaGhRVVZWU2tscU4wMTBjblZvYTF3dlRHdGxlVU4xZFdGYVRWZHpSSGhRUzJWNlJDdDVjRGRrU2tZemQxTkhkbTlLV0hkMFpFRkhOWFpwZVVnMFlUaHhYQzlhVEhkRlN6QmlZM3BjTDBsTlowSmtUamgyVlVsMWNtOUNjV2hoZFRaQmNUSmtiMmh6VVU4clRrRnZVVkV5WVRKSlJ6RlVkak5OZUZOM1ltNUhlamRyVWpReWIwZGtUakJCVVV4d1RYcDBRVVJ2U1ZWWUsxRkxlRkZPUld4eFlqQlpaakpqVjFBd1NsVjFXWE5HV0hkMGNYUnROVGsyU1dGaFUzYzlQU0lzSW0xaFl5STZJbVEwTXpSbU0yWXhOV0V6WXpBeE5tWTNOamRqTmpreU5XVXlZV1ExT0RobE9ESmpPV05sWldJNE9UaGpZemM1T0dSaFkyUXdOakl4WmpSak9UUTNZak1pZlE9PQ==', 1543285524),
('k5iOnUd0BXUQqzJ3yKAHATNxwJ03jnsytzIekKZ7', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'ZXlKcGRpSTZJbUpHUjNaWlIwWjZlSG81VW1saGNsWjZWamRrVlVFOVBTSXNJblpoYkhWbElqb2ljekYyYldWdFpWQnJTWGhNZEVsVlJqTjJVWFJHVUZkbU1HTmtSRUZaSzJGMk9ITlFZM1IwYkRKek0zWjNkV0pjTDJoUE9IZElWa0Z1ZUROa1QyOVhhMnBXUjFSWWRrVTFVM1ZxWmx3dlZYTktkbGh6VDA5bGJIRkxNMk5UT1ROdlJHMUNkbWRqUVdRMlJtOUtlR3BxTldGc2FEbFFOMWxoUlVOTU0zTnhiR05vV0VNNGRXdHNPRzl2Y1VKak5qSlhWak56VlV0blIzQlJlbXROYmt0UFkycE5lRUZXTm05clkydGpZVXcwYWtGVU5IZHFValpOVTNWRmQxWlNZV1JOY0ROWVN5dGFXalo2ZEZVNFRIcGxSVkIwUjNOWE5GY3lUekZEYzBoT2RVaEllR2wxWW5aRmJVSnVlRVIyUVdWVlhDOHhaSGRTTVZWemFXSjBibU5pUVdoTFdFVTNjRE1yVUdadlUwMVFRWE5IZWtreU5UQXJhemhJZG1GNWQwSTVVMHN3ZWtkeVVtbG1aVko1V2psSmNWVmtRbFZRWWtwUmVUZElLMUZ6VEUxTVUyZ2lMQ0p0WVdNaU9pSmxNRFJrTXpZd05qRTVOamhtTURVM01tUXdPR1U1WVRka05qRm1NamN6WkROa056Tm1OVE0yTUdReU9ETTVZalE1Tm1JNE1XSXlNREZrWW1VNU9EQTJJbjA9', 1541826734),
('LJpKUlHd7K57EvVLZzH5Vzend3U5qOJ68gqHoTXZ', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 'ZXlKcGRpSTZJbEJ1VGtONVMyZExlVkZpTlZGTGFWZ3lRVnBFZEdjOVBTSXNJblpoYkhWbElqb2lOVzVOYUU1TGVsQndXRmh0V2tRd2NERTNlbVJ1VmtkNFozTlViMVEwZFdjMmNHcE5VbTk0TTFSQlJuazRNR3RrTUc5MFpVcHpRMjl3UmtKUlREQmlkRFpPWkhkVFZWd3ZObFZ3U1dsM2ExQlhPWG93WjBKSGNFWk1URWxCUTFSNWJUZEhlbUpEVVZGMlYzUkxiQ3R0WVRsd1JHTXdOMk4zTVVSWUszcFBiamxFUjBaYVRsWkRXbUZFT1RkaVVtUkhaVEJrZEVoWVkxd3ZObE40VmpsWlZVeG9ZMFU0VDIxdWR6RTFTVGczVlVvd2EyazFkWE5YVmpCdVZFdEVUMFpWU2taT1UydzBhVEZFWVU4ME5sSnhaVFZTSzBScU9GZFphVXgzTlVscFFXUmpjRFo0YUZSTGRFOW9UMUIxU2l0cGRFbENZa0Z3TWpKY0wzaFpkV1pvWEM5aWVFbFVOSE5LZWt0SWVFeFVRVGRVZUd4R1Iwb3pTREoxTTJkYVZuTnZlRUZvWEM5c05XazNRMXd2TTBWcEswWXhWbnAzWmtKQ2RUSXhaVWhhZDNVeGFYSjZOMW9pTENKdFlXTWlPaUkyTmprNE56QmhaV1k0WW1ZelpqRTJZakl4WldFNE5tVTJZbUl3WkRobVl6UmlNelppWm1WaU1qRTVZbUkwTWpWa1pqbGpPVGhtTnpkbE1qWmhPRGhpSW4wPQ==', 1540277420),
('uUp5gMYKSFvC1gjCL9vnaXT5N6N6cpmBxGKNJCuK', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 'ZXlKcGRpSTZJblpHZDFCbWVubDRZalZoYzFjeWNGUnZXVEoxT0VFOVBTSXNJblpoYkhWbElqb2lhVzFoVTB0MVUwOHJRblYyYzNwdllVNUdkR0pZTkhCMFQxd3ZYQzlOYm1GcFJrSlFWMkZXYURCSFVtbHBRMGRRY0UxWWR6bFpUMkpoTmtRNFkzZExaa3BqZEZkS1oySnFSemd3TjA1MlRuTllPVlI1TW5OalNFVlBXRGhLWTBGTFRDdHpaVmgxUXpoTWRHNXNTMDh5UTJkelRVWkxNM0pWVW05eVprTlhhWEp1V2pBMFdUUlFXRE5aWEM5dFYzcDZWR0lyV0dac1FrVkhlV3BjTDFGb056bFVRbEY0WjBSclRuY3dibFJTVFdKMWVITkxTMUJRVHpCelRYa3hLMjlEVkdsSWIxb3JUVGhOYjJSeVYxWndhazVOVlhCUk5XZExjbkJVZEU5SmNYUldUV3BZUjNGalFVMVhlV0pyZVRGYUsxVlZiRlpJUVU4M1lqWnNUSEJsYkdWeVp6ZHliVk5TTjNWS2RFaFpZV2h2VjJadU1WaENjR1l4ZGt0MmFqVk9PR3czVkRFeVVrRjNNbkZXYjJwSVpraGhLMVJhU1hKUVRFczNVMUZJUkd3eFVpdDNJaXdpYldGaklqb2lNR00wWkdObU9HSm1OMk0wTXpRNFlXVTJPR0k0TlRNeE9XRXhZekJqTURRMU5ETTFZVFEyTVdGaU5EZzVPR1EwWVRGaVptVXdNbVpsTm1VeVpURXhaQ0o5', 1541817589);

-- --------------------------------------------------------

--
-- Table structure for table `social_logins`
--

CREATE TABLE `social_logins` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `upload`
--

CREATE TABLE `upload` (
  `id` int(11) NOT NULL,
  `data_pelamar_id` int(11) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `sertifikat_satpam` varchar(255) DEFAULT NULL,
  `skck` varchar(255) DEFAULT NULL,
  `surat_dokter` varchar(255) DEFAULT NULL,
  `surat_lamaran_kerja` varchar(255) DEFAULT NULL,
  `daftar_riwayat_hidup` varchar(255) DEFAULT NULL,
  `scan_ktp` varchar(255) DEFAULT NULL,
  `kartu_keluarga` varchar(255) DEFAULT NULL,
  `surat_keterangan_suami` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `confirmation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `status`, `confirmation_code`, `confirmed`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'Istrator', 'admin@admin.com', '$2y$10$B4R60xGlhyy2o.JaLZft7exeJVzdTN0eoxw8ZfvhHAsFG/2Cu0ocS', 1, '98051ba8ee356e2d3672537f4543a85a', 1, 'kS9v6ABVEaXoXxLK1QmzkpfbWbjx8gE82VYPRJKw4dV9Hh6kTE7bPZfly74N', '2018-09-25 19:42:35', '2018-09-25 19:42:35', NULL),
(2, 'Backend', 'User', 'executive@executive.com', '$2y$10$17DsLvy1n5KzjVbbPsTdd..VKst0g6a5MYq7GDlfSp0LAtnJSfYjO', 1, 'aa1923fc1da62d335fe24a57ce201e54', 1, NULL, '2018-09-25 19:42:35', '2018-09-25 19:42:35', NULL),
(3, 'Default', 'User', 'user@user.com', '$2y$10$KQKU9BCxxbaJaHZ0elIouuc.HOKGwYWrV80jyS50l5ILeakdHUAmO', 1, 'df2df9c424d509c18a48927ca0f13eea', 1, NULL, '2018-09-25 19:42:35', '2018-09-25 19:42:35', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_fisik_pelamars`
--
ALTER TABLE `data_fisik_pelamars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_keluarga_pelamars`
--
ALTER TABLE `data_keluarga_pelamars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_lain_pelamars`
--
ALTER TABLE `data_lain_pelamars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_pelamars`
--
ALTER TABLE `data_pelamars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_pendidikan_pelamars`
--
ALTER TABLE `data_pendidikan_pelamars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_type_id_foreign` (`type_id`),
  ADD KEY `history_user_id_foreign` (`user_id`);

--
-- Indexes for table `history_types`
--
ALTER TABLE `history_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_logins_user_id_foreign` (`user_id`);

--
-- Indexes for table `upload`
--
ALTER TABLE `upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_fisik_pelamars`
--
ALTER TABLE `data_fisik_pelamars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `data_keluarga_pelamars`
--
ALTER TABLE `data_keluarga_pelamars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `data_lain_pelamars`
--
ALTER TABLE `data_lain_pelamars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `data_pelamars`
--
ALTER TABLE `data_pelamars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `data_pendidikan_pelamars`
--
ALTER TABLE `data_pendidikan_pelamars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `history_types`
--
ALTER TABLE `history_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `social_logins`
--
ALTER TABLE `social_logins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `upload`
--
ALTER TABLE `upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `history_types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `history_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD CONSTRAINT `social_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
